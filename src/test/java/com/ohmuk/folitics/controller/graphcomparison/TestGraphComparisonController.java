package com.ohmuk.folitics.controller.graphcomparison;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.GraphComparisonFilter;
import com.ohmuk.folitics.utils.TestDataUtils;
import com.ohmuk.folitics.utils.TestUtils;

@RunWith(BlockJUnit4ClassRunner.class)
@WebIntegrationTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestGraphComparisonController {

    // Test RestTemplate to invoke the sAPIs.
    private RestTemplate restTemplate = new TestRestTemplate();
    Map<String, GraphComparisonFilter> comparisonFilters;

    @Test
    public void TestGraphComparisonAddApi() throws JsonProcessingException {

        comparisonFilters = testGraphComparisonFilterAddApi();

    }

    @Test
    public void TestGraphComparisonGETFILTERSApi() throws JsonProcessingException {
        testGraphComparisonFiltergetGraphFiltersApi(comparisonFilters);

    }

    private void testGraphComparisonFiltergetGraphFiltersApi(Map<String, GraphComparisonFilter> comparisonFilters) {

        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL 
                        + TestUtils.GRAPHCOMPARISON_CONTROLLER_APIS.GET_FILTERS)
                .queryParam("graphSegment", "GA").queryParam("statusFlag", "Deleted");
      
        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();

        HttpEntity<ResponseDto<GraphComparisonFilter>> response = restTemplate.exchange(
                builder.build().encode().toUri(), HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<ResponseDto<GraphComparisonFilter>>() {
                });

        System.out.println("Response of GET_FILTERS-GCFApi :" + response.getBody().toString());
        ResponseDto<GraphComparisonFilter> apiResponse = response.getBody();
        assertNotNull(apiResponse);
        assertTrue(apiResponse.getSuccess());
        
    }

    private Map<String, GraphComparisonFilter> testGraphComparisonFilterAddApi() throws JsonProcessingException {

        Map<String, GraphComparisonFilter> returnMap = new HashMap<>();

        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL + TestUtils.GRAPHCOMPARISON_CONTROLLER_APIS.ADD);

        HttpEntity<String> httpEntityGraphComparisonFilter = TestUtils
                .getHttpEntity(TestDataUtils.getGraphComparisonFilterRequestBody());

        HttpEntity<ResponseDto<GraphComparisonFilter>> responseGraphComparisonFilter = restTemplate.exchange(
                builder.build().encode().toUri(), HttpMethod.POST, httpEntityGraphComparisonFilter,
                new ParameterizedTypeReference<ResponseDto<GraphComparisonFilter>>() {
                });
        ResponseDto<GraphComparisonFilter> apiGraphComparisonFilterResponse = responseGraphComparisonFilter.getBody();

        System.out.println("Response of GCFApi :" + apiGraphComparisonFilterResponse.toString());

        assertNotNull(apiGraphComparisonFilterResponse);
        assertTrue(apiGraphComparisonFilterResponse.getSuccess());

        String responseMessageGraphComparisonFilterResponse = responseGraphComparisonFilter.getBody()
                .getResponseMessage().get(0);
        GraphComparisonFilter graphComparisonFilterResponses = (GraphComparisonFilter) (responseGraphComparisonFilter
                .getBody().getMessages().get(0));

        assertEquals(responseMessageGraphComparisonFilterResponse, "graphComparison filter added successfully");
        assertEquals(graphComparisonFilterResponses.getGraphSegment(), "GA");
        assertEquals(graphComparisonFilterResponses.getName(), "Birla");
        assertEquals(graphComparisonFilterResponses.getDescription(),
                "Sometime it takes many word to put the fullstop");
        assertEquals(graphComparisonFilterResponses.getStatusFlag(), "Deleted");
        returnMap.put("GraphComparisonFilter", graphComparisonFilterResponses);
        return returnMap;
    }

}
