package com.ohmuk.folitics.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.test.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.enums.CategoryType;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.enums.GenderType;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Component;
import com.ohmuk.folitics.hibernate.entity.GovtSchemeData;
import com.ohmuk.folitics.hibernate.entity.IndicatorData;
import com.ohmuk.folitics.hibernate.entity.IndicatorThreshold;
import com.ohmuk.folitics.hibernate.entity.IndicatorWeightedData;
import com.ohmuk.folitics.hibernate.entity.User;

/**
 * @author Abhishek
 *
 */
public class TestDataUtils {

    private static RestTemplate restTemplate = new TestRestTemplate();

    public static Map<String, Object> getParentCategoryRequestBody() {
        // Building the Request body data
        Map<String, Object> requestBody = new HashMap<String, Object>();
        requestBody.put("name", "TestParentCategory ");
        requestBody.put("type", CategoryType.CATEGORY.getValue());
        requestBody.put("description", "This is test Parent category");
        requestBody.put("createdBy", 10l);
        return requestBody;
    }

    public static Map<String, Object> getCategoryRequestBody(List<Category> parentList) {
        return getCategoryRequestBody(parentList, CategoryType.SUBCATEGORY);
    }

    public static Map<String, Object> getCategoryRequestBody(List<Category> parentList, CategoryType type) {
        // Building the Request body data
        Map<String, Object> requestBody = new HashMap<String, Object>();
        requestBody.put("name", "TestChildCategory");
        requestBody.put("type", type.getValue());
        requestBody.put("description", "This is test child category");
        requestBody.put("parents", parentList);
        requestBody.put("createdBy", 10l);
        return requestBody;
    }

    public static Category getCategory(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL + TestUtils.CATEGORY_CONTROLLER_APIS.FIND).queryParam("id", id);
        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
        HttpEntity<ResponseDto<Category>> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<Category>>() {
                });
        return (Category) (response.getBody().getMessages().get(0));
    }

    public static Map<String, Object> getUserRequestBody() throws IOException {
        return getUser("username1");
    }

    public static Map<String, Object> getUserConnectionRequestBody() throws IOException {
        return getUser("userConnection");
    }

    public static Map<String, Object> getUser(String username) throws IOException {

        Map<String, Object> requestBody = new HashMap<String, Object>();
        long randomNo = (long) (Math.random() * 1000000);
        requestBody.put("username", username + randomNo);
        requestBody.put("password", "12345");
        requestBody.put("emailId", "testfoliticsuser@gmail.com");
        requestBody.put("status", "VerifiedByEmail");
        requestBody.put("name", "name");
        requestBody.put("gender", GenderType.MALE.getValue());
        requestBody.put("userAssociation", null);
        requestBody.put("userEmailNotificationSettings", null);
        requestBody.put("userUINotification", null);
        requestBody.put("dob", System.currentTimeMillis() - 886400000);
        requestBody.put("maritalStatus", "Married");
        requestBody.put("points", 400);
        requestBody.put("religion", "Hinduism");
        requestBody.put("state", "Madhya Pradesh");
        // requestBody.put("userEducation", getUserEducation());

        return requestBody;
    }

    public static User getUser(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL + TestUtils.USER_CONTROLLER_APIS.FIND).queryParam("id", id);
        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
        HttpEntity<ResponseDto<User>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,
                httpEntity, new ParameterizedTypeReference<ResponseDto<User>>() {
                });
        return (User) (response.getBody().getMessages().get(0));
    }

    public static Map<String, Object> getResponseRequestBody() {
        Map<String, Object> requestBody = new HashMap<String, Object>();

        requestBody.put("subject", "Response Subject");
        requestBody.put("text", "Response Text");
        // requestBody.put("links", setOfLink);
        requestBody.put("componentType", ComponentType.RESPONSE.getValue());
        return requestBody;
    }

    public static Component getComponent(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL + TestUtils.COMPONENT_CONTROLLER_APIS.FIND).queryParam("id", id);
        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
        HttpEntity<ResponseDto<Component>> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<Component>>() {
                });
        return (Component) (response.getBody().getMessages().get(0));
    }

    public static Map<String, Object> getMilestoneRequestBody() throws JsonProcessingException {
        // Building the Request body data
        Map<String, Object> requestBody = new HashMap<String, Object>();
        requestBody.put("milestone", 30);
        requestBody.put("url", "https://www.google.co.in");
        requestBody.put("description", "Test Description");
        requestBody.put("milestone_type", "Mega Test");
        return requestBody;
    }

    public static Map<String, Object> getIndicatorDataRequestBody(Category category) throws JsonProcessingException {
        Map<String, Object> requestBody = new HashMap<String, Object>();

        requestBody.put("createTime", 1454043467928l);
        requestBody.put("deleted", 1);
        requestBody.put("delta", 1);
        requestBody.put("idealValueRange", "kkkk");
        requestBody.put("weightedIdealValue", 1);
        requestBody.put("weightedValue", 1);
        requestBody.put("editTime", 1454043467928l);
        requestBody.put("effectfromdate", 1454043467928l);
        requestBody.put("category", category);
        requestBody.put("indicatorvalue", 3);
        requestBody.put("score", 4);
        requestBody.put("state", "true");
        requestBody.put("thresholdcategory", "On Track");
        requestBody.put("updateddate", 1454043467928l);
        return requestBody;

    }

    public static Map<String, Object> getIndicatorDataResponseBody(Long categoryID) {
        // Building the Request body data
        Map<String, Object> requestBody = new HashMap<String, Object>();
        Category category = new Category();
        category.setId(categoryID);
        requestBody.put("createTime", 1454043467928l);
        requestBody.put("deleted", 1);
        requestBody.put("delta", 1);
        requestBody.put("idealValueRange", "kkkk");
        requestBody.put("weightedIdealValue", 1);
        requestBody.put("weightedValue", 1);
        requestBody.put("editTime", 1454043467928l);
        requestBody.put("effectfromdate", 1454043467928l);
        requestBody.put("category", category);
        requestBody.put("indicatorvalue", 3);
        requestBody.put("score", 1);
        requestBody.put("state", "true");
        requestBody.put("thresholdcategory", "On Track");
        requestBody.put("updateddate", 1454043467928l);
        return requestBody;
    }

    public static IndicatorData getIndicatorData(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL + TestUtils.INDICATOR_DATA_CONTROLLER_APIS.FIND).queryParam("id", id);
        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
        HttpEntity<ResponseDto<IndicatorData>> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<IndicatorData>>() {
                });
        return (IndicatorData) (response.getBody().getMessages().get(0));
    }

    public static IndicatorThreshold getIndicatorThresholdData(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL + TestUtils.INDICATOR_THRESHOLD_DATA_CONTROLLER_APIS.FIND)
                .queryParam("id", id);
        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
        HttpEntity<ResponseDto<IndicatorThreshold>> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<IndicatorThreshold>>() {
                });
        return (IndicatorThreshold) (response.getBody().getMessages().get(0));
    }

    public static Map<String, Object> getIndicatorThresholdRequestBody() {
        // Building the Request body data
        Map<String, Object> requestBody = new HashMap<String, Object>();
        Category category = new Category();
        category.setId(1l);
        // requestBody.put("id",1);
        requestBody.put("createTime", 1454043467928l);
        requestBody.put("editTime", 1454043467928l);
        requestBody.put("direction", 5);
        requestBody.put("category", category);
        requestBody.put("state", "true");
        // requestBody.put("threshold", 4);
        requestBody.put("threshold_end", 4);
        requestBody.put("threshold_start", 4);
        requestBody.put("threshHoldCategory", "UnSatisfactory");

        return requestBody;
    }

    public static Map<String, Object> getIndicatorThresholdResponseBody(long id, Long categoryId) {
        // Building the Request body data
        Map<String, Object> requestBody = new HashMap<String, Object>();
        Category category = new Category();
        category.setId(categoryId);
        // requestBody.put("id",1);
        requestBody.put("createTime", 1454043467928l);
        requestBody.put("editTime", 1454043467928l);
        requestBody.put("direction", 5);
        requestBody.put("category", category);
        requestBody.put("state", "true");
        // requestBody.put("threshold", 4);
        requestBody.put("threshold_end", 4);
        requestBody.put("threshold_start", 4);
        requestBody.put("threshHoldCategory", "UnSatisfactory");
        return requestBody;
        /*
         * UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
         * TestUtils.BASE_URL +
         * TestUtils.INDICATOR_THRESHOLD_DATA_CONTROLLER_APIS.FIND).queryParam(
         * "id", id); HttpEntity<HttpHeaders> httpEntity =
         * TestUtils.getHttpEntityHttpHeaders();
         * HttpEntity<ResponseDto<IndicatorThreshold>> response =
         * restTemplate.exchange(builder.build().encode() .toUri(),
         * HttpMethod.GET, httpEntity, new
         * ParameterizedTypeReference<ResponseDto<IndicatorThreshold>>() { });
         * return (Map<String,Object>)
         * (response.getBody().getMessages().get(0));
         */

    }

    public static Map<String, Object> getIndicatorWeightedDataRequestBody() throws JsonProcessingException {
        Map<String, Object> requestBody = new HashMap<String, Object>();
        Category category = new Category();
        category.setId(3l);
        requestBody.put("createTime", 1454043467928l);
        requestBody.put("deleted", 1);
        requestBody.put("editTime", 1454043467928l);
        requestBody.put("effectivefromdate", 1454043467928l);
        requestBody.put("category", category);
        requestBody.put("state", "true");
        requestBody.put("weightage", 3);
        requestBody.put("impactOnChart", 1);
        requestBody.put("updateddate", 1454043467928l);
        return requestBody;

    }

    public static IndicatorWeightedData getIndicatorWeightedData(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL + TestUtils.INDICATOR_WEIGHTED_DATA_CONTROLLER_APIS.FIND)
                .queryParam("id", id);
        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
        HttpEntity<ResponseDto<IndicatorWeightedData>> response = restTemplate.exchange(
                builder.build().encode().toUri(), HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<ResponseDto<IndicatorWeightedData>>() {
                });
        return (IndicatorWeightedData) (response.getBody().getMessages().get(0));
    }

    public static Map<String, Object> getIndicatorWeightedDataResponseBody(Long categoryID) {
        // Building the Request body data
        Map<String, Object> requestBody = new HashMap<String, Object>();
        Category category = new Category();
        category.setId(categoryID);
        // requestBody.put("id",1);
        requestBody.put("createTime", 1454043467928l);
        requestBody.put("deleted", 1);
        requestBody.put("editTime", 1454043467928l);
        requestBody.put("effectivefromdate", 1454043467928l);
        requestBody.put("category", category);
        requestBody.put("state", "true");
        requestBody.put("weightage", 3);
        requestBody.put("impactOnChart", 1);
        requestBody.put("updateddate", 1454043467928l);
        return requestBody;
    }

    public static Map<String, Object> getGovtSchemeDataRequestBody() throws JsonProcessingException {

        Map<String, Object> requestBody = new HashMap<String, Object>();
        requestBody.put("createTime", 1454043467928l);
        requestBody.put("isactive", 1);
        requestBody.put("editTime", 1454043467928l);
        requestBody.put("startdate", 1454043467928l);
        requestBody.put("state", "true");
        requestBody.put("schemename", "scname");
        requestBody.put("description1", "desc");
        requestBody.put("description2", "desccc");
        requestBody.put("webpageurl", "wpage");
        return requestBody;

    }

    public static GovtSchemeData getGovtSchemeData(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL + TestUtils.GOVT_SCHEME_DATA_CONTROLLER_APIS.FIND).queryParam("id", id);
        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
        HttpEntity<ResponseDto<GovtSchemeData>> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<GovtSchemeData>>() {
                });
        return (GovtSchemeData) (response.getBody().getMessages().get(0));
    }

    public static Map<String, Object> getGovtSchemeDataResponseBody() {
        // Building the Request body data
        Map<String, Object> requestBody = new HashMap<String, Object>();

        // requestBody.put("id",1);
        requestBody.put("createTime", 1454043467928l);
        requestBody.put("isactive", 1);
        requestBody.put("editTime", 1454043467928l);
        requestBody.put("startdate", 1454043467928l);
        requestBody.put("state", "trueee");
        requestBody.put("schemename", "scname");
        requestBody.put("description1", "desc");
        requestBody.put("description2", "desccc");
        requestBody.put("webpageurl", "wpage");
        return requestBody;
    }

    public static Long testPreAddIndicatorThresholdApi(Category category) throws JsonProcessingException {
        Map<String, Object> requestBody = TestDataUtils.getPreIndicatorThresholdRequestBody(category);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL + TestUtils.INDICATOR_THRESHOLD_DATA_CONTROLLER_APIS.ADD);
        HttpEntity<String> httpEntity = TestUtils.getHttpEntity(requestBody);
        HttpEntity<ResponseDto<IndicatorThreshold>> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<IndicatorThreshold>>() {
                });

        ResponseDto<IndicatorThreshold> apiResponse = response.getBody();
        assertNotNull(apiResponse);
        assertTrue(apiResponse.getSuccess());
        return ((IndicatorThreshold) response.getBody().getMessages().get(0)).getId();
    }

    public static Map<String, Object> getPreIndicatorThresholdRequestBody(Category category) {
        // Building the Request body data
        Map<String, Object> requestBody = new HashMap<String, Object>();
        // requestBody.put("id",1);
        requestBody.put("createTime", 1454043467928l);
        requestBody.put("editTime", 1454043467928l);
        requestBody.put("direction", 5);
        requestBody.put("category", category);
        requestBody.put("state", "true");
        // requestBody.put("threshold", 4);
        requestBody.put("threshold_end", 5);
        requestBody.put("threshold_start", 1);
        requestBody.put("threshHoldCategory", "On Track");

        return requestBody;
    }

    public static Long testPreAddIndicatorWeightedDataApi(Category category) throws JsonProcessingException {
        Map<String, Object> requestBody = TestDataUtils.getPreIndicatorWeightedDataRequestBody(category);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(TestUtils.BASE_URL + TestUtils.INDICATOR_WEIGHTED_DATA_CONTROLLER_APIS.ADD);
        HttpEntity<String> httpEntity = TestUtils.getHttpEntity(requestBody);
        HttpEntity<ResponseDto<IndicatorWeightedData>> response = restTemplate.exchange(
                builder.build().encode().toUri(), HttpMethod.POST, httpEntity,
                new ParameterizedTypeReference<ResponseDto<IndicatorWeightedData>>() {
                });

        ResponseDto<IndicatorWeightedData> apiResponse = response.getBody();
        assertNotNull(apiResponse);
        assertTrue(apiResponse.getSuccess());
        return ((IndicatorWeightedData) response.getBody().getMessages().get(0)).getId();

    }

    public static Map<String, Object> getPreIndicatorWeightedDataRequestBody(Category category)
            throws JsonProcessingException {
        Map<String, Object> requestBody = new HashMap<String, Object>();
        requestBody.put("createTime", 1454043467928l);
        requestBody.put("deleted", 1);
        requestBody.put("editTime", 1454043467928l);
        requestBody.put("effectivefromdate", 1454043467928l);
        requestBody.put("category", category);
        requestBody.put("state", "true");
        requestBody.put("weightage", 3);
        requestBody.put("impactOnChart", 1);
        requestBody.put("updateddate", 1454043467928l);
        return requestBody;

    }

    public static Map<String, Object> getGraphComparisonFilterRequestBody() throws JsonProcessingException {
        // TODO Auto-generated method stub
        Map<String, Object> requestBody = new HashMap<String, Object>();
        requestBody.put("name", "Birla");
        requestBody.put("description", "Sometime it takes many word to put the fullstop");
        requestBody.put("statusFlag", "Deleted");
        requestBody.put("graphSegment", "GA");
      /*  requestBody.put("createTime", 1454043467928l);
        requestBody.put("editTime", 1454043467928l);*/
        requestBody.put("startDate", 1454043467928l);
        requestBody.put("endDate", 1454043467928l);
        return requestBody;
    }

}
