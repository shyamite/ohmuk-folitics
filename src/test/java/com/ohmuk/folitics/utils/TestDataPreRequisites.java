package com.ohmuk.folitics.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.boot.test.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Component;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.util.DateUtils;

public class TestDataPreRequisites {

    public interface ComponentPrerequisiteMapKeys {
        String CATEGORIES = "categories";
        String SENTIMENT = "sentiment";
        String COMPONENT = "component";
        String USER = "user";
        String OPNION = "opinion";
        String POLL = "poll";
        String ATTACHMENT = "attachment";
    }

    private static RestTemplate restTemplate = new TestRestTemplate();

    
    public static Map<String, Object> createMilestonePrerequisite() {
        Map<String, Object> prerequisiteMap = new HashMap<String, Object>();
        prerequisiteMap.put("createTime", DateUtils.getSqlTimeStamp());
        prerequisiteMap.put("editTime", DateUtils.getSqlTimeStamp());
        prerequisiteMap.put("startTime", DateUtils.getSqlTimeStamp());
        prerequisiteMap.put("endTime", DateUtils.getSqlTimeStamp());
        prerequisiteMap.put("totalPoints", 30);
        prerequisiteMap.put("opinionResponseAggregationPoints", 10);
        prerequisiteMap.put("indicatorChangePoints", 10);
        prerequisiteMap.put("eventReportPoints", 10);
        return prerequisiteMap;

    }

    public static Map<String, Set<Category>> insertCategory() throws JsonProcessingException {
        Map<String, Set<Category>> returnMap = new HashMap<String, Set<Category>>();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(TestUtils.BASE_URL
                + TestUtils.CATEGORY_CONTROLLER_APIS.ADD);
        // category
        HttpEntity<String> httpEntityCategory = TestUtils.getHttpEntity(TestDataUtils.getParentCategoryRequestBody());
        HttpEntity<ResponseDto<Category>> responseCategory = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.POST, httpEntityCategory, new ParameterizedTypeReference<ResponseDto<Category>>() {
                });
        ResponseDto<Category> apiCategoryResponse = responseCategory.getBody();
        Category categoryParent1 = (Category) (responseCategory.getBody().getMessages().get(0));
        Category categoryParent2 = TestDataUtils.getCategory(categoryParent1.getId());

        // subcategory 1

        Set<Category> categoryReturnList = new HashSet<Category>();
        categoryReturnList.add(categoryParent1);
        returnMap.put("category", categoryReturnList);
        return returnMap;
    }

    public static Category insertIndicator() throws JsonProcessingException {
        Map<String, Set<Category>> returnMap = new HashMap<String, Set<Category>>();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(TestUtils.BASE_URL
                + TestUtils.CATEGORY_CONTROLLER_APIS.ADD);
        // category
        HttpEntity<String> httpEntityCategory = TestUtils.getHttpEntity(TestDataUtils.getParentCategoryRequestBody());
        HttpEntity<ResponseDto<Category>> responseCategory = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.POST, httpEntityCategory, new ParameterizedTypeReference<ResponseDto<Category>>() {
                });
        ResponseDto<Category> apiCategoryResponse = responseCategory.getBody();
        Category categoryParent1 = (Category) (responseCategory.getBody().getMessages().get(0));
        // Category categoryParent2 = TestDataUtils.getCategory(categoryParent1.getId());
        return categoryParent1;
    }

    public static void deleteCategory(Set<Category> prerequisiteSet) {
        for (Category category : prerequisiteSet) {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
                    TestUtils.BASE_URL + TestUtils.CATEGORY_CONTROLLER_APIS.PERMANENT_DELETE).queryParam("id",
                    category.getId());
            HttpEntity<Category> entity = new HttpEntity<Category>(category);
            HttpEntity<ResponseDto<Category>> response = restTemplate.exchange(builder.build().encode().toUri(),
                    HttpMethod.POST, entity, new ParameterizedTypeReference<ResponseDto<Category>>() {
                    });
            response.getBody().getSuccess();
        }
    }

    public static User insertUser() throws IOException {
        HttpEntity<String> httpEntity = TestUtils.getHttpEntity(TestDataUtils.getUserRequestBody());
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(TestUtils.BASE_URL
                + TestUtils.USER_CONTROLLER_APIS.ADD);
        HttpEntity<ResponseDto<User>> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<User>>() {
                });
        User user = (User) (response.getBody().getMessages().get(0));
        return user;
    }


    public static void deleteUser(User user) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
                TestUtils.BASE_URL + TestUtils.USER_CONTROLLER_APIS.PERMANENT_DELETE).queryParam("id", user.getId());
        HttpEntity<?> entity = new HttpEntity<>(user);
        HttpEntity<ResponseDto<User>> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.POST, entity, new ParameterizedTypeReference<ResponseDto<User>>() {
                });
    }


    public static void removeComponent(Component component) {
        // get component
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
                TestUtils.BASE_URL + TestUtils.COMPONENT_CONTROLLER_APIS.FIND).queryParam("id", component.getId());
        HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
        HttpEntity<ResponseDto<Component>> componentresponse = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Component>>() {
                });
        Component comp = (Component) (componentresponse.getBody().getMessages().get(0));
    }


    public static Map<String, Object> createPollPrerequisite() throws IOException {
        Map<String, Object> prerequisitemap = new HashMap<String, Object>();
        prerequisitemap.put(ComponentPrerequisiteMapKeys.USER, insertUser());
        return prerequisitemap;
    }

    public static void deletePollPrerequisite(Map<String, Object> prerequisitemap) {

        deleteUser((User) prerequisitemap.get(ComponentPrerequisiteMapKeys.USER));
    }

    
}
