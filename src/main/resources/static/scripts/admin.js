'use strict';



/**
 * @ngdoc overview
 * @name govrnnApp
 * @description # govrnnApp
 *
 * Main module of the application.
 */
angular.module(
    'govrnnApp',
    [ 'ngAnimate', 'ngCookies', 'ngResource', 'ngRoute', 'ngSanitize',
        'ngTouch', 'ngStorage'])

    .config(function($routeProvider) {
        $routeProvider.when('/', {
            templateUrl : 'views/admin/index.html',
            controller : 'AdminCtrl'
        }).when('/schemes/review-facts', {
            templateUrl : 'views/schemes/reviewFacts.html',
            controller : 'GovtSchemeDataCommentCtrl'
        }).when('/indicator/load-value', {
            templateUrl : 'views/indicators/load.html',
            controller : 'AdminCtrl'
        }).when('/indicator/create', {
            templateUrl : 'views/indicators/addIndicator.html',
            controller : 'AdminCtrl'
        }).when('/keyindicators', {
            templateUrl : 'views/admin/keyindicators.html',
            controller : 'KeyIndicatorsCtrl'
        }).when('/graphcomparisonfilters/add', {
            templateUrl : 'views/graphfilters/add.html',
            controller : 'GraphFiltersCtrl'
        }).when('/graphcomparisonfilters/list', {
            templateUrl : 'views/graphfilters/list.html',
            controller : 'GraphFiltersCtrl'
        }).when('/manifesto/add', {
            templateUrl : 'views/manifesto/add.html',
            controller : 'GraphFiltersCtrl'
        }).when('/manifesto/list', {
            templateUrl : 'views/manifesto/list.html',
            controller : 'GraphFiltersCtrl'
        }).otherwise({
            redirectTo : '/'
        });
    })

    .run(function($rootScope) {
        $rootScope.liveHost = 'http://localhost:8080';
    }).config([ '$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
} ]);

