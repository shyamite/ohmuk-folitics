'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.directive:GA
 * @description # GACtrl Controller of the govrnnApp
 */
angular.module('govrnnApp').directive('chartelementstock', [ '$http', '$q', function($http, $q) {
	return {
		restrict : 'E',
		replace : true,
		template : '<div id="chartdivmultiaxes" style="min-width: 100%; height: 400px; margin: 0 auto"></div>',
		link : function(scope, element, attrs) {

			var chart = false;
			var initChart = function() {
				if (chart)
					chart.destroy();
				var config = scope.config || {};
				var chartData = generateChartData();
				var chart = AmCharts.makeChart("chartdivmultiaxes", {
					"type" : "serial",
					"theme" : "none",
					"legend" : {
						"useGraphSettings" : true
					},
					"dataProvider" : chartData,
					"synchronizeGrid" : true,
					"valueAxes" : [ {
						"id" : "v1",
						"axisColor" : "#FF6600",
						"axisThickness" : 1,
						"axisAlpha" : 1,
						"position" : "left"
					}, {
						"id" : "v2",
						"axisColor" : "#FCD202",
						"axisThickness" : 1,
						"axisAlpha" : 1,
						"position" : "right"
					}, {
						"id" : "v3",
						"axisColor" : "#000000",
						"axisThickness" : 1,
						"gridAlpha" : 0,
						"offset" : 50,
						"axisAlpha" : 1,
						"position" : "right"
					} ],
					"graphs" : [ {
						"valueAxis" : "v1",
						"lineColor" : "#FF6600",
						"bullet" : "round",
						"bulletBorderThickness" : 1,
						"hideBulletsCount" : 30,
						"title" : "red line",
						"valueField" : "visits",
						"fillAlphas" : 0
					}, {
						"valueAxis" : "v2",
						"lineColor" : "#FCD202",
						"bullet" : "square",
						"bulletBorderThickness" : 1,
						"hideBulletsCount" : 30,
						"title" : "yellow line",
						"valueField" : "hits",
						"fillAlphas" : 0
					}, {
						"valueAxis" : "v3",
						"lineColor" : "#B0DE09",
						"bullet" : "triangleUp",
						"bulletBorderThickness" : 1,
						"hideBulletsCount" : 30,
						"title" : "green line",
						"valueField" : "views",
						"fillAlphas" : 0
					} ],
					"chartScrollbar" : {},
					"chartCursor" : {
						"cursorPosition" : "mouse"
					},
					"categoryField" : "date",
					"categoryAxis" : {
						"parseDates" : true,
						"axisColor" : "#DADADA",
						"minorGridEnabled" : true
					},
					"export" : {
						"enabled" : true,
						"position" : "bottom-right"
					}
				});

				chart.addListener("dataUpdated", zoomChart);
				zoomChart();

				// generate some random data, quite different range
				function generateChartData() {
					var chartData = [];
					var firstDate = new Date();
					firstDate.setDate(firstDate.getDate() - 100);

					for (var i = 0; i < 100; i++) {
						// we create date objects here. In your data, you can
						// have date strings
						// and then set format of your dates using
						// chart.dataDateFormat property,
						// however when possible, use date objects, as this will
						// speed up chart rendering.
						var newDate = new Date(firstDate);
						newDate.setDate(newDate.getDate() + i);

						var visits = Math.round(Math.sin(i * 5) * i);
						var hits = Math.round(Math.random() * 80) + 500 + i * 3;
						var views = Math.round(Math.random() * 6000) + i * 4;

						chartData.push({
							date : newDate,
							visits : visits,
							hits : hits,
							views : views
						});
					}
					return chartData;
				}

				function zoomChart() {
					chart.zoomToIndexes(chart.dataProvider.length - 20, chart.dataProvider.length - 1);
				}
			};
			initChart();
		}
	}
} ]);