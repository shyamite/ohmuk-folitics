'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.directive:GA
 * @description # GACtrl Controller of the govrnnApp
 */
angular.module('govrnnApp').directive('chartelementserial',
		[ '$http', '$q', '$timeout', 'IndicatorService', function($http, $q, $timeout, IndicatorService) {
			return {
				restrict : 'E',
				replace : true,
				scope : {
					data : '=graphdata' 
				},
				template : function(ele,attr){
					return '<div id="'+attr.id+'" style="min-width: 100%; height: 300px; margin: 0 auto"></div>';
				},

				link : function(scope, element, attrs) {
					scope.$watch('data', function() {
						var chart = false;
						var initChart = function() {
							if (chart)
								chart.destroy();


							var filteredDataset = [];
							var threshold = {};
							var datasets = [];
							var dataset = {};
							dataset.dataProvider = [];
							if (scope.data) {

								filteredDataset = scope.data.datasets ? scope.data.datasets : scope.data;


								for (var i = 0; i < filteredDataset.length; i++) {
									dataset.dataProvider = dataset.dataProvider.concat(filteredDataset[i].dataProvider);
									if (!dataset.fieldMappings) {
										dataset.fieldMappings = filteredDataset[i].fieldMappings;
									}
									if (!dataset.categoryField) {
										dataset.categoryField = filteredDataset[i].categoryField;
									}

									if (!dataset.filters) {
										dataset.filters = scope.data.filters;
									}

									if (!dataset.thresholdValues) {
										dataset.thresholdValues = filteredDataset[i].thresholdValues;
									}
									if(!dataset.title)
									if(scope.data.title)
									dataset.title = scope.data.title;
									else if(scope.data.length==1)
									dataset.title=scope.data[0].title;
									datasets.push(dataset);
								}

								if(attrs.name=="keyindicatorsgraph"){
									datasets=[];
									datasets.push(filteredDataset[0]);
								}


								threshold = scope.data.thresholdValues ? scope.data.thresholdValues : {};
							}

							var stockPanel = {
								"type": "stock",
								"theme": "none",
								"dataSets": datasets,
								"zoomEnabled":true,
								"addComparisonButton":function(_chrt,d){
									var comparisonBtn=document.createElement("a");
									comparisonBtn.className='btn pull-right custom_btn';
									var comparisonBtni=document.createElement("i");
									comparisonBtni.className='fa fa-plus';

									comparisonBtn.appendChild(comparisonBtni);
									var txt=document.createTextNode(" Comparison");
									comparisonBtn.appendChild(txt);
									comparisonBtn.setAttribute("comparisonEnabled",false);
									d.append(comparisonBtn);
									comparisonBtn.addEventListener("click",function(){
										var isComparisonEnabled=this.getAttribute("comparisonEnabled");
										if(!isComparisonEnabled){
											this.text="Hide Comparison";
											this.setAttribute("comparisonEnabled",true);
										}else{
											this.setAttribute("comparisonEnabled",false);
										}
										_chrt.comparison(isComparisonEnabled);

									});
								},
								"comparison":function(isComparisonEnabled){

									if(!isComparisonEnabled){
										chart.mainDataSet=chart.dataSets[0];
										char.comparedDataSets=[];
										for ( var i = 0; i < chart.dataSets.length; i++ ) {
											chart.dataSets[ i ].compared = false;
										}
									}else{
										console.log(chart);
										chart.comparedDataSets = [];
										for ( var i = 0; i < chart.dataSets.length; i++ ) {
											chart.dataSets[ i ].compared = false;
										}

										if(chart.mainDataSet.maxTime<chart.dataSets[this.keyDataSet].maxTime)
											chart.mainDataSet.dataProvider.push({"date":chart.dataSets[this.keyDataSet].maxTime})


										if(chart.mainDataSet.minTime<chart.dataSets[this.keyDataSet].minTime)
											chart.mainDataSet.dataProvider.push({"date":chart.dataSets[this.keyDataSet].minTime});



										chart.comparedDataSets.push(chart.dataSets[this.keyDataSet]);
										chart.dataSets[this.keyDataSet].compared = true;
									}

									//this.keyDataSet.compared=true;
									//this.dataSets=[];
									//this.keyDataSet.compared=true;
									//this.mainDataSet.compared=true;
									//this.dataSets.push(this.keyDataSet);
									//this.dataSets.push(this.mainDataSet);
									////this.comparedDataSets=[];
									////this.comparedDataSets.push(this.keyDataSet );
									//this.dataSets[0].compared=true;
									//this.dataSets[1].compared=true;
									//this.periodSelector.setDefaultPeriod();
									chart.validateData();
								},
								"panels": [{
									"recalculateToPercents":"never",									"stockGraphs": [{
										"id": "g1",
										"valueField": "value",
										"comparable": false,
										"compareGraphBalloonFunction": function (graphDataItem, graph) {
											console.log(this);
											var _details = graph.title + " : " + graphDataItem.values.value;
											if (graphDataItem.dataContext.dataContext.details) {
												var details = graphDataItem.dataContext.dataContext.details;
												for (var key  in details) {
													_details = _details + "<br/>" + key + ":" + details[key];
												}
											}
											return _details;
										},
										"balloonFunction": function (graphDataItem, graph) {
											var _details = graph.title + " : " + graphDataItem.values.value;
											if (graphDataItem.dataContext.dataContext.details) {
												var details = graphDataItem.dataContext.dataContext.details;
												for (var key  in details) {
													_details = _details + "<br/>" + key + ":" + details[key];
												}
											}
											return _details;
										}
									}],
									"stockLegend": {
										"enabled": true,
										"textClickEnabled":true,
										"switchable":true,
										listeners:[
											{	"event":"clickLabel",
												"method":function(event){
													event.chart.stockChart.filteredDataSets[ event.target.dataitemSelected].fieldMappings=[];
													if(event.chart.stockChart.dataSets && event.chart.stockChart.dataSets[0] && event.chart.stockChart.dataSets[0].fieldMappings)
													event.chart.stockChart.filteredDataSets[ event.target.dataitemSelected].fieldMappings=event.chart.stockChart.dataSets[0].fieldMappings;
													else
													console.log("field mappings not available")
													event.chart.stockChart.mainDataSet = event.chart.stockChart.filteredDataSets[ event.target.dataitemSelected ];
													event.chart.stockChart.validateNow();
													event.chart.stockChart.periodSelector.setDefaultPeriod()
												}}]
									}
								}],
								"chartScrollbarSettings": {
									"graph": "g1"
								},
								"chartCursorSettings": {
									"valueBalloonsEnabled": true,
									"graphBulletSize": 1,
									"valueLineBalloonEnabled": true,
									"valueLineEnabled": true,
									"valueLineAlpha": 0.5
								},

								"periodSelector": {
									selectFromStart:true,
									position: "top",
									inputFieldsEnabled: true,
									addDatePicker:function(g,c,p){
                                    console.log("calling date selector")
										var _g=g;
										var datepicker=document.createElement("span");
										datepicker.style='padding:3px';
										datepicker.className="amChartsButton "+c+"-period-input";
										var id1=document.createElement("i");
										id1.className="fa fa-calendar";
										var space=document.createTextNode("  ");
										var id2=document.createElement("i");
										id2.className="fa fa-angle-down";
										datepicker.appendChild(id1);
										datepicker.appendChild(space);
										datepicker.appendChild(id2);

										datepicker.addEventListener("click",function(){
											if(_g.style.display=='none')
											{

												_g.style="top:10%;background-color:#fff;position: absolute;box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);" +
													"padding: 12px 25px;z-index: 1;border: 1px solid rgba(0, 0, 0, 0.15);border-radius: 0.25rem;";
												_g.style.display="block";
											}
											else
												_g.style.display='none';
										});

										p.appendChild(datepicker);

										var spanShare=document.createElement("span");
										spanShare.setAttribute("style","padding-left:5px");
										spanShare.className='graphShare';
										var aShare=document.createElement("a");


										var share=document.createElement("i");
										share.className="fa fa-share-alt";
										var shareTextNode=document.createTextNode(" Share");

										aShare.appendChild(share);
										aShare.appendChild(shareTextNode);

										spanShare.appendChild(aShare);



										var shareItems=document.createElement("div");
										shareItems.style.display="none";

										var fShare=document.createElement("a");
										fShare.className="btn btn-social-icon btn-facebook";
										fShare.style="background-color:#4267b2;color:#fff;border-radius:0px";
										var fspan=document.createElement("span");
										fspan.className="fa fa-facebook";
										fShare.append(fspan);
										shareItems.appendChild(fShare);

										var tShare=document.createElement("a");
										tShare.className="btn btn-social-icon btn-twitter";
										tShare.style="background-color:#1c94e0;color:#fff;border-radius:0px";
										var tspan=document.createElement("span");
										tspan.className="fa fa-twitter";
										tShare.append(tspan);
										shareItems.appendChild(tShare);

										var gShare=document.createElement("a");
										gShare.className="btn btn-social-icon btn-google";
										gShare.style='color:#fff;background-color:#d14836;border-radius:0px'
										var gspan=document.createElement("span");
										gspan.className="fa fa-google";
										gShare.append(gspan);
										shareItems.appendChild(gShare);

										var lShare=document.createElement("a");
										lShare.className="btn btn-social-icon btn-linkedin";
										lShare.style='color:#fff;background-color:#0084bf;border-radius:0px'
										var lspan=document.createElement("span");
										lspan.className="fa fa-linkedin";
										lShare.append(lspan);
										shareItems.appendChild(lShare);


										var eShare=document.createElement("a");
										eShare.className="btn btn-social-icon";
										eShare.style="border-color:black;border-radius:0px";
										var espan=document.createElement("span");
										espan.className="fa fa-envelope";
										eShare.append(espan);
										shareItems.appendChild(eShare);

										//spanShare.appendChild(shareItems);
										spanShare.addEventListener("click",function(){
											if(shareItems.style.display=='none')
											{
												shareItems.style="top:10%;background-color:#fff;position: absolute;box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);" +
													"padding: 12px 25px;z-index: 1;left:300px;border: 1px solid rgba(0, 0, 0, 0.15)";
												shareItems.style.display="block";
											}
											else
												shareItems.style.display='none';
										});

										//spanShare.addEventListener("mouseout",function(){
										//		shareItems.style.display='none';
										//});

										p.appendChild(spanShare);
										p.appendChild(shareItems);
									},
									"periods": [{
										period: "MM",
										count: 1,
										label: "1M"
									}, {
										period: "MM",
										count: 3,
										label: "3M"
									}, {
										period: "MM",
										count: 6,
										label: "6M"
									}, {
										period: "YYYY",
										count: 1,
										label: "1Y"
									}, {
										period: "YYYY",
										count: 2,
										label: "2Y"
									}, {
										period: "YYYY",
										count: 5,
										label: "5Y"
									}, {
										period: "MAX",
										label: "MAX",
										selected: true
									}]
								},

								"panelsSettings": {
									"usePrefixes": true
								},
								"legendSettings":{
									"textClickEnabled":true
								},
								"export": {
									"enabled": false
								},
								"filteredDataSets": filteredDataset,
								"thresholdValues": threshold
							};

							if (dataset && dataset.length > 1) {
								stockPanel.dataSetSelector = {
									"position": "top"
								};
							}

							var e = window.AmCharts;
							e.StockLegend = e.Class({
								inherits: e.AmLegend,
								construct:function(a){
									e.StockLegend.base.construct.call(this,a);
									this.cname="StockLegend";
									},
								drawLegend: function () {
									var a=this;
									var d=window.AmCharts;
									var b=a.chart;
									this.div.setAttribute("class","amChartsLegend amcharts-legend-div");
									if (scope.data.filters) {
										var ct=a;
										var ul = document.createElement('ul');
										ul.className = 'party';

										for (var i = 0; i < scope.data.filters.length; i++) {
											var li = document.createElement('li');
											var input = document.createElement('input');

											input.setAttribute('id', 'filter_' + scope.data.filters[i].id);
											input.setAttribute('type', "radio");
											input.setAttribute('name', "party");
											input.setAttribute('dataitem', i);
											if(ct.dataitemSelected==i)
											input.setAttribute("checked",true);

											input.addEventListener("click",function(a,b,b1){
												this.checked=true;
												ct.dataitemSelected=this.getAttribute("dataitem");
												ct.dispatch("clickLabel",ct);
											});

											var label = document.createElement('label');
											label.textContent = scope.data.filters[i].name;
											label.setAttribute('for', 'filter_' + scope.data.filters[i].id);
											label.setAttribute('dataitem', i);

											label.addEventListener("click",function(a,b,b1){
												this.selected=true;
												ct.dataitemSelected=this.getAttribute("dataitem");
												ct.dispatch("clickLabel",ct);
											});

											li.appendChild(input);
											li.appendChild(label);
											ul.appendChild(li);
										}

										this.div.appendChild(ul);

									} else if (scope.data.keyIndicators) {
										var ct=a;
										var ul = document.createElement('ul');
										ul.className = 'black_list';

										for (var i = 0; i < scope.data.keyIndicators.length; i++) {
											var li = document.createElement('li');
											var anchor = document.createElement('a');
											anchor.setAttribute("href","#/ga");
											anchor.innerText= scope.data.keyIndicators[i].name;
											anchor.setAttribute('dataitem', i);

											if(ct.dataitemSelected==i ||(!ct.dataitemSelected && i==0))
												anchor.setAttribute("class",anchor.className+" active ");


											anchor.addEventListener("click",function(a,b,b1){
												ct.dataitemSelected=this.getAttribute("dataitem");
												ct.dispatch("clickLabel",ct);
											});

											li.appendChild(anchor);
											ul.appendChild(li);
										}

										this.div.appendChild(ul);
									}
								}
							});

							var chart = e.makeChart(attrs.id, stockPanel);
						}
						$timeout(initChart, 3000);
					});
				}
			}

		}]);
