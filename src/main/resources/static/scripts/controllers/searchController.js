'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:GA
 * @description # GACtrl Controller of the govrnnApp
 */
angular.module('govrnnApp').controller(
    'SearchCtrl',
    [ '$http','$location', '$scope', '$rootScope', '$routeParams', '$sce', '$timeout','$route', 'GAService', 'IndicatorService',
        function($http,$location, $scope, $rootScope, $routeParams, $sce, $timeout,$route ,GAService, IndicatorService) {
            $scope.Math=window.Math;
            $scope.searchTerm="";
            $scope.searchResults="";

            $scope.isFetching=false;
            //$scope.$watch('searchTerm',function(oldValue,newValue){
            //    if(! ($scope.isFetching)){
            //        GAService.search(newValue).then(function(response){
            //            $scope.results=response;
            //            $scope.isFetching=false;
            //        });
            //    }
            //
            //});


            $scope.st="";
            if($route.current.params && $route.current.params.q){
                $scope.st=$route.current.params.q;
                $scope.searchTerm=$route.current.params.q;
            }

            $scope.showSearchResults=false;


            $scope.getSuggestions = function (searchTerm) {
                if (searchTerm.length < 2) {
                    $scope.searchResults = [];
                    return;
                }
                GAService.search(searchTerm).then(function(result) {
                        jQuery("#search_indicator").addClass("open");
                        $scope.showSearchResults=true;
                        $scope.searchResults = result;

                    },
                    function(result) {
                        $scope.showSearchResults=false;
                        $scope.searchResults = [];
                    });
            }


            if($route.current.params && $route.current.params.q){
                    GAService.search($route.current.params.q).then(function(response){
                        $scope.results=response;
                        $scope.isFetching=false;
                    });


            }

            $scope.search=function(term){

            }
        }]);
