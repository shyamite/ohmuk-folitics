'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:GA
 * @description # GACtrl Controller of the govrnnApp
 */
angular.module('govrnnApp').controller(
    'KeyIndicatorsCtrl',
    [ '$http','$location', '$scope', '$rootScope', '$routeParams', '$sce', '$timeout','$route', 'IndicatorService','GAService','AdminService',
        function($http,$location, $scope, $rootScope, $routeParams, $sce, $timeout,$route , IndicatorService,GAService,AdminService) {

    $scope.selectedFaIndex = 0;
    $scope.selectedIndicators = [];
    $scope.selectedKeyIndicators = [];
    $scope.keyIndicators = [
        []
    ];
    $scope.indicators = [
        []
    ];
            GAService.getActiveCategories().then(function(response) {
                $scope.categories = response.data.messages;
            });

            AdminService.listCategories().then(function(response){
                //$scope.
            });

            $scope.category={};

                $scope.fetchSubCategories=function(catId) {
                    GAService.getSubCatAndIndByCatId({
                        categoryId: catId
                    }).then(function (response) {
                        $scope.subcategories = response.data.messages[0].childs;
                    });
                }

            $scope.fetchIndicators=function(subcatId) {
                GAService.getIndicatorsBySubCatID({
                    subCategoryId: subcatId
                }).then(function (response) {
                    $scope.indicators = [];
                    $scope.keyIndicators = [];
                    for (var i=0;i<response.data.messages[0].childs.length;i++){
                        if(response.data.messages[0].childs[i].keyIndicator)
                        $scope.keyIndicators.push(response.data.messages[0].childs[i])
                        else
                            $scope.indicators.push(response.data.messages[0].childs[i])
                    }
                });
            }

            $scope.fetchKeyIndicators=function(subcatId) {
                GAService.getIndicatorsBySubCatID({
                    subCategoryId: subcatId
                }).then(function (response) {
                    $scope.indicators = response.data.messages[0].childs;
                });
            }



            $scope.moveToKeyIndicators = function () {
            //move selected.
            angular.forEach($scope.selectedIndicators, function (value, key) {
                    this.push(value);
            }, $scope.keyIndicators);

            //remove the ones that were moved.
            angular.forEach($scope.selectedIndicators, function (value, key) {

                AdminService.addKeyIndicator(value.id).then(function success() {
                });
                for (var i = $scope.indicators.length - 1; i >= 0; i--) {
                    if ($scope.indicators[i].id == value.id) {
                        $scope.indicators.splice(i, 1);
                    }
                }
            });
            $scope.selectedIndicators = [];
    };

    $scope.removeFromKeyIndicators = function () {

            angular.forEach($scope.selectedKeyIndicators, function (value, key) {
                    this.push(value);
            }, $scope.indicators);

            //remove the ones that were moved from the source container.
            angular.forEach($scope.selectedKeyIndicators, function (value, key) {
                AdminService.removeKeyIndicator(value.id).then(function success(){
                });
                for (var i = $scope.keyIndicators.length - 1; i >= 0; i--) {
                    if ($scope.keyIndicators[i].id == value.id) {
                        $scope.keyIndicators.splice(i, 1);
                    }
                }
            });
            $scope.selectedKeyIndicators = [];
    };
}]);