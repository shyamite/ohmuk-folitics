'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:GA
 * @description # GACtrl Controller of the govrnnApp
 */
angular.module('govrnnApp').controller(
		'AdminCtrl',
		[ '$http', '$scope', '$rootScope', '$routeParams', '$sce', '$timeout', 'AdminService','IndicatorService','GAService',
				function($http, $scope, $rootScope, $routeParams, $sce, $timeout, AdminService,IndicatorService,GAService) {


					$scope.graphsegments=["GA","GPI","Feel"];
					$scope.indicators=[];
					$scope.indicatorObj={};
					$scope.indicatorObj.categoryID='';
					$scope.indicatorObj.score='';

					IndicatorService.getActiveIndicators().then(function(response) {
						$scope.indicators=response.messages;
					});

					IndicatorService.getActiveCategories().then(function(response) {
						$scope.categories=response.data.messages;
					});

					$scope.fetchSubCategories=function(categoryId){
						IndicatorService.getSubCategories({categoryId:categoryId}).then(function(response){
							$scope.subcategories = response.data.messages[0].childs;
						});
					},

					$scope.saveIndicator=function(indicatorObj){
						var indicator={};
						indicator.id=indicatorObj.id;
						indicator.score=indicatorObj.score;
						var ct=new Date(indicatorObj.createTime);
						var efd=new Date(indicatorObj.effectfromdate);
						indicator.createTime=ct.getTime();
						indicator.effectfromdate=efd.getTime();
						indicator.indicatorvalue=indicatorObj.score;
						IndicatorService.saveIndicator(indicator).then(function(response) {
							if(response.success==true)
								jQuery.notify("Indicator values updated", "success");

							else
								jQuery.notify("Unable to save Indicator values", "error");

						});

					}

					$scope.saveCategory=function(category){
						category.createdBy=0;
						category.editTime=new Date().getTime();
						category.createTime=new Date().getTime();
						IndicatorService.saveCategory(category).then(function(response) {
							if(response.success==true)
								jQuery.notify("Succesfully added category", "success");

							else
								jQuery.notify("Unable to add category.", "error");

						});

					}
				}
		]
);
