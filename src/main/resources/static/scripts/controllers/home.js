'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the govrnnApp
 */
angular.module('govrnnApp')
	.controller('HomeCtrl', ['$http', '$scope', '$rootScope', '$routeParams','SentimentService', 'HomeService','$sce',
		function ($http, $scope, $rootScope, $routeParams, SentimentService, HomeService,$sce) {

			$rootScope.id = 0,
			$scope.newsFlash = "Gulberg massacre verdict: 11 awarded life, 12 others to be jailed";
			$scope.tab = 1;
			$scope.emojiTab=0;
			$scope.clickedtab;
			var sentiments= $scope.sentiments = [];
			$scope.selectedSentiment = {};
			$scope.isSentimentClicked= false;
			$scope.isRelatedClicked=false;
			$scope.ifRelSentiment=true;
			$scope.isAggregationPresent=false;
			$scope.rateSentiment= {};
			$scope.temporarySentimentsSets = [];
			$scope.permanentSentimentsSets = [];
			$scope.relatedSentimentsSets=[];
			$scope.pollSets=[];
			$scope.rateNews= {};
			$scope.rateRef= {};
			$scope.refValidation="";
			$scope.newsValidation="";
			$scope.searchValue="";
			$scope.searchValues="";
			$scope.opinionOutputModelsSets =[];
			/*$scope.loggedUserId=$scope.$storage.sessionData.user_id;*/
				
			  $http.get("/opinion/getLatestOpinions?count=3").then(function(response){
				  if(response.data.messages != null){
					    var opinionOutputModels = response.data.messages[0];
					    for(var i =0;i<response.data.messages[0].length;i++){
					    	opinionOutputModels[i].opinionText = $sce.trustAsHtml(opinionOutputModels[i].text);
							$scope.opinionOutputModelsSets .push(opinionOutputModels[i]);
					    }
				  }
			  });
				
				$http.get("/user/getLatestUsers?count=5").then(function(response){
					  if(response.data.messages != null){
						    $scope.latestUsers = response.data.messages[0];
						    
						    $("#carousel-latestuser").carousel({interval: 3000});   
					  }
				  });
			  
			  $http
				.get('/trend/getTopTrends?count=5')
				.then(function(response) {
					if (response.data.success) {
						$scope.latestTrends = response.data.messages[0];
						for (var i = 0; i < $scope.latestTrends.length; i++) {
							$scope.latestTrends[i].title = '#'+$scope.latestTrends[i].title;
						}
					}
				});
			//====================GET TOP TRENDS=====================================		
			$scope.topTrends = function(count) {
				$http
					.get('/trend/getTopTrends?count='+count)
					.then(
						function(response) {
							if (response.data.success) {
								$scope.topTrends = response.data.messages[0];
								for (var i = 0; i < $scope.topTrends.length; i++) {
									//console.log(topTrends[i].title);
								}
							}
						});
			};

			//====================GET NEWS=====================================		
			  $http
				.get('/dailyNews/getDailyNews')
				.then(function(response) {
					if (response.data.success) {
						$scope.dailyNews = response.data.messages[0];
						for (var i = 0; i < $scope.dailyNews.length; i++) {
							$scope.dailyNews[i].title = $scope.dailyNews[i].title;
						}
					}
				});
			
			/*=====================================RATING APIS==============================================*/

			$scope.rate= function(componentType, attrid, parentId, rate, key){
				if($scope.isUserLoggedIn() == true){
				$scope.rateSentiment.componentType= componentType;
				$scope.rateSentiment.componentId= attrid;
				$scope.rateSentiment.parentId= parentId;
				$scope.rateSentiment.rating = rate;
				$scope.rateSentiment.userId = $scope.$storage.sessionData.user_id;
				//alert(JSON.stringify($scope.rateSentiment));
				if(componentType=="reference"){

					$scope.rateRef.componentType= "reference";
					$scope.rateRef.parentId= parentId;
					$scope.rateRef.userId = $scope.$storage.sessionData.user_id;;

					$http({
						method: 'POST',
						url: "/rating/getRating",
						headers: {'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
						data:  JSON.stringify($scope.rateRef)
					}).then(function successCallback(response) {
						$scope.refList=response.data.messages[0];
						//alert(JSON.stringify(response.data.messages[0]));
						if($scope.refList.length != 0){
							for(var i=0; i<$scope.refList.length;i++){
								//alert(JSON.stringify($scope.refList[i].componentId));
								if($scope.refList[i].componentId == $scope.rateSentiment.componentId && $scope.refList[i].rating == $scope.rateSentiment.rating){
									//$('#rate_'+rate+attrid).css("background", "");
									$scope.refValidation=0;
									//alert("matched"+$scope.refValidation);
									break;
								}else{
									$scope.refValidation=1;
								}
							}}else{
							$scope.refValidation=2;
						}

						if($scope.refValidation>0){

							HomeService.saveRating($scope.rateSentiment);
							//$('#rate_'+rate+attrid).css("background", "red");
							$("#refImg_"+attrid).attr("src","/images/"+key+".png");
						}else{
							//alert($scope.refValidation);
							$('#rate_'+rate+attrid).css("background", "");
							$("#refImg_"+attrid).attr("src","/images/feel.png");//smiley.jpg
							HomeService.deleteRating($scope.rateSentiment);
						}

					}, function errorCallback(response) {
						console.log(response);
						return {};

					});

				}
				else{
					//alert("inside else");
					$scope.rateNews.componentType= "sentimentNews";
					//alert(parentId);
					$scope.rateNews.parentId= parentId;
					$scope.rateNews.userId = $scope.$storage.sessionData.user_id;;

					$http({
						method: 'POST',
						url: "/rating/getRating",
						headers: {'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
						data:  JSON.stringify($scope.rateNews)
					}).then(function successCallback(response) {
						$scope.newsList= response.data.messages[0];
						//alert(JSON.stringify(response.data.messages));
						if($scope.newsList.length != 0){
							for(var i=0; i<$scope.newsList.length;i++){
								//alert(JSON.stringify($scope.newsList[i].componentId));
								if($scope.newsList[i].componentId == $scope.rateSentiment.componentId && $scope.newsList[i].rating == $scope.rateSentiment.rating){
									$scope.newsValidation=0;
									//alert("matched"+$scope.newsValidation);
									break;

								}else{
									$scope.newsValidation=1;
								}
							}}else{
							$scope.newsValidation=2;
						}

						if($scope.newsValidation>0){

							$("#newsImg_"+attrid).attr("src","/images/"+key+".png");
							HomeService.saveRating($scope.rateSentiment);
						}else{
							//alert($scope.newsValidation);
							$('#rate_'+rate+attrid).css("background", "");
							$("#newsImg_"+attrid).attr("src","/images/feel.png");//smiley-1
							HomeService.deleteRating($scope.rateSentiment);
						}
					}, function errorCallback(response) {
						console.log(response);
						return {};

					});
				}
				}else{
					$scope.loginNow();
				}
			};
			/*$http.get('/poll/getPollAggregationBean?pollId=7').then(
			 function(response){
			 $scope.voteResp = response.data.messages[0];
			 //alert();
			 for(var i=0; i<$scope.voteResp.length;i++)
			 alert($scope.voteResp[i].percentage);
			 });*/


			//====================DEFAULT POLL VIEW(AS PER USER PREV SESSION)=====================================		
			$scope.pollAnsAsPerUser= function(id, polls){
				//console.log(polls);
				if($scope.isUserLoggedIn() == true){
				$http.get('/poll/getAllPollOptionBySentimentBean?sentimentId='+id+'&userId='+$scope.$storage.sessionData.user_id).then(
				function(response){
					$scope.pollResp = response.data.messages[0];
					//alert(JSOG.stringify($scope.pollResp));
					for(var i=0; i<$scope.pollResp.length;i++){
						//alert($scope.pollResp[i].pollOptionId);
							
						if($scope.pollResp[i].pollOptionId){
							$('#poll_'+$scope.pollResp[i].pollOptionId).attr('checked','true');
							//alert("#poll_"+$scope.pollResp[i].pollOptionId);
													
						}else{
							//alert();
							}


						}
					});
				}
			};

			//$scope.lineShow=false;
			/*======================================================POLL VOTE API========================================	*/
		
			$scope.pollAgrregation=function(pollId){
				//alert("inside poll aggregation"+pollId);
				$http.get('/poll/getPollAggregationBean?pollId='+pollId).then(
					function(response){
						$scope.voteResp = response.data.messages[0];
						//alert(JSOG.stringify($scope.voteResp));
						for(var i=0; i<$scope.voteResp.length; i++){
							$('#agre_'+$scope.voteResp[i].pollOptionId+"_"+pollId).css("display","block");
							//$('#agre_'+$scope.voteResp[i].pollOptionId+"_"+pollId).css("background","black");
							//$('#poll_out'+pollId+'_'+$scope.voteResp[i].pollOptionId).css("display","block");
							var redper=$scope.voteResp[i].percentage;
							var tran=(100-$scope.voteResp[i].percentage);
							$('#pollOpn_'+$scope.voteResp[i].pollOptionId).css("display","block");
							if(redper>0){
							$('#pollOpn_'+$scope.voteResp[i].pollOptionId).css("background","linear-gradient(90deg, red "+redper+"%, transparent "+tran+"%)");
							}else{
								$('#pollOpn_'+$scope.voteResp[i].pollOptionId).css("background","transparent 100%");
							}
						}
					});

			};
			/*	=========================VOTE API================================================*/
			$scope.vote= function(pollId,pollOptnId,compType){
				if($scope.isUserLoggedIn() == true){
					//$('#poll_'+pollOptnId).attr('checked','true');
				$http.get('/poll/getPollOption?pollId='+pollId+'&userId='+$scope.$storage.sessionData.user_id).then(
					function(response){
						//alert("inside"+pollOptnId);
						$scope.voteRes = JSOG.parse(JSOG.stringify(response.data.messages[0]));
						//alert($scope.voteRes);
						if($scope.voteRes==pollOptnId){
							$scope.unSelectVote(pollOptnId, pollId);//Long pollOptionId, Long userId
						}else if($scope.voteRes==""){
							$scope.selectVote(pollOptnId,compType, pollId );
						}	//pollOptionId, userId, componentType, componentId
						else{

							$scope.selectVote(pollOptnId,compType, pollId );
						}
					});
			}else{
				$scope.loginNow();
			}
			};


			$scope.selectVote= function(pollOptnId,compType, pollId ){
				//pollOptionId=13&userId=2&componentType=Sentiment&componentId=7
				$http.post('/poll/selectVote?pollOptionId='+ pollOptnId +'&userId='+$scope.$storage.sessionData.user_id +'&componentType='+compType +'&componentId='+pollId).then(
					function(response){					
						$scope.voteResSelect = JSOG.parse(JSOG.stringify(response.data.messages[0]));
						$scope.pollAgrregation(pollId);
					});
			};

			$scope.unSelectVote= function(pollOptnId, pollId){
				$http.post('/poll/unSelectVote?pollOptionId='+ pollOptnId +'&userId='+$scope.$storage.sessionData.user_id).then(
					function(response){
						$scope.voteResUnselect = JSOG.parse(JSOG.stringify(response.data.messages[0]));
						$scope.pollAgrregation(pollId);
					});
			};


			/*==========================GET ALL SENTIMENT==================================*/
			/*$http.get('/sentiment/getAll').then(
				function(response) {
					console.log('hello');
					$scope.sentiments = JSOG.parse(JSOG
						.stringify(response.data.messages));
				});
			*/


			/*=============================DISPLAY SENTIMENT -sentiment Description tab========================================*/

			$scope.isClicked = function(tabNum){
				return $scope.clickedtab === tabNum;
				//console.log($scope.clickedtab);
			};

			$scope.displaySentiment = function(newTab, id, sentiType){
				for(var key in $scope.permanentSentiments){
					$("#permanent_"+key).removeClass("active");}
				for(var key in $scope.temporarySentiments){
					$("#current_"+key).removeClass("active");}
				$( "#carousel-sentiment .item ul li .box_sentiment" ).removeClass('active');
				$( "#carousel-sentiment-perm .item ul li .box_sentiment" ).removeClass('active');
				$('#'+sentiType+'_'+id).addClass('active');
				//alert(id);
                pauseSlides();
                $( ".show_onclick" ).show();
				$( ".collapse_sec" ).hide();
				$('html, body').animate({scrollTop:460}, 'slow');
		
				/*if(sentiType == "permanent"){
                	//background-position:0 0; background:url("../images/sentiment_gif.gif")
                	//$("#verdict_per_"+newTab).css("background-position","0 0");
               // $("#verdict_per_"+newTab).css("background","url('../images/sentiment_gif.gif')");
                	//alert("permanent");
                	//$("#verdict_per"+newTab).addClass("active");
                	
                	$("#current_"+newTab).removeClass('box_sentiment active');//removeClass("active");
                	$("#current_"+newTab).css("width", "100%"); 
					$("#current_"+newTab).css("border-radius", "4px"); 
					$("#current_"+newTab).css("-moz-border-radius", "4px"); 
					$("#current_"+newTab).css("border","1px solid #ddd"); 
					$("#current_"+newTab).css("padding","5px"); 
					$("#current_"+newTab).css("min-height","208px");
				}else{
					
					$("#permanent_"+newTab).removeClass("box_sentiment active");
					$("#permanent_"+newTab).css("width", "100%"); 
					$("#permanent_"+newTab).css("border-radius", "4px"); 
					$("#permanent_"+newTab).css("-moz-border-radius", "4px"); 
					$("#permanent_"+newTab).css("border","1px solid #ddd"); 
					$("#permanent_"+newTab).css("padding","5px"); 
					$("#permanent_"+newTab).css("min-height","208px");
				
				}*/
                
				$http.get('/sentiment/findSentiment?id='+id).then(
					function(response) {
						if (response.data.messages != null) {
							$scope.selectedSentiment = response.data.messages[0];
							$scope.clickedtab = newTab;
							$scope.isSentimentClicked = true;
							$scope.aggregation(id);
							$scope.userRate(id);
							$scope.pollAnsAsPerUser(id, $scope.selectedSentiment.polls);
							$scope.references= $scope.selectedSentiment.attachments;
							$scope.relatedSentiments= $scope.selectedSentiment.relatedSentiments;
							//$scope.ifRelSentiment=true;
							var data = $scope.relatedSentiments;
							var relatedSentimentLength = data.length;

							for (var i = 0; i < relatedSentimentLength; i += 6) {
								var newobj = {};
								for(var j=i; j< (i+6); j++){
									if(data[j]){
										newobj[j]= data[j];
									}
								}
								$scope.relatedSentimentsSets.push(newobj);
								console.log($scope.relatedSentimentsSets);
								
							}
							 $("#carousel-related").carousel({interval: 3000, pause: "hover"}); 
							 $("#carousel-poll").carousel({interval: 3000, pause: "hover"}); 

						}
					});

			};

//===========================================userDefaultView=======================================

			$scope.userRefRate={};
			$scope.userNewsRate={};
			$scope.userRate= function(id){
				$scope.userRefRate.componentType="reference";
				$scope.userRefRate.parentId = id;
				if($scope.isUserLoggedIn() == true){
				$scope.userRefRate.userId = $scope.$storage.sessionData.user_id;
				}
				$scope.userNewsRate.componentType="sentimentNews";
				$scope.userNewsRate.parentId = id;
				if($scope.isUserLoggedIn() == true){
				$scope.userNewsRate.userId = $scope.$storage.sessionData.user_id;}
				$scope.allNewsRating = HomeService.getAllRating($scope.userNewsRate, "sentimentNews");
				$scope.allRefRating= HomeService.getAllRating($scope.userRefRate, "reference");
				//alert($scope.allRefRating);
			}

			/*$scope.highlight= function(allRating){
			 alert("came");
			 alert(allRating.length);
			 return;
			 }*/

//===============================================aggregation API=================================


			$scope.aggregation = function(id){
				//alert(id);
				$http.get('/opinion/getOpinionAggregation?sentimentId='+id).then(
					function(response){
						//if(response.data.success){
						$scope.aggregate = response.data.messages;
						//alert(JSOG.stringify($scope.aggregate));
						if($scope.aggregate!=null){
							$scope.isAggregationPresent=false;
						$scope.supportPercentage=(($scope.aggregate[0].support)/($scope.aggregate[0].support+$scope.aggregate[0].against))*100;
						$scope.againstPercentage=(($scope.aggregate[0].against)/($scope.aggregate[0].support+$scope.aggregate[0].against))*100;
						$scope.myJson = {
							globals: {
								shadow: false,
								fontFamily: "Verdana",
								fontWeight: "100"
							},
							type: "pie",
							backgroundColor: "#fff",

							legend: {

								layout: "x5",
								position: "none",
								borderColor: "transparent",
								marker: {
									borderRadius: 10,
									borderColor: "transparent"
								}
							},
							tooltip: {
								text: "%v"+"%"+ "%t"
							},
							plot: {
								refAngle: "-90",
								borderWidth: "5px",
								valueBox: {
									placement: "in",
									text: "",
									fontSize: "15px",
									textAlpha: 1,
								}
							},
							series:[
								{
									text: "support",
									values: [$scope.supportPercentage],
									backgroundColor: "#ff7305",
								}, {
									text: "against",
									values: [$scope.againstPercentage],
									backgroundColor: "#128807"
								}]
						};
						}
						else {
							//alert("null");
							$scope.isAggregationPresent=true;
						}
					});
					};
				

			/*--------------------------------------tab section of current/permanent----------------------------------*/
			$scope.setTab = function(newTab){
				$scope.tab = newTab;
			};

			$scope.isSet = function(tabNum){
				return $scope.tab === tabNum;
			};


			/*=========================================permanent/current sentiment==========================================*/

			$scope.prev = function(slide) {
				$(slide).carousel("prev");
			};
			$scope.next = function(slide) {
				$(slide).carousel("next");
			};
			var pauseSlides = function(){
				$("#carousel-sentiment").carousel("pause");
				$("#carousel-sentiment-perma").carousel("pause");
				$("#carousel-related").carousel("pause");
				$("#carousel-poll").carousel("pause");
			};


			SentimentService.findByTypePermanent().then(function(response) {
				//console.log('called per');

				$scope.permanentSentiments = response;
				var data = response;
				var sentimentLength = data.length;

				for (var i = 0; i < sentimentLength; i += 8) {
					//console.log(i);
					var newobj = {};
					for(var j=i; j< (i+8); j++){
						if(data[j]){
							newobj[j]= data[j];
						}
					}
					$scope.permanentSentimentsSets.push(newobj);
				}
				$("#carousel-sentiment-perma").carousel({interval: 3000, pause: "hover"});
				//$("#carousel-sentiment").carousel({interval: 3000, pause: "hover"});
			});
			SentimentService.findByTypeTemporary().then(function(response) {
				//console.log('called temporary');

				$scope.temporarySentiments = response;
				var data = response;
				var sentimentLength = data.length;

				for (var i = 0; i < sentimentLength; i += 8) {
					var newobj = {};
					for(var j=i; j< (i+8); j++){
						if(data[j]){
							newobj[j]= data[j];
						}
					}
					$scope.temporarySentimentsSets.push(newobj);
				}
				$("#carousel-sentiment").carousel({interval: 5000, pause: "hover"});
			});

//========================================relatedSentiment onClick event==================================================

			$scope.displayRelatedSentiment = function(newTab, id){
				$http.get('/sentiment/findSentiment?id='+id).then(
					function(response) {
						if (response.data.messages != null) {
							$scope.sentiment = response.data.messages[0];
							$scope.isSentimentClicked = true;
							var type= $scope.sentiment.type;
							
												
													for(var key in $scope.permanentSentiments){
														$("#permanent_"+key).removeClass("active");}
													for(var key in $scope.temporarySentiments){
														$("#current_"+key).removeClass("active");}
													if(type=="Permanent"){
														$scope.displaySentiment(key, id, 'permanent');
													}
												else{
														$scope.displaySentiment(key, id, 'current' );
														
													}
												}
											
									});
	
						};
			
			
			
			/*===================================BY SENTIMENT ID IN THE URL========================================*/
			
			//$scope.temporarySentiments, $scope.permanentSentiments
						
						
			if($routeParams.sentimentId!=null){	
				//SentimentService.findById($routeParams.sentimentId);
				$scope.sentitId=$routeParams.sentimentId;
				$http.get('/sentiment/findSentiment?id='+$scope.sentitId).then(
						function(response) {
							if (response.data.messages != null) {
								var type=response.data.messages[0].type;
								//alert(type);
									$scope.tempy = $http.get('/sentiment/getSentimentList?type='+type).then(
											function(response) {
												if (response.data != null) {
													for(var i=0; i<response.data.messages.length;i++){
														if($scope.sentitId==response.data.messages[i].id){
															//$('#current_'+i).prop('clicked', true);
															if(type=="Temporary"){
															$scope.tab=1;
															$("#current_"+$scope.sentitId).addClass("active");
															$scope.displaySentiment(i, $scope.sentitId, 'current' );
															}else{
																$scope.tab=2;
																$("#permanent"+$scope.sentitId).addClass("active");
																$scope.displaySentiment(i, $scope.sentitId, 'permanent' );
															}
														}
													}
													/*alert(response.data.messages[0].id);
													return response.data.messages;*/
												}
											});
								
								return response.data.messages[0];	
							}
						});
		
			};
			
	/*===================================/BY SENTIMENT ID IN THE URL========================================*/	
			
			/*if($scope.isUserLoggedIn() == true){
				$
			}else{
			 $scope.loginNow();
			}*/	
	/*============================IMAGE MODAL ONCLICK EVENT OF SENTIMENT IMAGE=======================================*/		
			$scope.showImage=function(){
			$('#sentimentPic_Modal').appendTo("body").modal('show');
			};
			/*==========================URLIVE IMPLEMENTATION FOR NEWS PREVIEW==========================================*/
			/*	$('#news_img').urlive({
			 imageSize: 'small';
			 });*/
			$scope.pollSlider= function(inx){
				$(slide).carousel('inx');
			}
			
			$scope.stylePoll=function(pollId, inx){
				//alert();
				//$('#p_1_'+pollId).attr('checked',true);
				//$('#p_1_'+pollId).prop('checked',true);
				$('#p_1_'+pollId).css('background','red');
				$('#p_1_'+pollId).attr('data-target','#carousel-poll');
				$('#p_1_'+pollId).attr('data-slide-to',inx);
			}
			
		
		$( ".toggle_txt ul li.last a" ).click(function() {
			$( ".collapse_sec" ).show();
			$( ".show_onclick" ).hide();
		});
	 
		}])
	.filter('trustAsResourceUrl', ['$sce', function($sce) {
		return function(val) {
			return $sce.trustAsResourceUrl(val);
		};



	}]);