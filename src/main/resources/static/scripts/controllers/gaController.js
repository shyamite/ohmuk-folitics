'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:GA
 * @description # GACtrl Controller of the govrnnApp
 */
angular.module('govrnnApp').controller(
		'GACtrl',
		[ '$http','$location', '$scope', '$rootScope', '$routeParams', '$sce', '$timeout','$route', 'GAService', 'IndicatorService',
				function($http,$location, $scope, $rootScope, $routeParams, $sce, $timeout,$route ,GAService, IndicatorService) {
					$scope.graphdata = [];
					$scope.Math=window.Math;
					$scope.primaryIndicatorTitle="";
					$scope.primaryIndicator=[];
					$scope.submitFact={};
					$scope.submitFact.componentType= "govtSchemeData";
					$scope.submitFact.componentId=1;
					$scope.submitFact.userId=0;
					$scope.submitFact.comment="";
					$scope.activeIndicatorName="";
					$scope.activeCategory=0;
					$scope.activeSubcategory=0;
					$scope.activeIndicator=0;

					$scope.searchResults=[];
					$scope.searchTerm='';
					$scope.showSearchResults=false;
					$scope.submitFactStatus=false;
					$scope.readmoreoptions = {
						speed: 500,
						collapsedHeight:100,
						moreLink: '<a href="javascript:void(0)" style="font-weight:bold">More...</a>',
						lessLink: '<a href="javascript:void(0)" style="font-weight:bold">Less</a>'
					};


					$scope.selectedIndicator=function(indicator,name){
						jQuery("#search_indicator").removeClass("open");
						jQuery("#searchbox").val(name);

						$scope.showSearchResults=false;
						$scope.searchTerm=name;
						$scope.activeIndicatorName=name;
						//$('#carousel').carousel(i);
							GAService.fetchCategoryDetailsByIndicatorId(indicator).then(function(response) {
								$scope.subcategories = response.data.result.subcategories[0];
								$scope.activeSubCategory=response.data.result.parent.id;
								$scope.activeSubCat=response.data.result.parent;
								$scope.activeIndicator=indicator;
								$scope.activeCategory=response.data.result.category.id;
								GAService.getIndicatorsBySubCatID({
									subCategoryId : $scope.activeSubCategory
								}).then(function(response) {
									$scope.indicators = response.data.messages[0];
									GAService.getChartData('prim-indicator',$scope.activeIndicator).then(function(response){
										$scope.graphdata=response;
										$scope.updateIndicatorStatus(response);
									});
									//GAService.getChartData('indicator',$scope.activeIndicator).then(function(response){
									//	$scope.graphdata=response;
									//});
								});
								//$scope.category=response.data.category;
								//$scope.indicator=response.data.indicato
							});

					};

					$scope.updateIndicatorStatus=function(response){
						if(response.status)
						$scope.indicatorStatus=(response.status=='Best Performance' || response.status=='On Track')?'indicatorstatus_good':'indicatorstatus_bad';
					}

					$scope.shareObj={};
					$scope.share=function(){
						GAService.share(shareObj);
						jQuery('#shareafriend').modal('toggle');
					}

					$scope.indicatorStatus="";
					$scope.updateIndicatorChart=function(indicatorId){
						jQuery(".radio").each(function(){
							if(this.value!=indicatorId){
								this.checked=false;
							}
						});
						GAService.getChartData('prim-indicator',indicatorId).then(function(response){
							$scope.graphdata=response;
							$scope.updateIndicatorStatus(response);
						});
						//GAService.getChartData('indicator',indicatorId).then(function(response){
						//	$scope.graphdata=response;
						//});
					}

				
					$scope.submitFactForSchema=function() {
						$scope.submitFact.componentId=1;
						$scope.submitFact.userId=1;
						GAService.submitFact($scope.submitFact).then(function (response) {
							jQuery('#fact').modal('toggle');
							$scope.submitFact={};
							alert("Thanks For submitting the fact.\nOur Team will review the facts before showing them.");
						});
					};


					GAService.getGovernmentSchemes().then(function(response) {
						$scope.govtSchemes=response;
					});

					if($routeParams.seo)
						$scope.activeIndicatorName=$routeParams.seo;
					if($routeParams.indicatorId){
						$scope.activeIndicator=$routeParams.indicatorId;
						$scope.selectedIndicator($scope.activeIndicator);
						GAService.getActiveCategories().then(function(response) {
							$scope.categories = response.data.messages;
						});
					}else{
						GAService.getActiveCategories().then(function(response) {
							$scope.activeCategory=  response.data.messages[0].id;
							$scope.categories = response.data.messages;
							console.log("Categories: " + $scope.categories.name);
							console.log("Categories id: " + $scope.categories[0].id);

							$timeout(function() {
								GAService.getSubCatAndIndByCatId({
									categoryId : $scope.categories[0].id
								}).then(function(response) {
									$scope.activeSubCategory=response.data.messages[0].childs[0].id
									$scope.activeSubCat=response.data.messages[0].childs[0];
									$scope.subcategories = response.data.messages[0];
									GAService.getIndicatorsBySubCatID({
										subCategoryId : $scope.subcategories.childs[0].id
									}).then(function(response) {
										$scope.indicators = response.data.messages[0];
										$scope.activeIndicator=$scope.indicators["childs"][0].id;
										$scope.activeIndicatorName=$scope.indicators["childs"][0].name
										//GAService.getChartData('prim-indicator',$scope.activeIndicator).then(function(response){
										//	$scope.graphdata=response;
										//});
										GAService.getChartData('indicator',$scope.activeIndicator).then(function(response){
											$scope.graphdata=response;
											$scope.updateIndicatorStatus(response);
										});
									});
								});
							}, 2000);
						});
					}
					$scope.scheme="";

					var SPACE=32,CARRIAGE_RETURN=13;
					$scope.searchGovtSchemes=function(searchTerm,$event){

						if($event.keyCode==SPACE || $event.keyCode==CARRIAGE_RETURN  ) {
							if(searchTerm.trim().length==0){
								GAService.getGovernmentSchemes().then(function(response) {
									$scope.govtSchemes=response;
								});
							}else {
								GAService.searchGovtSchemes(searchTerm).then(function (response) {
									$scope.govtSchemes = response;
								});
							}
						}
					},


					$scope.prev=function( carouselId){
						jQuery(carouselId).carousel('prev');
					},
					$scope.next=function(carouselId){
						jQuery(carouselId).carousel('next');
					},
					//$scope.$watch()


					$timeout(function() {
						GAService.getChartData('keyindicators').then(function(response){
									$scope.keyindicators=response;
								});
					}, 2000);

					$scope.showIndicator=function(indicatorId){
						console.log(indicatorId+">>>>>>>")
						GAService.getChartData('prim-indicator',indicatorId).then(function(response){
							$scope.primaryIndicatorTitle=response.datasets[0].title +" - "+response.subcategory;
							$scope.primaryIndicator=response.datasets;
							jQuery('#indicatorgraph').modal('show');
						});
					};


					$scope.selectSubCategories = function(categoryId) {
						$scope.activeCategory=categoryId;
						GAService.getSubCatAndIndByCatId({
							categoryId : categoryId
						}).then(function(response) {
							$scope.subcategories = response.data.messages[0];
							$scope.activeSubCategory=$scope.subcategories.childs[0].id;
							$scope.activeSubCat=$scope.subcategories.childs[0];
							console.log("Sub Categories: " + $scope.subcategories.childs[0].name);
							GAService.getIndicatorsBySubCatID({
								subCategoryId : $scope.subcategories.childs[0].id
							}).then(function(response) {
								$scope.indicators = response.data.messages[0];
								$scope.activeIndicator=$scope.indicators["childs"][0].id;
								$scope.activeIndicatorName=$scope.indicators["childs"][0].name

								GAService.getChartData('prim-indicator',$scope.activeIndicator).then(function(response){
									$scope.graphdata=response;
									$scope.updateIndicatorStatus(response);
								});

								//GAService.getChartData('indicator',$scope.activeIndicator).then(function(response){
								//	$scope.graphdata=response;
								//});
							});
						});
					}

					$scope.selectIndicators = function(subCategoryId) {
						$scope.activeSubcategory=subCategoryId;
						$http.get("/category/getIndicatorsBySubCatID/" + subCategoryId).then(function successCallBack(response) {
							$scope.indicators = response.data.messages[0];
							$scope.activeIndicator=$scope.indicators.childs[0].id;
							$scope.activeIndicatorName=$scope.indicators.childs[0].name;
							$scope.activeSubCategory=subCategoryId;
							$scope.activeSubCat=response.data.messages[0];
							GAService.getChartData('prim-indicator',$scope.activeIndicator).then(function(response){
								$scope.graphdata=response;
								$scope.updateIndicatorStatus(response);
							});
							//GAService.getChartData('indicator',$scope.activeIndicator).then(function(response){
							//	$scope.graphdata=response;
							//});
						});
					}


					GAService.getIndicatorsByThreshold().then(function(response){
						$scope.indicatorsByThreshold=response;
						$scope.primaryIndicatorTitle=$scope.indicatorsByThreshold['Fooling US'][0].category.name;
						GAService.getChartData('prim-indicator',$scope.indicatorsByThreshold['Fooling US'][0].category.id).then(function(_response){
							$scope.primaryIndicator=_response.datasets;
						});

					});

					GAService.getManifesto().then(function(response){
						$scope.manifesto=response;
					});



					if($route.current.params && $route.current.params.q){
						$scope.searchTerm=$route.current.params.q;
					}


					$scope.getSuggestions = function (searchTerm) {
						if (searchTerm.length < 2) {
							$scope.searchResults = [];
							return;
						}
						GAService.search(searchTerm).then(function(result) {
								jQuery("#search_indicator").addClass("open");
								$scope.showSearchResults=true;
								$scope.searchResults = result;

							},
							function(result) {
								$scope.showSearchResults=false;
								$scope.searchResults = [];
							});
					}

						if($route.current.params && $route.current.params.q){
						    GAService.search($route.current.params.q).then(function(response){
							    $scope.results=response;
							    $scope.isFetching=false;
						    });
					    }

				} ]);
