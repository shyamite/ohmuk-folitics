'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:GA
 * @description # GACtrl Controller of the govrnnApp
 */
angular.module('govrnnApp').controller(
    'GraphFiltersCtrl',
    [ '$scope', '$rootScope','$route','AdminService','$http',
        function( $scope, $rootScope,$route ,AdminService,$http) {

            $scope.graphsegments=["GPI","GA","Feel"];
            $scope.showStatus=false;
            $scope.graphsegment="GA";

            $scope.filters=[];
            $scope.filter={};
        AdminService.getAllGraphFilters($scope.graphsegment).then(function(response){
            $scope.filters=response.messages;
        });

            $scope.updateFilterStatus=function(filterId,status){

            },
            $scope.updateGraphFilters=function(){
                AdminService.getAllGraphFilters($scope.graphsegment).then(function(response){
                    $scope.filters=response.messages;
                });

            };

            $scope.toggle=function(id,status,event){
                var	filter ={'id':id,'statusFlag':status};
                $http.post('/graphcomparison/update',filter).then(function(response) {
                    if (response.data.messages != null) {
                        if(response.data.messages[0].statusFlag=='Active')
                            event.target.parentNode.parentNode.className='success';
                        else
                            event.target.parentNode.parentNode.className='danger';

                    }
                });

            }


            $scope.saveFilter=function(filter){

                filter.startDate=(new Date(filter._startDate)).getTime();
                filter.endDate=(new Date(filter._endDate)).getTime();
                AdminService.addGraphFilter(filter).then(function(response){
                    $scope.status=true;
                    $scope.success=true;
                    $scope.filter={};
                },function(error){
                    $scope.status=true;
                    $scope.success=false;
                })
            };
        }]);