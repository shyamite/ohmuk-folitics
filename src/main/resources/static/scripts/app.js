'use strict';

/**
 * @ngdoc overview
 * @name govrnnApp
 * @description # govrnnApp
 * 
 * Main module of the application.
 */
angular.module(
		'govrnnApp',
		[ 'ngAnimate', 'ngCookies', 'ngResource', 'ngRoute', 'ngSanitize',
				'ngTouch', 'ngStorage', 'zingchart-angularjs' ])

.config(function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'views/home.html',
		controller : 'HomeCtrl',
		controllerAs : 'home'
	}).when('/about', {
		templateUrl : 'views/about.html',
		controller : 'AboutCtrl',
		controllerAs : 'about'
	}).when('/privacyPolicy', {
		templateUrl : 'views/privacyPolicy.html'
	}).when('/communityGuidelines', {
		templateUrl : 'views/communityGuidelines.html'
	}).when('/cookiePolicy', {
		templateUrl : 'views/cookiePolicy.html'
	}).when('/copyrightPolicy', {
		templateUrl : 'views/copyrightPolicy.html'
	}).when('/ga', {
		templateUrl : 'views/ga.html',
		controller : 'GACtrl',
		controllerAs : 'ga'
	}).when('/search', {
		templateUrl : 'views/search/searchResults.html',
		controller : 'SearchCtrl'
	}).when('/ga/indicator/:indicatorId', {
		templateUrl : 'views/ga.html',
		controller : 'GACtrl'
	}).when('/ga/indicator/:indicatorId/:seo', {
		templateUrl : 'views/ga.html',
		controller : 'GACtrl'
	}).when('/charts/demo', {
		templateUrl : 'views/admin/charts.html',
		controller : 'GACtrl'
	}).otherwise({
		redirectTo : '/'
	});
})

.run(function($rootScope) {
	$rootScope.liveHost = 'http://localhost:8080';
}).config([ '$locationProvider', function($locationProvider) {
	$locationProvider.hashPrefix('');
} ]);


angular.module('govrnnApp').filter('removeSpaces', [function() {
	return function(string) {
		if (!angular.isString(string)) {
			return string;
		}
		return string.replace(/[\s]/g, '-');
	};
}])