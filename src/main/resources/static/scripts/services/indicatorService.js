'use strict';

/**
 * @ngdoc service
 * @name govrnnApp.myService
 * @description # myService Service in the govrnnApp.
 */
angular.module('govrnnApp').factory('IndicatorService', function($http) {
	return {

		getActiveCategories : function getActiveCategories() {
			return $http.get("/category/getActiveCategories").then(function successCallBack(response) {
				return response;
			}, function errorCallBack(response) {
				return response;
			});
		},
		getSubCategories : function getSubCategories(params) {
			return $http.get("/category/getSubCatAndIndByCatId/" + params.categoryId).then(function successCallBack(response) {
				return response;
			}, function errorCallBack(response) {
				return response;
			});
		},
		getIndicatorDataById : function getIndicatorDataById(GAService) {
			return $http.get("/indicatordata/findIndicators/1111101").then(function successCallBack(response) {
				return response;
			}, function errorCallBack(response) {
				return response;
			});
		},
		getIndicatorsForCategory:function(){
			 return $http.get("/api/ga/chartData").then(function successCallBack(response) {
				 return response;
			}, function errorCallBack(response) {
				return response;
			});

		},
		getActiveIndicators : function() {
			return $http.get("/category/getActiveIndicators").then(function successCallBack(response) {
				return response.data;
			}, function errorCallBack(response) {
				return response;
			});
		},
		saveIndicator : function(indicator) {
			return $http.post("/indicatordata/add",indicator).then(function successCallBack(response) {
				return response.data;
			}, function errorCallBack(response) {
				return response;
			});
		},
		saveCategory : function(category) {
			return $http.post("/category/add",category).then(function successCallBack(response) {
				return response.data;
			}, function errorCallBack(response) {
				return response;
			});
		}
	}
});