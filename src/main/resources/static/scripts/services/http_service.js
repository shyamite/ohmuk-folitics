'use strict';

/**
 * @ngdoc service
 * @name govrnnApp.myService
 * @description
 * # myService
 * Service in the govrnnApp.
 */
angular.module('govrnnApp')
  .service('HttpHelper',['$http','$rootScope',function ($http,$rootScope) {

  this.create = function(type, url, postData){
   // var cntType = (contentType)? contentType:'application/json;';
    if(type === "GET"){
      return $http({
        method: type,
        url: url,
        data: postData,
        headers: {'Content-Type':'application/json; charset=UTF-8'},
        // withCredentials: true
      });
    }else if(type === "POST"){
      return $http({
        method: type,
        url: url,
        data: postData,
    //    headers: {'Content-Type':'text/plain; charset=UTF-8'},
		headers: {'Content-Type':'application/json; charset=UTF-8'}
     //   withCredentials: true
      });
    }
  };
}]);