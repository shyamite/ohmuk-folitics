'use strict';

/**
 * @ngdoc service
 * @name govrnnApp.myService
 * @description
 * # myService
 * Service in the govrnnApp.
 */
angular.module('govrnnApp')
  .service('HomeService',['$http','$rootScope','HttpHelper',function ($http,$rootScope,HttpHelper) {
	  
	  var rateArr=["/images/sad.png","/images/baised.png","/images/advert.png","/images/plagrism.png","/images/facts.png",
			  "/images/format.png","/images/intresting.png","/images/informative.png",
			  "/images/attractive.png","/images/ethical.png"] ;
		  
	this.saveRating = function(rateSentiment) {
			//alert(JSON.stringify(rateSentiment+"inside add"));
			$http(
					{
						method : 'POST',
						url : '/rating/rate',
						headers : {
							'Accept': 'application/json',						
							'Content-Type' : 'application/json'
						},
						data :  JSON.stringify(rateSentiment)
						
					}).then(function successCallback(response) {
						for(var i=0;i<10;i++){
							$('#rate_'+i+rateSentiment.componentId).css("background", "");
						}
						$('#rate_'+rateSentiment.rating+rateSentiment.componentId).css("background", "red");
		            }, function errorCallback(response) {
		            	//alert("error in saving ");
		            	console.log("error in saving rate");
		              return {};
		            });
		};
		
		this.deleteRating = function(rateSentiment) {
			//alert(JSON.stringify(rateSentiment+"inside delete"));
			$http(
					{
						method : 'POST',
						url : '/rating/unrate',
						headers : {
							'Accept': 'application/json',						
							'Content-Type' : 'application/json'
						},
						data :  JSON.stringify(rateSentiment)
						
					}).then(function successCallback(response) {
		            	//alert("successfully deleted rating ");
		            }, function errorCallback(response) {
		            	//alert("error in delete ");
		            	console.log("error in saving rate");
		              return {};
		            });
		};
		
		this.newsUserRatingList=function(rateNews){
			//alert("inside voteNewsRes");
			var newsRating = $http({
	              method: 'POST',
	              url: "/rating/getRating",
	              headers: {'Accept': 'application/json',
	            	  		'Content-Type': 'application/json'
	              			},
	              data:  JSON.stringify(rateNews)
	            }).then(function successCallback(response) {
	          //  	alert("news "+JSON.stringify(response.data.messages));
	            	return response.data.messages;
	            }, function errorCallback(response) {
	              console.log(response);
	              return {};
	            });
			};
			
			
			this.refrenceUserRatingList=function(rateRef){
				//alert("inside voteNewsRes");
				var refRating = $http({
		              method: 'POST',
		              url: "/rating/getRating",
		              headers: {'Accept': 'application/json',
		            	  		'Content-Type': 'application/json'
		              			},
		              data:  JSON.stringify(rateRef)
		            }).then(function successCallback(response) {
		          //  	alert("refrence "+JSON.stringify(response.data.messages));
		            	return response.data.messages;
		            }, function errorCallback(response) {
		            	console.log(response);
		            	return {};
		              
		            });
				};
		
				this.getAllRating=function(rateObj, cmptype){
					//alert("inside voteNewsRes");
					var refRating = $http({
			              method: 'POST',
			              url: "/rating/getAllRatings",
			              headers: {'Accept': 'application/json',
			            	  		'Content-Type': 'application/json'
			              			},
			              data:  JSON.stringify(rateObj)
			            }).then(function successCallback(response) {
			            	var allratings= response.data.messages[0];
			            	if(cmptype=="sentimentNews"){
			           			//alert("news");
			           			for(var i=0; i<allratings.length;i++){
			           				//alert('#rate_'+allratings[i].rating + allratings[i].componentId);
			           				$('#rate_'+allratings[i].rating+allratings[i].componentId).css("background", "red");
			           				var keynews= allratings[i].rating;
			           				//alert(rateArr[key]);
				            		$("#newsImg_"+allratings[i].componentId).attr("src",rateArr[keynews]);
					           	}
			           		}else if(cmptype=="reference"){
			           			//alert("ref");
			           			for(var i=0; i<allratings.length;i++){
			           				$('#rate_'+allratings[i].rating+allratings[i].componentId).css("background", "red");
			           				var keyref= allratings[i].rating;
			           			$("#refImg_"+allratings[i].componentId).attr("src",rateArr[keyref]);
					           	}
			           		}
			           	
			            	return response.data.messages[0];
			            }, function errorCallback(response) {
			            	console.log(response);
			            	return {};
			              
			            });
					};
					
					
		
}]);