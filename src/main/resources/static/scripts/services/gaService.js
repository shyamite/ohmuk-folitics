'use strict';

angular.module('govrnnApp').filter('range', function() {
	return function(input, total) {
		total = parseInt(total);
		for (var i=0; i<total; i++)
			input.push(i);
		return input;
	};
});


angular.module('govrnnApp').directive("qtip", function () {

	return function (scope, element, attrs) {

		var text = attrs.qtipContent || null,
			title = attrs.qtipTitle || null;

		scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-bootstrap");

		element.qtip({
			content: {
				text: text,
				title: title
			},

			style: {
				classes: scope.qtipSkin + " qtip-rounded qtip-shadow "
			},
			show: {
				event: 'click mouseover',
				solo: true
			},
			hide: {
				event: (scope.closeButton == true ? "false" : "click mouseleave"),
				delay: 300,
				fixed: (($(this).hover || attrs.fixed) ? true : false),  //prevent the tooltip from hiding when set to true
				leave: false
			},
			position: {
				target: (attrs.target ? attrs.target :"event"),
				my: "bottom left",
				at:  "top right",
				adjust: {
					method: 'none shift'
				}
			}
		});

		scope.$on("$destroy", function () {
			$(element).qtip('destroy', true); // Immediately destroy all tooltips belonging to the selected elements
		});

		$('[qtip]').css("display", "inline-block");
	}
});

angular.module('govrnnApp').directive('readMore', ['$compile','$timeout', function ($compile,$timeout) {
	return {
			link: function (scope, element, attr) {

				scope.$watch(attr.readMore,function(){

					var readMoreOption = scope.$eval(attr.readMoreOptions);
					var readMore= scope.$eval(attr.readMore);
					$timeout(function(){
						$(element).html(readMore);
						$(element).readmore(readMoreOption);
					},1000);
				});

		}
	};
}]);
/**
 * @ngdoc service
 * @name govrnnApp.myService
 * @description # myService Service in the govrnnApp.
 */
angular.module('govrnnApp').factory('GAService', function($http) {
	return {
		getActiveCategories : function getActiveCategories(GAService) {
			return $http.get("/category/getActiveCategories").then(function successCallBack(response) {
				return response;
			}, function errorCallBack(response) {
				return response;
			});
		},

		getSubCatAndIndByCatId : function getSubCatAndIndByCatId(params, GAService) {
			if (isNaN(params.categoryId)) {
				params.categoryId = 11;
			}
			return $http.get("/category/getSubCatAndIndByCatId/" + params.categoryId).then(function successCallBack(response) {
				return response;
			}, function errorCallBack(response) {
				return response;
			});
		},

		getIndicatorsBySubCatID : function getIndicatorsBySubCatID(params, GAService) {
			if (isNaN(params.subCategoryId)) {
				params.subCategoryId = 112;
			}
			return $http.get("/category/getIndicatorsBySubCatID/" + params.subCategoryId).then(function successCallBack(response) {
				return response;
			}, function errorCallBack(response) {
				return response;
			});
		},

		fetchCategoryDetailsByIndicatorId : function fetchIndicator(indicatorId) {
			return $http.get("/category/indicator/" +indicatorId).then(function successCallBack(response) {
				return response;
			}, function errorCallBack(response) {
				return response;
			});
		},

		getChartData:function getChartData(chartName,id){
			if(chartName=='subcategory'){
				return $http.get("/chart/subcategory/" + id).then(function successCallBack(response) {
					return response.data;
				}, function errorCallBack(response) {
					return response;
				});
			}else if(chartName=='keyindicators'){
				return $http.get("/chart/keyindicators").then(function successCallBack(response) {
					return response.data;
				}, function errorCallBack(response) {
					return response;
				});
			}else if(chartName=='indicator'){
				return $http.get("/chart/indicators/" + id).then(function successCallBack(response) {
					return response.data;
				}, function errorCallBack(response) {
					return response;
				});
			}else if(chartName=='prim-indicator'){
				return $http.get("/chart/indicator/" + id).then(function successCallBack(response) {
					return response.data;
				}, function errorCallBack(response) {
					return response;
				});
			}

		},
		
		getIndicatorsThresholdsByIndicatorId : function getIndicatorsThresholdsByIndicatorId(params, GAService) {
			if (isNaN(params.indicatorId)) {
				params.indicatorId = 1272;
			}
			return $http.get("/indicatorthreshold/findIndicatorThresholds/" + params.indicatorId).then(function successCallBack(response) {
				return response;
			}, function errorCallBack(response) {
				return response;
			});
		},

		getGovernmentSchemes:function(){
			return $http.get("/govtschemedata/getall").then(function successHandler(response){
				return response.data.messages;
			});
		},

		share:function(shareObj){
			return  $http.post("/componentShare/email",shareObj).then(function successHandler(response){
				return response.data.messages;
			});
		},

		getIndicatorsByThreshold:function(){
			return $http.get("/indicatordata/tracker").then(function successHandler(response){
				return response.data.result;
			});
		},

		getManifesto:function(){
			return $http.get("/api/ga/manifesto").then(function successHandler(response){
				return response.data;
			});
		},

		submitFact:function(fact){
			return $http.post("/comment/add",fact).then(function successHandler(response){
				return response.data;
			});
		},
		search:function(text){
			return $http.get("/api/search?q="+text).then(function successHandler(response){
				return response.data;
			});
		},
		searchGovtSchemes:function (searchTerm){
			return $http.get("/api/search/schemes?q="+searchTerm).then(function successHandler(response){
				return response.data;
			});
		}
	}
	
});