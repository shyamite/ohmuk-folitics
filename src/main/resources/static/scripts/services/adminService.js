'use strict';


/**
 * @ngdoc service
 * @name govrnnApp.myService
 * @description # myService Service in the govrnnApp.
 */
angular.module('govrnnApp').factory('AdminService', function($http) {
	return {
		addCategory:function(category){
			return $http.post("/category/add/",category).then(function successCallBack(response) {
				return response.data;
			}, function errorCallBack(response) {
				return response;
			});
		},
		listCategories:function(){
			return $http.get("/category/getAll").then(function successCallBack(response) {
				return response.data;
			}, function errorCallBack(response) {
				return response;
			});
		},
		addKeyIndicator : function(indicator) {
			return $http.get("/api/admin/keyindicators/add/"+indicator).then(function successCallBack(response) {
				return response.data;
			}, function errorCallBack(response) {
				return response;
			});
		},
		removeKeyIndicator : function(indicator) {
			return $http.get("/api/admin/keyindicators/remove/"+indicator).then(function successCallBack(response) {
				return response.data;
			}, function errorCallBack(response) {
				return response;
			});
		},
		getAllGraphFilters : function(filter) {
			return $http.get("/graphcomparison/getGraphFilters?graphSegment="+filter).then(function successCallBack(response) {
				return response.data;
			}, function errorCallBack(response) {
				return response;
			});
		},
		addGraphFilter : function(filter) {
			return $http.post("/graphcomparison/add/",filter).then(function successCallBack(response) {
				return response.data;
			});
		},
		disableGraphFilter : function(filter) {
			return $http.get("/graphcomparison/disable/"+filter).then(function successCallBack(response) {
				return response.data;
			}, function errorCallBack(response) {
				return response;
			});
		}

	}
	
});

angular.module('govrnnApp').directive('date', ['$q', function($q) {
	return {
		restrict: 'E',
		replace: true,
		link:function($scope, element, attr){
			var timeStmp=attr.value*1;
			var todate=new Date(timeStmp ).getDate();
			var tomonth=new Date(timeStmp).getMonth()+1;
			var toyear=new Date(timeStmp).getFullYear();
			var original_date=toyear+'/'+tomonth+'/'+todate;
			element.html(original_date);
		}
	};
}]);
