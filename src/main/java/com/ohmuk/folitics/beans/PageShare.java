package com.ohmuk.folitics.beans;

/**
 * Created by WENKY on 9/17/2017.
 */
public class PageShare {
    String receiverName;
    String recieverEmail;
    String sharerName;
    String sharerEmail;

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getRecieverEmail() {
        return recieverEmail;
    }

    public void setRecieverEmail(String recieverEmail) {
        this.recieverEmail = recieverEmail;
    }

    public String getSharerName() {
        return sharerName;
    }

    public void setSharerName(String sharerName) {
        this.sharerName = sharerName;
    }

    public String getSharerEmail() {
        return sharerEmail;
    }

    public void setSharerEmail(String sharerEmail) {
        this.sharerEmail = sharerEmail;
    }
}
