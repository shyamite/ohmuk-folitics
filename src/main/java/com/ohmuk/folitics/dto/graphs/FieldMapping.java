package com.ohmuk.folitics.dto.graphs;

/**
 * Created by WENKY on 7/6/2017.
 */
public class FieldMapping {
    String fromField,toField;

    public FieldMapping(String fromField, String toField) {
        this.fromField = fromField;
        this.toField = toField;
    }

    public String getFromField() {
        return fromField;
    }

    public void setFromField(String fromField) {
        this.fromField = fromField;
    }

    public String getToField() {
        return toField;
    }

    public void setToField(String toField) {
        this.toField = toField;
    }
}
