package com.ohmuk.folitics.dto.graphs;

import java.util.List;
import java.util.Map;

/**
 * Created by WENKY on 7/6/2017.
 */
public class DatasetDto {
    String title,categoryField,color;
    List<ChartDataDto> dataProvider;
    List<StockEvent> stockEvents;
    List<FieldMapping> fieldMappings;
    Map<String,Object> thresholdValues;

    public Map<String,Object> getThresholdValues() {
        return thresholdValues;
    }

    public void setThresholdValues(Map<String,Object> thresholdValues) {
        this.thresholdValues = thresholdValues;
    }

    public DatasetDto(){}
    public DatasetDto(String title){
        this.title=title;
    }
    public String getCategoryField() {
        return categoryField;
    }

    public void setCategoryField(String categoryField) {
        this.categoryField = categoryField;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<FieldMapping> getFieldMappings() {
        return fieldMappings;
    }

    public void setFieldMappings(List<FieldMapping> fieldMappings) {
        this.fieldMappings = fieldMappings;
    }

    public List<ChartDataDto> getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(List<ChartDataDto> dataProvider) {
        this.dataProvider = dataProvider;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<StockEvent> getStockEvents() {
        return stockEvents;
    }

    public void setStockEvents(List<StockEvent> stockEvents) {
        this.stockEvents = stockEvents;
    }
}
