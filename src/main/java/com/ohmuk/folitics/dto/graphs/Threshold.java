package com.ohmuk.folitics.dto.graphs;

/**
 * Created by WENKY on 7/15/2017.
 */
public class Threshold {
    String name,value;
    Double min,max;

    public Threshold(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public Threshold(Double max, Double min, String name) {
        this.max = max;
        this.min = min;
        this.name = name;
    }

    public Double getMax() {
        return max;
    }

    public Double getMin() {
        return min;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
