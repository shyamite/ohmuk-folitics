package com.ohmuk.folitics.dto.graphs;

import java.util.List;
import java.util.Map;

/**
 * Created by WENKY on 7/5/2017.
 */
public class ChartDataDto {
    long date;
    double value,volume;
    Map details;

    public Map getDetails() {
        return details;
    }

    public void setDetails(Map details) {
        this.details = details;
    }

    public ChartDataDto(long date, double value, double volume) {
        this.date = date;
        this.value = value;
        this.volume = volume;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }
}
