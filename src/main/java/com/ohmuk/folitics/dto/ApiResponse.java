package com.ohmuk.folitics.dto;

/**
 * Created by WENKY on 7/1/2017.
 */
public class ApiResponse<T> {
    String status;
    T result;

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
