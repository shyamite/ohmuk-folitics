package com.ohmuk.folitics.dto;

/**
 * Created by WENKY on 7/6/2017.
 */
public class ManifestoDto {
    String image,title;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
