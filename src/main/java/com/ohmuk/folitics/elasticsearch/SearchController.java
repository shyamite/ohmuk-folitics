package com.ohmuk.folitics.elasticsearch;

import com.ohmuk.folitics.businessDelegate.implementations.SearchBusinessDelegate;
import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by WENKY on 7/19/2017.
 */
@RestController
@RequestMapping("/api/search")
public class SearchController {

    @Autowired
    Client client;

    @Autowired
    SearchBusinessDelegate searchbusinessDelegate;

    @Autowired
    ElasticsearchOperations elasticsearchOperations;

    @RequestMapping(method = RequestMethod.GET,value = "/init")
    @Scheduled(fixedDelayString = "24*60*60")
    public void init(){
        searchbusinessDelegate.init();
    }

    @RequestMapping(method = RequestMethod.GET, value = "")
    public Object search(@RequestParam("q") String searchString) {
      Object o= searchbusinessDelegate.search(searchString);
        return o;
    }


    @RequestMapping(method = RequestMethod.GET, value = "/schemes")
    public Object searchSchemes(@RequestParam("q") String searchString) {
        Object o= searchbusinessDelegate.searchSchemes(searchString);
        return o;
    }
}