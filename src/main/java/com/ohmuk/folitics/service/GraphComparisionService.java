package com.ohmuk.folitics.service;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.enums.CategoryType;
import com.ohmuk.folitics.enums.GraphSegmentType;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.GraphComparisonFilter;


@Service
@Transactional
public class GraphComparisionService implements IGraphComparisionService {
    private static Logger logger = Logger.getLogger(CategoryService.class);
    
    
    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }
    

    @Override
    public GraphComparisonFilter create(GraphComparisonFilter graphComparisonFilter) throws Exception {

        logger.debug("Inside category serivice create method");
        Long id = (Long) getSession().save(graphComparisonFilter);
        logger.debug("Exiting from category serivice create method");
        return read(id);

        // TODO Auto-generated method stub
    }

    @Override
    public GraphComparisonFilter read(Long id) throws Exception {
        return (GraphComparisonFilter) getSession().get(GraphComparisonFilter.class, id);
    }
    
    /**
     * 
     */
    @Override
    public List<GraphComparisonFilter> readGraphFilters(String graphSegment, Set<String> setOfStatusFlag) {
        logger.info("Inside GraphComparisionService of readGraphFilters method");
        Criteria criteria = getSession().createCriteria(GraphComparisonFilter.class);
        criteria.add(Restrictions.eq("graphSegment", GraphSegmentType.getGraphSegmentType(graphSegment).getValue()));
        criteria.add(Restrictions.in("statusFlag", setOfStatusFlag));
        List<GraphComparisonFilter> graphComparisonFilters = criteria.list();
        logger.debug("Exiting GraphComparisionService of readGraphFilters method");
        return graphComparisonFilters;
    }

}
