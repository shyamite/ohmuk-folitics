package com.ohmuk.folitics.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.model.ImageModel;

/**
 * @author Abhishek
 *
 */
@Service
@Transactional
public class UserService implements IUserService {

	private static Logger logger = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private SessionFactory _sessionFactory;


	private Session getSession() {
		return _sessionFactory.getCurrentSession();
	}

	@Override
	public User create(User user) throws Exception {
		logger.info("Inside user service create method");

		Long userId = (Long) getSession().save(user);
//		if (Constants.useElasticSearch)
//			esService.save(ElasticSearchUtils.INDEX, ElasticSearchUtils.TYPE_USER, String.valueOf(userId),
//					Serialize_JSON.getJSONString(user));

		//logger.debug("User is added with username: " + user.getUsername());
		logger.info("Exiting from UserService create method");
		return findUserById(userId);
	}

	public User findUserById(Long id) throws Exception {
		logger.info("Inside UserService findUserById method");
		User user = (User) getSession().get(User.class, id);
		//Hibernate.initialize(user.getUserImage());
		if (null != user) {
			logger.debug("User found with id: " + id);
			logger.info("Exiting from UserService findUserById method");
			return user;
		}
		logger.debug("No user found with id: " + id);
		logger.info("Exiting from UserService findUserById method");
		return null;
	}

	@Override
	public ImageModel getImageModel(Long entityId, boolean isThumbnail)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ImageModel> getImageModels(String entityIds, boolean isThumbnail) {
		// TODO Auto-generated method stub
		return null;
	}

}
