package com.ohmuk.folitics.service;

import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.KeyIndicators;
import com.ohmuk.folitics.util.DateUtils;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.fontbox.cff.CFFOperator;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by WENKY on 7/25/2017.
 */
@Repository
public class KeyIndicatorService {
    @Autowired
    private SessionFactory _sessionFactory;


    public void save(KeyIndicators indicator)
    {
        _sessionFactory.getCurrentSession().save(indicator);
    }


    public void removeKeyIndicator()
    {

    }

    public KeyIndicators findById(Long indicatorId) {
        return  (KeyIndicators) _sessionFactory.getCurrentSession().get(KeyIndicators.class,indicatorId);
    }
}
