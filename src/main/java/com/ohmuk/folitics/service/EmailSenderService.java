package com.ohmuk.folitics.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by WENKY on 9/16/2017.
 */
@Service
public class EmailSenderService {

    @Autowired
    Environment environment;


    /**
     *
     * @param to
     * @param subject
     * @param body
     */
    @Async
    public void sendEmail(String to, String subject,String body) {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost(environment.getProperty("mail.smtp.host"));
        // sender.setUsername(environment.getProperty("mail.support.id"));
        sender.setPort(Integer.parseInt(environment.getProperty("mail.smtp.port")));
        sender.setPassword(environment.getProperty("mail.from.pwd"));
        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.mime.charset", "utf-8");
        sender.setDefaultEncoding("utf-8");
        // javaMailProperties.put("mail.smtp.starttls.enable", "true");
        sender.setJavaMailProperties(javaMailProperties);
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, false, "utf-8");
            message.setContent(body, "text/html;Charset=UTF-8");
        } catch (MessagingException e1) {
            e1.printStackTrace();
        }
        try {
            helper.setFrom(environment.getProperty("mail.from.email"));
            helper.setTo(to);

            helper.setSubject(subject);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        sender.send(message);
    }

}
