package com.ohmuk.folitics.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.IndicatorData;
import com.ohmuk.folitics.hibernate.entity.IndicatorThreshold;
import com.ohmuk.folitics.util.DateUtils;

/**
 * 
 * @author Mayank Sharma
 * 
 */
@Service
@Transactional
public class IndicatorThresholdService implements IIndicatorThresholdService {

    private static Logger logger = LoggerFactory.getLogger(IndicatorThresholdService.class);

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    /**
     * This method is used to add IndicatorThreshold by calling
     * 
     * @author Mayank Sharma
     * @return IndicatorThreshold
     * @throws Exception
     */
    @Override
    public IndicatorThreshold create(IndicatorThreshold indicatorthreshold) throws Exception {
        logger.info("Inside IndicatorThresholdService create method");
        Long id = (Long) getSession().save(indicatorthreshold);
        logger.info("Exiting from IndicatorThresholdService create method");
        return read(id);
    }

    /**
     * This method is to get IndicatorThreshold by id
     * 
     * @author Mayank Sharma
     * @return IndicatorThreshold
     * @throws Exception
     */
    @Override
    public IndicatorThreshold read(Long id) throws Exception {
        logger.info("Inside IndicatorThresholdService read method");
        return (IndicatorThreshold) getSession().get(IndicatorThreshold.class, id);
    }

    /**
     * This method is to get IndicatorThresholds by id
     * 
     * @author Mayank Sharma
     * @return IndicatorThreshold
     * @throws Exception
     */
    @Override
    public List<IndicatorThreshold> readThresholdByIndicatorId(Long id) throws Exception {
        logger.info("Inside IndicatorThresholdService readThresholdByIndicatorId method");
        SQLQuery query = getSession()
                .createSQLQuery("select * from indicatorthreshold where indicatorId =" + id + " order by editTime");
        query.addEntity(IndicatorThreshold.class);
        List<IndicatorThreshold> indicatorThresholds = query.list();
        return indicatorThresholds;
    }

    /**
     * This web service is used to get all Indicator and segregate them in
     * different threshold category - Header of GA *
     * 
     * @author kMS
     * @return Set<IndicatorData>
     * @throws Exception
     */

    @Override
    public Set<IndicatorData> getIndicatorsGroupByThreshold(String[] thresholdType) {

        logger.info("Inside IndicatorThresholdService getIndicatorsGroupByThreshold method");

        // select * from folitics.indicatordata where indicatorId in (select
        // distinct(indicatorId) from indicatordata) order by updateddate desc
        // Limit 1;

        // fetch all the indicator from category table
        // select * from folitics.indicatordata where indicatorId in ( select
        // distinct(indicatorId) from indicatordata);

        String sqlQuery = new StringBuilder(
                "SELECT t.id,t.createTime,t.editTime,t.deleted, t.indicatorid, r.MaxTime as updateddate,t.delta, t.effectfromdate, t.indicatorvalue,")
                        .append("t.score,t.state,t.weightedIdealValue,t.weightedValue,t.actualValueRange,t.thresholdcategory")
                        .append(" FROM (SELECT indicatorid, MAX(updateddate) as MaxTime FROM folitics.indicatordata GROUP BY indicatorid ) r ")
                        .append(" INNER JOIN folitics.indicatordata t")
                        .append(" ON t.indicatorid = r.indicatorid AND t.updateddate = r.MaxTime ;").toString();

        SQLQuery getLatestindicatorsByUpdatedDate = getSession().createSQLQuery(sqlQuery);
        getLatestindicatorsByUpdatedDate.addEntity(IndicatorData.class);
        Set<IndicatorData> indicatorsdata = new HashSet<IndicatorData>(getLatestindicatorsByUpdatedDate.list());
        
        System.out.println("indicatorsdata.size() = " + indicatorsdata.size());

        logger.info("Exiting IndicatorThresholdService getIndicatorsGroupByThreshold method");

        return indicatorsdata;

    }

    /**
     * This method is to get all IndicatorThreshold
     * 
     * @author Mayank Sharma
     * @return IndicatorThreshold
     * @throws Exception
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<IndicatorThreshold> readAll() throws Exception {
        logger.info("Inside IndicatorThresholdService create method");
        return getSession().createCriteria(IndicatorThreshold.class).list();
    }

    /**
     * This method is to update IndicatorThreshold by calling
     * 
     * @author Mayank Sharma
     * @return IndicatorThreshold
     * @throws Exception
     */
    @Override
    public IndicatorThreshold update(IndicatorThreshold indicatorthreshold) throws Exception {
        logger.info("Inside IndicatorThresholdService update method");
        indicatorthreshold.setEditTime(DateUtils.getSqlTimeStamp());
        getSession().update(indicatorthreshold);
        logger.info("Exiting from IndicatorThresholdService update method");
        return indicatorthreshold;
    }

    /**
     * This method is to soft delete IndicatorThreshold
     * 
     * @author Mayank Sharma
     * @return IndicatorThreshold
     * @throws Exception
     */
    @Override
    public boolean delete(Long id) throws Exception {
        logger.info("Inside IndicatorThresholdService delete(Long id) method");
        IndicatorThreshold indicatorThreshold = (IndicatorThreshold) getSession().get(IndicatorThreshold.class, id);
        indicatorThreshold.setState(ComponentState.DELETED.getValue());
        getSession().update(indicatorThreshold);
        logger.info("Exiting from IndicatorThresholdService delete(Long id) method");
        return true;

    }

    /**
     * This method is to hard delete IndicatorThreshold by id
     * 
     * @author Mayank Sharma
     * @return IndicatorThreshold
     * @throws Exception
     */
    @Override
    public boolean deleteFromDB(Long id) throws Exception {
        logger.info("Inside IndicatorThresholdService deleteFromDB(Long id) method");
        IndicatorThreshold indicatorThreshold = read(id);
        getSession().delete(indicatorThreshold);
        logger.info("Exiting from IndicatorThresholdService deleteFromDB(Long id) method");
        return true;
    }

    /**
     * This method is to soft delete IndicatorThreshold
     * 
     * @author Mayank Sharma
     * @return IndicatorThreshold
     * @throws Exception
     */
    @Override
    public boolean delete(IndicatorThreshold indicatorthreshold) throws Exception {
        logger.info("Inside IndicatorThresholdService delete method");
        indicatorthreshold.setState(ComponentState.DELETED.getValue());
        getSession().update(indicatorthreshold);
        logger.info("Exiting from IndicatorThresholdService delete method");
        return true;
    }

    /**
     * Method is to find {@link IndicatorThreshold} by latest {@link Category}
     * 
     * @param category
     * @return {@link IndicatorThreshold}
     * @throws Exception
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<IndicatorThreshold> findByCategoryLatest(Category category) {

        SQLQuery query = getSession().createSQLQuery(
                "select * from indicatorthreshold  where id in (select max(id) from indicatorthreshold where indicatorId = "
                        + category.getId() + " group by threshHoldCategory)");
        query.addEntity(IndicatorThreshold.class);
        List<IndicatorThreshold> indicatorThresholds = query.list();

        return indicatorThresholds;
    }

    /**
     * Method is to find {@link IndicatorThreshold} by latest {@link Category}
     * 
     * @param category
     * @return {@link IndicatorThreshold}
     * @throws Exception
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<IndicatorThreshold> findByCategory(Long categoryId) {

        SQLQuery query = getSession().createSQLQuery(
                "select * from indicatorthreshold where indicatorId =" + categoryId + " order by editTime");
        query.addEntity(IndicatorThreshold.class);
        List<IndicatorThreshold> indicatorThresholds = query.list();

        return indicatorThresholds;
    }

}
