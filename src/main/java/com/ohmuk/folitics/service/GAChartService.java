package com.ohmuk.folitics.service;

import java.util.*;

import com.ohmuk.folitics.dto.graphs.*;
import com.ohmuk.folitics.dto.worldbank.Data;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.hibernate.entity.*;
import com.ohmuk.folitics.jpa.repository.module.*;
import com.ohmuk.folitics.util.ChartUtil;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.PreConditions;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.charting.Exception.ProcessingException;
import com.ohmuk.folitics.charting.Exception.ValidationException;
import com.ohmuk.folitics.charting.beans.ChartRequest;
import com.ohmuk.folitics.charting.beans.ChartResponse;
import com.ohmuk.folitics.charting.beans.LineChartData;
import com.ohmuk.folitics.util.Serialize_JSON;
import org.springframework.ui.Model;

import javax.naming.CompoundName;

/**
 * 
 * @author Mayank Sharma
 *
 */
@Aspect
@Service
@Transactional
public class GAChartService implements IChartService {

    private static Logger logger = LoggerFactory.getLogger(GAChartService.class);

    @Autowired
    private SessionFactory _sessionFactory;

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ChartMetaDataRepository chartMetaDataRepository;
    @Autowired
    IndicatorDataRepository indicatorDataRepository;
    @Autowired
    IndicatorThresholdRepository indicatorThresholdRepository;
    @Autowired
    GraphComparisonFilterRepository graphComparisonFilterRepository;
    @Autowired
    KeyIndicatorsRepository keyIndicatorsRepository;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    /**
     * This method is to genrate chart data by ChartRequest entity and chategory id
     * 
     * @return ChartResponse
     * @throws Exception
     */
    @Override
    @SuppressWarnings("unchecked")
    public ChartResponse getChartData(ChartRequest chartRequest, Long id) throws Exception {
        logger.info("Inside GAChartService getChartData method");

        ChartResponse chartResponseData = new ChartResponse();

        List<String> chartMeta = new ArrayList<String>();
        List<LineChartData> lineChartDatas = new ArrayList<LineChartData>();
        String chartJson = null;

        Criteria criteria = getSession().createCriteria(Chart.class);
        criteria.add(Restrictions.eq("chartID", chartRequest.getChartID()));
        criteria.add(Restrictions.eq("chartSecondaryID", chartRequest.getChartSubID()));
        Chart chart = (Chart) criteria.uniqueResult();
        int i = 0;
        for (ChartMetaData chartMetaData : chart.getChartMetaData()) {

            logger.debug("Chart property name: " + chartMetaData.getPropertyName() + " value: "
                    + chartMetaData.getPropertyValue());
            chartMeta.add(i, chartMetaData.getPropertyValue());
            i++;
        }
        Category category = new Category();
        category.setId(id);
        double score = 0.0;
        String idealValue = "";
        Criteria indicatorDataCriteria = getSession().createCriteria(IndicatorData.class);
        indicatorDataCriteria.add(Restrictions.eq("category", category));
        List<IndicatorData> indicatorsData = indicatorDataCriteria.list();

        for (IndicatorData indicatorData : indicatorsData) {
            LineChartData lineChartData = new LineChartData();
            lineChartData.setTimestamp(indicatorData.getEffectfromdate());
            lineChartData.setScore(indicatorData.getScore());
            lineChartDatas.add(lineChartData);
            score = indicatorData.getScore();
            idealValue = indicatorData.getIdealValueRange();
        }
        if (!idealValue.equals("")) {
            String idealValueStart = idealValue.substring(0, idealValue.indexOf("-"));
            double idealValueLong = Double.valueOf(idealValueStart);
            if (score > idealValueLong) {
                logger.debug("Chart direction UP");
                chartResponseData.getChartMeta().add("ChartTrend,UP");
            } else {
                logger.debug("Chart direction DOWN");
                chartResponseData.getChartMeta().add("ChartTrend,Down");
            }
        }

        try {
            chartJson = Serialize_JSON.getJSONString(chartMeta);
        } catch (Exception exception) {
            logger.error("Exception in serializing chart data");
            logger.error("Exception: " + exception);
            throw new ProcessingException("We are not able to process the request at this time");
        }
        chartResponseData.setChartMeta(chartMeta);
        chartResponseData.setData(lineChartDatas);
        logger.info("Exciting from GAChartService getChartData method");
        return chartResponseData;

    }

    @Override
    public List<Chart> readAll() {
        return null;
    }

    @Override
    public void getChartForSubCategory(Model model) {
        String subCategoryId=(String)model.asMap().get("subCategoryId");
        String chartId="";
        List<Category> indicators=categoryRepository.findIndicatorsBySubCategory(subCategoryId);
        List<DatasetDto> chartDatsets=prepareChartData(indicators);

        model.addAttribute("chartdata",chartDatsets);
        model.addAttribute("indicators",indicators);
    }

    @Override
    public void getChartForIndicators(Model model) {
        String indicatorId=(String)model.asMap().get("indicatorId");
        Category indicator=categoryRepository.findOne(Long.valueOf(indicatorId));

        List<IndicatorData> indicatorDataList=null;

        List<DatasetDto> chartDatsets=new ArrayList<>();
        List<GraphComparisonFilter> filters=graphComparisonFilterRepository.findAllFiltersByStatus(ComponentState.ACTIVE.getValue());

        int color=0x33B7FF;
        if(PreConditions.isNotEmpty(filters))
            for(GraphComparisonFilter filter:filters){
                 indicatorDataList= indicatorDataRepository.findIndicators(indicator.getId(),filter.getStartDate(),filter.getEndDate());

                List<ChartDataDto> chartDataDtoList=new ArrayList<ChartDataDto>();

                for(IndicatorData indicatorData:indicatorDataList){
                    ChartDataDto chartDataDto=new ChartDataDto(indicatorData.getEffectfromdate().getTime(),indicatorData.getIndicatorvalue(),20);
                    if(PreConditions.isNotEmpty(indicatorData.getIndicatorDetails())){
                        Map details=new HashMap();
                        for(IndicatorDataDetails d:indicatorData.getIndicatorDetails()){
                            details.put(d.getIndicatorName(),d.getValue());
                        }
                        chartDataDto.setDetails(details);
                    }
                    chartDataDtoList.add(chartDataDto);
                }

                DatasetDto datasetDto=prepareChartData(filter.getName(),chartDataDtoList,indicator);
//                datasetDto.setColor("#"+ Integer.toHexString(color));
                chartDatsets.add(datasetDto);
                color+=0X1bfFF;
            }

        Map<String,Object> thresholdsList=new HashMap<>();

        List<IndicatorThreshold> thresholds=indicatorThresholdRepository.findThresholdByIndicatorId(indicator.getId());
        if(PreConditions.isNotEmpty(thresholds))
            for(IndicatorThreshold i:thresholds){
                Threshold threshold=new Threshold(i.getThreshold_end(),i.getThreshold_start(),i.getThreshHoldCategory());
                thresholdsList.put(threshold.getName(),threshold);
            }
        IndicatorData data=indicatorDataRepository.findLatestDataByIndicatorId(indicator.getId());

        Map m=new HashMap<>();
        m.put("status",data.getThresholdcategory());

        m.put("datasets",chartDatsets);
        m.put("thresholdValues",thresholdsList);
        m.put("title",indicator.getName());
        m.put("filters",filters);

        model.addAttribute("chartdata",m);
    }




    @Override
    public void getChartForIndicator(Model model) {
        String indicatorId=(String)model.asMap().get("indicatorId");
        Category indicator=categoryRepository.findOne(Long.valueOf(indicatorId));

        List<IndicatorData> indicatorDataList=null;
        List<DatasetDto> chartDatsets=new ArrayList<>();


                indicatorDataList= indicatorDataRepository.findAllByIndicatorId(Long.valueOf(indicatorId));

                List<ChartDataDto> chartDataDtoList=new ArrayList<ChartDataDto>();

                for(IndicatorData indicatorData:indicatorDataList){
                    ChartDataDto chartDataDto=new ChartDataDto(indicatorData.getEffectfromdate().getTime(),indicatorData.getIndicatorvalue(),20);
                    if(PreConditions.isNotEmpty(indicatorData.getIndicatorDetails())){
                        Map details=new HashMap();
                        for(IndicatorDataDetails d:indicatorData.getIndicatorDetails()){
                            details.put(d.getIndicatorName(),d.getValue());
                        }
                        chartDataDto.setDetails(details);
                    }
                    chartDataDtoList.add(chartDataDto);
                }

                DatasetDto datasetDto=prepareChartData(indicator.getName(),chartDataDtoList,indicator);
                chartDatsets.add(datasetDto);


        Map<String,Object> thresholdsList=new HashMap<>();

        List<IndicatorThreshold> thresholds=indicatorThresholdRepository.findThresholdByIndicatorId(indicator.getId());
        if(PreConditions.isNotEmpty(thresholds))
            for(IndicatorThreshold i:thresholds){
                Threshold threshold=new Threshold(i.getThreshold_start(),i.getThreshold_end(),i.getThreshHoldCategory());
                thresholdsList.put(threshold.getName(),threshold);
                thresholdsList.put("direction",i.getDirection());
            }
        datasetDto.setThresholdValues(thresholdsList);

        List<GraphComparisonFilter> filters=graphComparisonFilterRepository.findAllFiltersByStatus(ComponentState.ACTIVE.getValue());

    IndicatorData data=indicatorDataRepository.findLatestDataByIndicatorId(indicator.getId());
        String status="";
        if(PreConditions.isNotEmpty(data))
            status=data.getThresholdcategory();

        Map m=new HashMap<>();

        m.put("datasets",chartDatsets);
        m.put("subcategory",indicator.getParents().get(0).getName());

        m.put("title",indicator.getName());
        m.put("status",status);

        m.put("filters",filters);

        model.addAttribute("chartdata",m);
    }


    @Override
    public void getChartForKeyIndicators(Model model) {

        List<Category> categories=categoryRepository.findAllActiveKeyIndicators();

        List<DatasetDto> chartDatsets=null;
        if(PreConditions.isNotEmpty(categories))
           chartDatsets=prepareChartData(categories);
        Map m=new HashMap();
        m.put("datasets",chartDatsets);
        m.put("keyIndicators",categories);

        model.addAttribute("chartdata",m);

    }


    private DatasetDto prepareChartData(String filter,List<ChartDataDto> chartDataDtoList,Category category) {
        DatasetDto datasetDto = new DatasetDto(filter);

            datasetDto.setCategoryField("date");
            List<StockEvent> stockEvents = new ArrayList<StockEvent>();
            StockEvent stockEvent = new StockEvent();
            stockEvent.setDate(new Date());
            stockEvent.setText("S");
            stockEvent.setDescription("This is a description");
            stockEvent.setBackgroundColor("#85CDE6");
            stockEvent.setGraph("g1");
            stockEvent.setType("sign");
            stockEvents.add(stockEvent);

            List<FieldMapping> fieldMappings=new ArrayList<FieldMapping>();
            fieldMappings.add(new FieldMapping("value","value"));
            fieldMappings.add(new FieldMapping("volume","volume"));
            datasetDto.setFieldMappings(fieldMappings);

            datasetDto.setDataProvider(chartDataDtoList);

        return  datasetDto;
    }

        private List<DatasetDto> prepareChartData(List<Category> indicators) {
        List<DatasetDto> chartDatsets=new ArrayList<DatasetDto>();

        int color=0x33B7FF;
        for (Category category : indicators) {
            List<ChartDataDto> chartDataDtoList = new ArrayList<ChartDataDto>();
            DatasetDto datasetDto = new DatasetDto(category.getName());
            datasetDto.setColor("#"+ Integer.toHexString(color));
            color+=0xFFF;
            datasetDto.setCategoryField("date");

            List<String[]> chartProperties = chartMetaDataRepository.findPropertyNameValuePairByChartId("");

            List<StockEvent> stockEvents = new ArrayList<StockEvent>();
            StockEvent stockEvent = new StockEvent();
            stockEvent.setDate(new Date());
            stockEvent.setText("S");
            stockEvent.setDescription("This is a description");
            stockEvent.setBackgroundColor("#85CDE6");
            stockEvent.setGraph("g1");
            stockEvent.setType("sign");
            stockEvents.add(stockEvent);
            List<IndicatorData> indicatorDataList=indicatorDataRepository.findAllByIndicatorId(category.getId());
            for(IndicatorData indicatorData:indicatorDataList){
                ChartDataDto chartDataDto=new ChartDataDto(indicatorData.getEffectfromdate().getTime(),indicatorData.getIndicatorvalue(),20);
                chartDataDtoList.add(chartDataDto);
            }


            List<FieldMapping> fieldMappings=new ArrayList<FieldMapping>();
            fieldMappings.add(new FieldMapping("value","value"));
            fieldMappings.add(new FieldMapping("volume","volume"));
            datasetDto.setFieldMappings(fieldMappings);


            datasetDto.setDataProvider(chartDataDtoList);
            Map<String,Object> thresholdsList=new HashMap();

            List<IndicatorThreshold> thresholds=indicatorThresholdRepository.findThresholdByIndicatorId(category.getId());
            if(PreConditions.isNotEmpty(thresholds))
                for(IndicatorThreshold i:thresholds){
                    Threshold threshold=new Threshold(i.getThreshold_end(),i.getThreshold_start(),i.getThreshHoldCategory());
                    thresholdsList.put(threshold.getName(),threshold);
                    thresholdsList.put("direction",i.getDirection());
                }
            datasetDto.setThresholdValues(thresholdsList);
//            datasetDto.setCategoryField(ChartUtil.getPropertyValue("xaxislabel",chartProperties));

            chartDatsets.add(datasetDto);
        }

        return  chartDatsets;
    }
    @Override
    public boolean validate(ChartRequest chartRequest) throws ValidationException {
        // TODO Auto-generated method stub
        return true;
    }
}
