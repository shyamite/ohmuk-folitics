package com.ohmuk.folitics.service;

import com.ohmuk.folitics.enums.CategoryType;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Category;
import org.apache.log4j.Logger;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

/**
 * Service implementation for entity: {@link Category}
 * 
 * @author Abhishek
 *
 */
@Service
@Transactional
public class CategoryService implements ICategoryService {

	private static Logger logger = Logger.getLogger(CategoryService.class);

	@Autowired
	private SessionFactory _sessionFactory;
	@PersistenceContext
	EntityManager entityManager;

	private Session getSession() {
		return _sessionFactory.getCurrentSession();
	}

	/**
	 * This method to add new {@link Category}
	 * 
	 * @author Mayank Sharma
	 * @return {@link Category}
	 * @throws Exception
	 */
	@Override
	public Category create(Category category) throws Exception {
		logger.debug("Inside category serivice create method");
		if("SubCategory".equals(category.getType())){
			Category parent=read(category.getCategoryId());
			List<Category> parents=new ArrayList<>();
			parents.add(parent);
			category.setParents(parents);
		}
		if("Indicator".equals(category.getType())){
			Category parent=read(category.getSubCategoryId());
			List<Category> parents=new ArrayList<>();
			parents.add(parent);
			category.setParents(parents);
		}

		Long id = (Long) getSession().save(category);
		logger.debug("Exiting from category serivice create method");
		return read(id);
	}

	/**
	 * This method return {@link Category} by id
	 * 
	 * @author Mayank Sharma
	 * @return {@link Category}
	 * @throws Exception
	 */
	@Override
	public Category read(Long id) throws Exception {
		return (Category) getSession().get(Category.class, id);
	}

	/**
	 * This method return list of all {@link Category}
	 * 
	 * @author Mayank Sharma
	 * @return List< {@link Category} >
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Category> readAll() throws Exception {
		logger.debug("Inside category service readAll method");
		Criteria criteria = getSession().createCriteria(Category.class);
		List<Category> categories = criteria.list();
		logger.debug("Exiting from category serivice readAll method");
		return categories;
	}

	/**
	 * This method update {@link Category}
	 * 
	 * @author Mayank Sharma
	 * @return {@link Category}
	 * @throws Exception
	 */
	@Override
	public Category update(Category category) throws Exception {
		logger.debug("Inside category service update method");
		if (null != category.getId()) {
			getSession().update(category);
			logger.debug("Exiting from category serivice readAll method");
			return read(category.getId());
		}
		logger.debug("Category id is null");
		logger.debug("Exiting from category serivice readAll method");
		return null;
	}

	/**
	 * This mehtod give list of all active {@link Category}
	 * 
	 * @author Mayank Sharma
	 * @return List<Category>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Category> readAllActiveCategories(Set<String> setOfStates) throws Exception {
		logger.debug("Inside category service readAllActiveCategories method");
		Criteria criteria = getSession().createCriteria(Category.class);
		criteria.add(Restrictions.not(Restrictions.in("status", setOfStates)));
		criteria.add(Restrictions.eq("type", CategoryType.CATEGORY.getValue()));
		List<Category> categories = criteria.list();
		//if(PreConditions.isNotEmpty(categories))
			//Hibernate.initialize(categories);
		logger.debug("Exiting from category serivice readAllActiveCategories method");
		return categories;
	}

	/**
	 * @author gautam.yadav
	 * @return List< {@link Category} > : list of subcategory which are not
	 *         deleted and disabled
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Category> readAllActiveSubCategories(Set<String> setOfStates) throws Exception {
		logger.info("Inside category service ActiveSubCategories method");
		Criteria criteria = getSession().createCriteria(Category.class);
		criteria.add(Restrictions.eq("type", CategoryType.SUBCATEGORY.getValue()));
		criteria.add(Restrictions.not(Restrictions.in("status", setOfStates)));
		List<Category> categories = criteria.list();
		logger.debug("Exiting category service readAllActiveSubCategories method");
		return categories;
	}

	/**
	 * TODO
	 * 
	 * @author krishan.shukla
	 * @return List< {@link Category} > : list of indicators specifically mapped
	 *         to the SubCategory which are not deleted and disabled
	 * @throws Exception
	 */

	@Override
	public List<Category> readAllActiveSubCategoriesByCategoryId(String categoryID) throws MessageException, Exception {
		logger.info("Inside category service readAllActiveSubCategoriesByCategoryId method");
		Criteria criteria = getSession().createCriteria(Category.class);
		criteria.add(Restrictions.eq("status", ComponentState.ACTIVE.getValue()));
		criteria.add(Restrictions.eq("id", Long.valueOf(categoryID)));
		List<Category> subcategoriesAndIndicators = criteria.list();
		for (Category subcat : subcategoriesAndIndicators) {
			Hibernate.initialize(subcat.getChilds());
		}
		// Hibernate.initialize(subcategoriesAndIndicators.getChilds());
		logger.debug("Exiting category service readAllActiveSubCategories method");
		return subcategoriesAndIndicators;
	}

	@Override
	public List<Category> readIndicatorsBySubCategoryId(String subCategoryID) throws MessageException, Exception {
		logger.info("Inside get category service ActiveIndicators");
		Criteria criteria = getSession().createCriteria(Category.class);
		criteria.add(Restrictions.eq("status", ComponentState.ACTIVE.getValue()));
		criteria.add(Restrictions.eq("id", Long.valueOf(subCategoryID)));
		List<Category> indicators = criteria.list();
		for (Category subcat : indicators) {
			Hibernate.initialize(subcat.getChilds());

		}
		logger.info("Exiting form category service ActiveIndicators");
		return indicators;
	}

	/**
	 * @author gautam.yadav
	 * @return List< {@link Category} > : list of indicators which are not
	 *         deleted and disabled
	 * @throws Exception
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Category> readAllActiveIndicators(Set<String> setOfStates) throws Exception {
		logger.info("Inside get category service ActiveIndicators");
		Criteria criteria = getSession().createCriteria(Category.class);
		criteria.add(Restrictions.eq("type", CategoryType.INDICATOR.getValue()));
		criteria.add(Restrictions.not(Restrictions.in("status", setOfStates)));
		List<Category> indicators = criteria.list();
		logger.info("Exiting form category service ActiveIndicators");
		return indicators;
	}

	@SuppressWarnings("unchecked")
	public List<Category> getKeyIndicators(Set<String> setOfStates) throws Exception {
		logger.info("Inside get category service ActiveIndicators");
		org.hibernate.Query query = getSession().createQuery("from Category c where c.keyIndicator=1  and c.status='Active'");
		List<Category> indicators = query.list();
		logger.info("Exiting form category service ActiveIndicators");
		return indicators;
	}

	/**
	 * This method delete {@link Category} with category id
	 * 
	 * @author Mayank Sharma
	 * @return boolean
	 * @throws Exception
	 */
	@Override
	public boolean delete(Long id) throws Exception {
		logger.info("Inside category service delete method");
		Category category = read(id);
		getSession().delete(category);
		logger.info("Exiting from category service delete");
		return true;
	}

	/**
	 * This method delete category
	 * 
	 * @author Mayank Sharma
	 * @return boolean
	 * @throws Exception
	 */
	@Override
	public boolean delete(Category category) throws Exception {
		logger.info("Inside category service delete method");
		category = read(category.getId());
		getSession().delete(category);
		logger.info("Exiting from category service delete");
		return true;
	}

	/**
	 * This method find Category by name
	 * 
	 * @author Mayank Sharma
	 * @return List<Category>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Category> search1(String name) throws Exception {
		logger.info("Inside category service search method");
		Criteria criteria = getSession().createCriteria(Category.class);
		criteria.add(Restrictions.eq("name", name));
		List<Category> categories = criteria.list();
		logger.info("Exiting category service search method");
		return categories;
	}

	/**
	 * Method is to get all {@link Category} by type
	 * 
	 * @param string
	 * @return List<{@link Category}>
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Category> findByType(String string) {
		logger.info("Inside category service findByType method");
		List<Category> categories = getSession().createCriteria(Category.class).add(Restrictions.eq("type", "category"))
				.list();
		logger.info("Exiting category service findByType method");
		return categories;
	}


	@Override
	public Map getCategoryByIndicatorId(Long indicatorId) throws Exception {
		logger.info("Inside get category service ActiveIndicators");
		Map<String,Object> result=new HashMap<>();
		javax.persistence.Query query = entityManager.createNativeQuery("select * from Category c,CategoryHierarchy ch" +
						" where c.type='SubCategory' and ch.parentId=c.id and ch.childId=:indicatorId",
				Category.class);
		query.setParameter("indicatorId",indicatorId);
		Category subCategory= (Category) query.getSingleResult();
		subCategory.getChilds();
		result.put("parent",subCategory);

		query = entityManager.createNativeQuery("select * from Category c,CategoryHierarchy ch" +
						" where c.type='Category' and ch.parentId=c.id and ch.childId=:subCategoryId",
				Category.class);
		query.setParameter("subCategoryId",subCategory.getId());

		Category category= (Category) query.getSingleResult();

		category.getChilds();
		result.put("category",category);

		result.put("subcategories",readAllActiveSubCategoriesByCategoryId(String.valueOf(category.getId())));

		logger.info("Exiting form category service ActiveIndicators");

		return result;
	}

	@Override
	public  List<Category> search(String term){
		FullTextEntityManager fullTextEntityManager= Search.getFullTextEntityManager(entityManager);

		QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory()
				.buildQueryBuilder().forEntity(Category.class).get();


		Query jpaQuery = queryBuilder.phrase().withSlop(2).onField("nGramName")
				.andField("edgeNGramName").boostedTo(5)
				.sentence(term.toLowerCase()).createQuery();

		FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(
				jpaQuery, Category.class);


		Criteria criteria=_sessionFactory.openSession().createCriteria(Category.class);
		criteria.add(Restrictions.eq("type", "Indicator"));
		fullTextQuery.setCriteriaQuery(criteria);
		fullTextQuery.setMaxResults(20);

		Sort sort = new Sort(new SortField("name_sort", SortField.Type.STRING) );
		fullTextQuery.setSort( sort );

		@SuppressWarnings("unchecked")
		List<Category> results = fullTextQuery.getResultList();
		return results;
	}


	public List<Category> wordSearch(String text) throws  Exception {

		FullTextEntityManager fullTextEntityManager= Search.getFullTextEntityManager(entityManager);

		QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory()
				.buildQueryBuilder().forEntity(Category.class).get();

		// a very basic query by keywords
		org.apache.lucene.search.Query query =queryBuilder
				.keyword()
				.onFields("name","description")
				.matching(text)
				.createQuery();


		javax.persistence.Query jpaQuery =
				fullTextEntityManager.createFullTextQuery(query, Category.class);


		List results = jpaQuery.getResultList();


		return results;
	}


}
