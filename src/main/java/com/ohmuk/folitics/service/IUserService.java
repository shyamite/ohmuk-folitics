package com.ohmuk.folitics.service;

import com.ohmuk.folitics.hibernate.entity.User;

/**
 * 
 * @author Mayank Sharma
 *
 */
public interface IUserService extends IBaseService {
	/**
	 * Method is to add {@link User}
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public User create(User user) throws Exception;
}
