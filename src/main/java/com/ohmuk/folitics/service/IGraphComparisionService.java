package com.ohmuk.folitics.service;

import java.util.List;
import java.util.Set;

import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.GraphComparisonFilter;

public interface IGraphComparisionService {
    
    
    public GraphComparisonFilter create(GraphComparisonFilter graphComparisonFilter) throws MessageException, Exception;
    
    public GraphComparisonFilter read(Long id) throws MessageException, Exception;

    public List<GraphComparisonFilter> readGraphFilters(String graphSegment, Set<String> setOfStatusFlag) throws MessageException, Exception;

    

}
