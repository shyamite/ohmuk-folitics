package com.ohmuk.folitics.service;

import java.util.List;

import com.ohmuk.folitics.util.PreConditions;
import org.hibernate.*;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.IndicatorData;
import com.ohmuk.folitics.hibernate.entity.IndicatorMetaData;
import com.ohmuk.folitics.util.DateUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

@Service
@Transactional
public class IndicatorDataService implements IIndicatorDataService {


    private static final String GET_TRACKER="select cat.id,idata.indicatorvalue,idata.thresholdcategory,cat.name from IndicatorData idata " +
            "where idata.category.status='Active' and idata.category.type='Indicator'";

    @Autowired
    IIndicatorThresholdService indicatorThresholdRepository;

    @Autowired
    IIndicatorWeightedDataService indicatorWeightedDataRepository;

    @Autowired
    ICategoryService categoryRepository;

    private static Logger logger = LoggerFactory.getLogger(IndicatorDataService.class);

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    /**
     * Method is to add {@link IndicatorData}
     * @param IndicatorData
     * @throws MessageException
     */
    @Override
    public IndicatorData create(IndicatorData indicatordata) throws MessageException {
        Long indicatorDataId = (Long) getSession().save(indicatordata);
        logger.debug("indicatordata is save of id: " + indicatorDataId);
        logger.info("Exiting from indicatorDataService create method");
        return (IndicatorData) getSession().get(IndicatorData.class, indicatorDataId);
    }

    /**
     * Method is to get {@link IndicatorData} by id
     * @param id
     * @return {@link IndicatorData}
     */
    @Override
    public IndicatorData read(Long id) throws Exception {
        logger.info("Inside IndicatorDataService create method");
        IndicatorData indicatorData = (IndicatorData) getSession().get(IndicatorData.class, id);
        return indicatorData;
    }

    /**
     * Method is to hard delete {@link IndicatorData} by id
     * @param id
     * @return {@link IndicatorData}
     */
    @Override
    public boolean deleteFromDB(Long id) throws Exception {
        logger.info("Inside IndicatorDataService create method");
        IndicatorData indicatorData = (IndicatorData) getSession().get(IndicatorData.class, id);
        getSession().delete(indicatorData);
        return true;
    }

    /**
     * Method is to get all {@link IndicatorData}
     * @return List< {@link Category} >
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<IndicatorData> readAll() throws Exception {
        logger.info("Inside IndicatorDataService create method");
        return getSession().createCriteria(IndicatorData.class).list();
    }

    /**
     * Method is to update {@link IndicatorData}
     * @return boolean
     */
    @Override
    public IndicatorData update(IndicatorData indicatordata) throws Exception {
        logger.info("Inside IndicatorDataService create method");
        indicatordata.setEditTime(DateUtils.getSqlTimeStamp());
        getSession().update(indicatordata);
        return indicatordata;
    }

    /**
     * Method is soft delete {@link IndicatorData} by id
     * @param id
     * @return boolean
     * @throws Exception
     */
    @Override
    public boolean delete(Long id) throws Exception {
        logger.info("Inside IndicatorDataService create method");
        IndicatorData indicatorData = (IndicatorData) getSession().get(IndicatorData.class, id);
        indicatorData.setState(ComponentState.DELETED.getValue());
        getSession().update(indicatorData);
        return true;

    }

    /**
     * Method is to soft delete {@link IndicatorData}
     * @param indicatordata
     * @return boolean
     * @throws Exception
     */
    @Override
    public boolean delete(IndicatorData indicatorData) throws Exception {
        logger.info("Inside IndicatorDataService create method");
        indicatorData.setState(ComponentState.DELETED.getValue());
        getSession().update(indicatorData);
        return true;
    }

    /**
     * Method is to find {@link IndicatorData} by latest {@link Category}
     * @param category
     * @return IndicatorData
     */
    @Override
    public IndicatorData findByCategoryLatest(Category category) {
        ProjectionList proj = Projections.projectionList();
        proj.add(Projections.max("id"));
        DetachedCriteria latestIndicatorId = DetachedCriteria.forClass(IndicatorData.class).setProjection(proj)
                .add(Restrictions.eq("category", category));

        Criteria crit = getSession().createCriteria(IndicatorData.class, "fl");
        crit.add(Property.forName("fl.category").eq(category));
        crit.add(Restrictions.eq("category", category));
        List<IndicatorData> l = crit.list();
        System.out.println(l.get(0).getId());
        return l.get(0);
    }

    /**
     * Method is to get all {@link IndicatorData} order by recently added
     * @return List<IndicatorData>
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<IndicatorData> findAllIndicatorDataLatest() {
        SQLQuery query = getSession()
                .createSQLQuery(
                        "Select * from IndicatorData i JOIN  Category category where i.indicatorId=category.id and i.id in (select max(iData.id) from IndicatorData  iData group by iData.indicatorId) order by i.editTime");
        query.addEntity(IndicatorData.class);
        List<IndicatorData> indicatorDataList = query.list();

        return indicatorDataList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<IndicatorData> findIndicatorDataForVerdict() {
        SQLQuery query = getSession()
                .createSQLQuery(
                        "SELECT * FROM   IndicatorData s WHERE( SELECT  COUNT(*) FROM IndicatorData  f WHERE f.indicatorId = s.indicatorId AND   f.id >= s.id  ) <= 2 order by indicatorId");
        query.addEntity(IndicatorData.class);
        List<IndicatorData> indicatorDataList = query.list();

        return indicatorDataList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<IndicatorData> findByCategory(Long categoryId) {
        SQLQuery query = getSession().createSQLQuery(
                "select * from IndicatorData where indicatorId ="+categoryId+" order by editTime");
        query.addEntity(IndicatorData.class);
        List<IndicatorData> indicatorDataList = query.list();

        return indicatorDataList;
    }

    @Override
    public List<IndicatorData> findIndicatorDataByIndicatorId(Long indicatorID) {
        logger.info("Inside findIndicatorDataByIndicatorId method in IndicatorDataService");
        Criteria criteria = getSession().createCriteria(IndicatorData.class);
        criteria.add(Restrictions.eq("category.id", Long.valueOf(indicatorID)));
        List<IndicatorData> indicatorData = criteria.list();
        /*for (Category subcat : indicators) {
            Hibernate.initialize(subcat.getChilds());

        }*/
        logger.info("Exiting form findIndicatorDataByIndicatorId method in IndicatorDataService");
        return indicatorData;
        
    }

    @Override
    public List<IndicatorMetaData> readIndicatorMetaData(String indicatorID) {
        logger.info("Inside readIndicatorMetaData method in IndicatorDataService");
        Criteria criteria = getSession().createCriteria(IndicatorMetaData.class);
        criteria.add(Restrictions.eq("category.id", Long.valueOf(indicatorID)));
        List<IndicatorMetaData> indicatorMetaData = criteria.list();
        /*for (Category subcat : indicators) {
            Hibernate.initialize(subcat.getChilds());

        }*/
        logger.info("Exiting form readIndicatorMetaData method in IndicatorDataService");
        return indicatorMetaData;
    }

    @Override
    public List<IndicatorData> getLatestIndicators() {
        List<IndicatorData> id=null;
        Query query=getSession().createQuery("SELECT o FROM IndicatorData o JOIN FETCH o.category i WHERE o.id in (select max(iData.id) from IndicatorData  iData group by iData.category.id)  order by o.editTime");
        id=query.list();
        if(PreConditions.isNotEmpty(id))
        Hibernate.initialize(id);

        return id;
    }

}
