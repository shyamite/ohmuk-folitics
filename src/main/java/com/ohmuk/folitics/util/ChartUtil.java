package com.ohmuk.folitics.util;

import java.util.List;

/**
 * Created by WENKY on 7/9/2017.
 */
public class ChartUtil {

    public static String getPropertyValue(String propertyName, List<String[]> propertyNameValuePairs){
        for(String[] propertyNameValuePair:propertyNameValuePairs){
            if(PreConditions.isNotEmpty(propertyNameValuePair)&&propertyNameValuePair.length==2 && PreConditions.isNotEmpty(propertyNameValuePair[0])){

                switch(propertyNameValuePair[0]){
                    case "xaxislabel":
                        return propertyNameValuePair[1];
                    case "yaxislabel":
                        return propertyNameValuePair[1];
                    case "title":
                        return propertyNameValuePair[1];
                }
            }

        }
        return null;

    }

}
