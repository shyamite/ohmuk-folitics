package com.ohmuk.folitics.util;

import antlr.StringUtils;

import java.util.Collection;

/**
 * Created by WENKY on 6/26/2017.
 */
public class PreConditions {

    public static boolean isEmpty(Object o){
        if(o==null)
            return true;
        if(o instanceof Collection)
            return ((Collection) o).isEmpty();
        if(o instanceof String)
            return ((String) o).length()==0;
        return false;
    }
    public static boolean isNotEmpty(Object o){
        return !isEmpty(o);
    }

    public static String removeSpaces(String o){
        return o.replace(" ","");
    }

    public static boolean isNumber(String page) {
        if(isEmpty(page))
            return false;
        try{
            Integer.parseInt(page);
        }catch (Exception e){
            return false;
        }
        return true;



    }
}
