package com.ohmuk.folitics.jpa.repository.module;

import com.ohmuk.folitics.hibernate.entity.IndicatorThreshold;
import com.ohmuk.folitics.hibernate.entity.IndicatorWeightedData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IndicatorWeightedDataRepository extends JpaRepository<IndicatorWeightedData, Long> {

//    @Query(value="from IndicatorWeightedData where category.id =:indicatorId" ,nativeQuery= false)
    @Query(value="from  IndicatorWeightedData where indicatorId =:indicatorId ",nativeQuery = false)
    IndicatorWeightedData findByIndicatorId(@Param("indicatorId") Long indicatorId);

}
