package com.ohmuk.folitics.jpa.repository.module;

import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.ChartMetaData;
import com.ohmuk.folitics.hibernate.entity.ComponentModuleStorage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

public interface ChartMetaDataRepository extends JpaRepository<ChartMetaData, Long> {

    @Query(value="SELECT c.propertyName,c.propertyValue FROM ChartMetaData c where c.chart.chartID=:id")
    List<String[]> findPropertyNameValuePairByChartId(@Param("id") String id);

    @Query(value="SELECT c.propertyName,c.propertyValue FROM ChartMetaData c where c.chart.chartID=:id and c.propertyName=:propertyname")
    List<String[]> findPropertyValuePairByChartIdAndPropertyName(@Param("id") String id ,@Param("propertyname") String propertyname);
}
