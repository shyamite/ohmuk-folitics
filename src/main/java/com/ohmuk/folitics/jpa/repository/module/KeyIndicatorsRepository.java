package com.ohmuk.folitics.jpa.repository.module;

import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.IndicatorDataTmp;
import com.ohmuk.folitics.hibernate.entity.KeyIndicators;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface KeyIndicatorsRepository extends JpaRepository<KeyIndicators, Long> {

    @Query("from KeyIndicators where active=:status")
    public List<KeyIndicators> findByStatus(@Param("status") boolean status);
}
