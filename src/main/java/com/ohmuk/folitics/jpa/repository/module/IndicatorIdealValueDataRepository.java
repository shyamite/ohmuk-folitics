package com.ohmuk.folitics.jpa.repository.module;

import com.ohmuk.folitics.hibernate.entity.IndicatorIdealValueData;
import com.ohmuk.folitics.hibernate.entity.IndicatorMetaData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IndicatorIdealValueDataRepository extends JpaRepository<IndicatorIdealValueData, Long> {

}
