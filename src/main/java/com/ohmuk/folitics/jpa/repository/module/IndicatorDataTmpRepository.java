package com.ohmuk.folitics.jpa.repository.module;

import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.IndicatorData;
import com.ohmuk.folitics.hibernate.entity.IndicatorDataTmp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IndicatorDataTmpRepository extends JpaRepository<IndicatorDataTmp, Long> {

    @Query("from IndicatorDataTmp where isInserted=:status")
    public List<IndicatorDataTmp> findByStatus(@Param("status") String status);
}
