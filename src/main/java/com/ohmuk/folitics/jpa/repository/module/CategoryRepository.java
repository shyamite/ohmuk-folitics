package com.ohmuk.folitics.jpa.repository.module;

import com.ohmuk.folitics.hibernate.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Query(value="from  Category c where c.type='Indicator' and c.status='Active' and c.keyIndicator=1")
    List<Category> findAllActiveKeyIndicators();

    @Query(value="SELECT * FROM Category c where c.id in(select ch.childId  from CategoryHierarchy  ch where ch.parentId=:id )and c.type='Indicator'",nativeQuery = true)
    List<Category> findIndicatorsBySubCategory(@Param("id") String id);


    @Query(value="from Category c where c.type='Indicator'")
    List<Category> findAllIndicators();

    @Query(value="from Category c where c.type='Indicator' and c.name like concat(:name,'%')")
    Category findIndicatorByName(@Param("name") String name);
}
