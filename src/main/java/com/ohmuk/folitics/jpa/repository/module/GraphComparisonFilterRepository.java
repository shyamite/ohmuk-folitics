package com.ohmuk.folitics.jpa.repository.module;

import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.GraphComparisonFilter;
import com.ohmuk.folitics.hibernate.entity.IndicatorData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface GraphComparisonFilterRepository extends JpaRepository<GraphComparisonFilter, Long> {

    @Query(value = "from GraphComparisonFilter filter where filter.statusFlag=:status")
    List<GraphComparisonFilter> findAllFiltersByStatus(@Param("status") String status);
}
