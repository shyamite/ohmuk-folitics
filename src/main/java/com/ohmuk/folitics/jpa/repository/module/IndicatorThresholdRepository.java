package com.ohmuk.folitics.jpa.repository.module;

import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.IndicatorMetaData;
import com.ohmuk.folitics.hibernate.entity.IndicatorThreshold;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IndicatorThresholdRepository extends JpaRepository<IndicatorThreshold, Long> {

    @Query(value="select * from indicatorthreshold where indicatorId =:id order by threshold_start",nativeQuery = true)
    List<IndicatorThreshold> findThresholdByIndicatorId(@Param("id") double id);

    @Query(value="from IndicatorThreshold where indicatorId =:id and threshHoldCategory=:thresholdCategory ",nativeQuery = false)
    IndicatorThreshold findIndicatorThresholdId(@Param("id") Long id,@Param("thresholdCategory") String thresholdCategory);


}
