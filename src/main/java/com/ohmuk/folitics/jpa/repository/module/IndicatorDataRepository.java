package com.ohmuk.folitics.jpa.repository.module;

import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.IndicatorData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface IndicatorDataRepository extends JpaRepository<IndicatorData, Long> {

    @Query(value="SELECT * FROM Category  where cast(id as char) like :id% and type='Indicator'",nativeQuery = true)
    List<Category> findIndicatorsBySubCategory(@Param("id") String id);

    @Query(value="Select * from IndicatorData where id in (select max(id) from IndicatorData group by indicatorId) order by editTime",nativeQuery = true)
    public List<IndicatorData> findAllIndicatorDataLatest();

    @Query(value="from IndicatorData i where i.category.id =:id order by createTime")
    public List<IndicatorData> findAllByIndicatorId(@Param("id") Long id);

    @Query(value="select * from IndicatorData where  indicatorId = :id order by editTime desc limit 1 ",nativeQuery = true)
    public IndicatorData findLatestDataByIndicatorId(@Param("id") Long id);

    @Query(value="Select * from IndicatorData where indicatorId=:indicatorId and effectfromDate between :startDate and :endDate  order by effectfromDate",nativeQuery = true)
    public List<IndicatorData> findIndicators(@Param("indicatorId")Long indicatorId,@Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
