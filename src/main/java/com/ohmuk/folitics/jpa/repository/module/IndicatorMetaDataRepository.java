package com.ohmuk.folitics.jpa.repository.module;

import com.ohmuk.folitics.hibernate.entity.ChartMetaData;
import com.ohmuk.folitics.hibernate.entity.IndicatorMetaData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IndicatorMetaDataRepository extends JpaRepository<IndicatorMetaData, Long> {

}
