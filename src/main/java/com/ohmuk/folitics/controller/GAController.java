package com.ohmuk.folitics.controller;

import com.ohmuk.folitics.businessDelegate.implementations.GABusinessDelegate;
import com.ohmuk.folitics.dto.ApiResponse;
import com.ohmuk.folitics.dto.ManifestoDto;
import com.ohmuk.folitics.dto.graphs.DatasetDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * Created by WENKY on 7/1/2017.
 */
@RestController
@RequestMapping("/api/ga")
public class GAController {
    @Autowired
    GABusinessDelegate gaBusinessDelegate;

    @RequestMapping(value="/getActiveCategories",method = RequestMethod.GET)
    public ApiResponse getActiveSubCategories(){
        ApiResponse apiResponse=new ApiResponse();
        return apiResponse;
    }


    @RequestMapping(value="/manifesto",method = RequestMethod.GET)
    public ManifestoDto getManifesto(){
       ManifestoDto manifestoDto=new ManifestoDto();
        manifestoDto.setImage("images/big_news.jpg");
        manifestoDto.setTitle("Manifesto is Modi meeting Promises");
        return manifestoDto;
    }


    @RequestMapping(value="/promises",method = RequestMethod.GET)
    public ApiResponse promises(){
        ApiResponse apiResponse=new ApiResponse();
        return apiResponse;
    }



    @RequestMapping(value="/latestIndicators",method = RequestMethod.GET)
    public ApiResponse tracker(){
        ApiResponse apiResponse=new ApiResponse();
        return apiResponse;
    }

    @RequestMapping(value="/keyindicators/get",method = RequestMethod.GET)
    public ApiResponse getKeyIndicators(){
        ApiResponse apiResponse=new ApiResponse();
        gaBusinessDelegate.getKeyIndicators();
        return apiResponse;
    }


    @RequestMapping(value={"/chart/subcategory/{subcategoryId}"},method = RequestMethod.GET)
    public List<DatasetDto> subCategory(@PathVariable String subcategoryId, Model model){
        
        List<DatasetDto> dataSets=new ArrayList<>();
        DatasetDto  congress=new DatasetDto("Congress 09-14");
        DatasetDto first100Days=new DatasetDto("First 100 Days");
        DatasetDto highestEver=new DatasetDto("Highest Ever");
        DatasetDto lowestEver=new DatasetDto("Lowest Ever");
        dataSets.add(congress);
        dataSets.add(first100Days);
        dataSets.add(highestEver);
        dataSets.add(lowestEver);
        return dataSets;
    }

    @RequestMapping(value={"/chart/indicator/{indicatorId}"},method = RequestMethod.GET)
    public List<DatasetDto> indicator(@PathVariable String indicatorId, Model model){

        List<DatasetDto> dataSets=new ArrayList<>();
        DatasetDto  congress=new DatasetDto("Congress 09-14");
        DatasetDto first100Days=new DatasetDto("First 100 Days");
        DatasetDto highestEver=new DatasetDto("Highest Ever");
        DatasetDto lowestEver=new DatasetDto("Lowest Ever");
        dataSets.add(congress);
        dataSets.add(first100Days);
        dataSets.add(highestEver);
        dataSets.add(lowestEver);
        return dataSets;
    }

    @RequestMapping(value={"/chart/keyindicators"},method = RequestMethod.GET)
    public List<DatasetDto> keyIndicators(Model model){

        List<DatasetDto> dataSets=new ArrayList<>();
        DatasetDto  congress=new DatasetDto("Congress 09-14");
        DatasetDto first100Days=new DatasetDto("First 100 Days");
        DatasetDto highestEver=new DatasetDto("Highest Ever");
        DatasetDto lowestEver=new DatasetDto("Lowest Ever");
        dataSets.add(congress);
        dataSets.add(first100Days);
        dataSets.add(highestEver);
        dataSets.add(lowestEver);
        return dataSets;
    }
}
