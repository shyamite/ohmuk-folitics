package com.ohmuk.folitics.controller;

import com.ohmuk.folitics.businessDelegate.implementations.AdminBusinessDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by WENKY on 7/25/2017.
 */
@RestController
@RequestMapping("/api/admin")
public class AdminController {

    @Autowired
    AdminBusinessDelegate adminBusinessDelegate;

    @RequestMapping(value={"/keyindicators/add/{indicatorId}"},method = RequestMethod.GET)
    public void addKeyIndicators(@PathVariable String indicatorId){
       adminBusinessDelegate.updateCategory(indicatorId,true);
    }

    @RequestMapping(value={"/keyindicators/remove/{indicatorId}"},method = RequestMethod.GET)
    public void removeKeyIndicators(@PathVariable String indicatorId){
        adminBusinessDelegate.updateCategory(indicatorId,false);
    }
}
