package com.ohmuk.folitics.controller;

import com.ohmuk.folitics.businessDelegate.DataBusinessDalegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by WENKY on 7/23/2017.
 */
@RestController
@RequestMapping("/api/data")
public class DataController {

    @Autowired
    DataBusinessDalegate dataBusinessDalegate;
    @RequestMapping("/indicators/thresholds/load")
    public void load() throws Exception{
        dataBusinessDalegate.loadIndicatorThreshold();
    }

    @RequestMapping("/indicators/weights/add")
    public String addIndicatorWeights() throws Exception{
        dataBusinessDalegate.loadIndicatorWeightedData();
        return "success";
    }

    @RequestMapping("/indicators/idealvalues/add")
    public String addIndicatorIdealValues() throws Exception{
        dataBusinessDalegate.loadIndicatorIdealValues();
        return "success";
    }

    @RequestMapping("/indicators/data/load")
    public String loadIndicatorData() throws Exception{
        dataBusinessDalegate.loadIndicatorData();
        return "success";
    }

}
