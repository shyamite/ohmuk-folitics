package com.ohmuk.folitics.controller;

import com.ohmuk.folitics.businessDelegate.interfaces.IGraphComparisonBusinessDelegate;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.GraphComparisonFilter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/graphcomparison")
public class GraphComparisonController {

    private static Logger logger = Logger.getLogger(GraphComparisonController.class);
    
    @Autowired
    private IGraphComparisonBusinessDelegate graphComparisonbusinessDelegate;

   /* @RequestMapping
    public String getCategoryPage() {
        return "graphcomparison-page";
    }
    */
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<GraphComparisonFilter> add(@Validated @RequestBody GraphComparisonFilter graphComparisonFilter) {

        logger.info("Inside add method");

        try {
            graphComparisonFilter = graphComparisonbusinessDelegate.create(graphComparisonFilter);

            if (graphComparisonFilter != null) {

                logger.info("Exiting add method");
                return new ResponseDto<GraphComparisonFilter>(true, graphComparisonFilter, "graphComparison filter added successfully");
            }
        } catch (MessageException e) {

            logger.error("Exception while adding graphComparisonFilter", e);

            return new ResponseDto(false, null, e.getMessage());
        }

        catch (Exception e) {

            logger.error("Exception while adding graphComparisonFilter", e);

            return new ResponseDto(false, null, e.getMessage());
        }

        return new ResponseDto<GraphComparisonFilter>("Data not added", false);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<GraphComparisonFilter> update(@RequestBody GraphComparisonFilter graphComparisonFilter) {

        logger.info("Inside add method");

        try {
            graphComparisonFilter = graphComparisonbusinessDelegate.update(graphComparisonFilter);

            if (graphComparisonFilter != null) {

                logger.info("Exiting add method");
                return new ResponseDto<GraphComparisonFilter>(true, graphComparisonFilter, "graphComparison filter updated successfully");
            }
        }
        catch (Exception e) {

            logger.error("Exception while adding graphComparisonFilter", e);

            return new ResponseDto(false, null, e.getMessage());
        }

        return new ResponseDto<GraphComparisonFilter>("Data not added", false);
    }

    @RequestMapping(value = "/getGraphFilters", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<GraphComparisonFilter> getGraphFilters(@RequestParam(required=false,value="graphSegment") String graphSegment,
            @RequestParam(required=false,value="statusFlag") String[] statusFlag) {
            
        logger.info("Ïnside getGraphFilters in GraphComparisonController");
        try {
            List<GraphComparisonFilter> graphComparisonFilter = graphComparisonbusinessDelegate.readGraphFilters(graphSegment,statusFlag);
            if (graphComparisonFilter != null) {
                return new ResponseDto<GraphComparisonFilter>(true, graphComparisonFilter, "getting active filters successfully");
            }
        } catch (MessageException e) {

            logger.error("Exception while getting active filters", e);

            return new ResponseDto(false, null, e.getMessage());
        } catch (Exception e) {

            logger.error("Exception while fetching active filters", e);
            return new ResponseDto(false, null, e.getMessage());
        }

        return new ResponseDto<GraphComparisonFilter>("no any active filters found", false);
        
        
        
        
        
    }
    
    
    
}
