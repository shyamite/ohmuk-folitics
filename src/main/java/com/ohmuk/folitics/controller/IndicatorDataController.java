package com.ohmuk.folitics.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohmuk.folitics.scheduler.IndicatorDataScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.beans.ChangeIndicatorDataBean;
import com.ohmuk.folitics.beans.HeaderDataBean;
import com.ohmuk.folitics.beans.TrackPromiseBean;
import com.ohmuk.folitics.businessDelegate.interfaces.IIndicatorDataBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IIndicatorThresholdBusinessDelegate;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.IndicatorData;
import com.ohmuk.folitics.hibernate.entity.IndicatorMetaData;
import com.ohmuk.folitics.hibernate.entity.IndicatorThreshold;

@Controller
@RequestMapping("/indicatordata")
public class IndicatorDataController {

    @Autowired
    private volatile IIndicatorDataBusinessDelegate indicatorBusinessDelegate;
    
    @Autowired
    private volatile IIndicatorThresholdBusinessDelegate threshHoldBusinessDelegate;
    @Autowired
    IndicatorDataScheduler indicatorDataScheduler;


    private static Logger logger = LoggerFactory.getLogger(IndicatorDataController.class);

    /**
     * Web service is to add {@link IndicatorData}
     * @param indicatordata
     * @return {@link ResponseDto<IndicatorData>}
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<IndicatorData> add(@RequestBody IndicatorData indicatordata) {
        logger.info("Inside IndicatorDataController add method");
        try {
            indicatordata = indicatorBusinessDelegate.create(indicatordata);
        } catch (Exception exception) {
            logger.error("Exception in adding IndicatorData");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController add method");
            return new ResponseDto<IndicatorData>(false);
        }
        if (indicatordata != null) {
            logger.debug("IndicatorData is added");
            logger.info("Exiting from IndicatorDataController add method");
            return new ResponseDto<IndicatorData>(true, indicatordata);
        }
        logger.debug("IndicatorData is not added");
        logger.info("Exiting from IndicatorDataController add method");
        return new ResponseDto<IndicatorData>(false);
    }

    /**
     * Web service to get all {@link IndicatorData}
     * @return
     */
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<IndicatorData> getAll() {
        logger.info("Inside IndicatorDataController getAll method");
        List<IndicatorData> components = null;
        try {
            components = indicatorBusinessDelegate.readAll();
        } catch (Exception exception) {
            logger.error("Exception in get all IndicatorData");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController getAll method");
            return new ResponseDto<IndicatorData>(false);
        }
        if (components != null) {
            logger.debug("List of IndicatorData is found");
            logger.info("Exiting from IndicatorDataController getAll method");
            return new ResponseDto<IndicatorData>(true, components);
        }
        logger.debug("List of IndicatorData is not found");
        logger.info("Exiting from IndicatorDataController getAll method");
        return new ResponseDto<IndicatorData>(false);
    }

    /**
     * Web service is to update {@link IndicatorData}
     * @param indicatordata
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<IndicatorData> edit(@RequestBody IndicatorData indicatordata) {
        logger.info("Inside IndicatorDataController edit method");
        try {
            indicatordata = indicatorBusinessDelegate.update(indicatordata);
        } catch (Exception exception) {
            logger.error("Exception in editing IndicatorData");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController edit method");
            return new ResponseDto<IndicatorData>(false);
        }
        if (indicatordata != null) {
            logger.debug("IndicatorData is edited");
            logger.info("Exiting from IndicatorDataController edit method");
            return new ResponseDto<IndicatorData>(true, indicatordata);
        }
        logger.debug("IndicatorData is not edited");
        logger.info("Exiting from IndicatorDataController edit method");
        return new ResponseDto<IndicatorData>(false);
    }

    /**
     * Web service is to delete {@link IndicatorData} by id
     * @param id
     * @return
     */
    @RequestMapping(value = "/deletebyid", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<IndicatorData> delete(Long id) {
        logger.info("Inside IndicatorDataController delete method");
        try {
            if (indicatorBusinessDelegate.delete(id)) {
                logger.debug("IndicatorData is soft deleted");
                logger.info("Exiting from IndicatorDataController delete method");
                return new ResponseDto<IndicatorData>(true);
            }
        } catch (Exception exception) {
            logger.error("Exception in deleting IndicatorData");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController delete method");
            return new ResponseDto<IndicatorData>(false);
        }
        logger.debug("IndicatorData is not deleted");
        logger.info("Exiting from IndicatorDataController delete method");
        return new ResponseDto<IndicatorData>(false);
    }

    /**
     * Web service is to soft delete {@link IndicatorData}
     * @param indicatordata
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<IndicatorData> delete(@RequestBody IndicatorData indicatordata) {
        logger.info("Inside IndicatorDataController delete method");
        try {
            if (indicatorBusinessDelegate.delete(indicatordata)) {
                logger.debug("IndicatorData is soft deleted");
                logger.info("Exiting from IndicatorDataController delete method");
                return new ResponseDto<IndicatorData>(true);
            }
        } catch (Exception exception) {
            logger.error("Exception in deleting IndicatorData");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController delete method");
            return new ResponseDto<IndicatorData>(false);
        }
        logger.debug("IndicatorData is not deleted");
        logger.info("Exiting from IndicatorDataController delete method");
        return new ResponseDto<IndicatorData>(false);
    }

    /**
     * Web service is to get {@link IndicatorData} by id
     * @param id
     * @return
     */
    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<IndicatorData> find(Long id) {
        logger.info("Inside IndicatorDataController find method");
        IndicatorData indicatordata = null;
        try {
            indicatordata = indicatorBusinessDelegate.read(id);
        } catch (Exception exception) {
            logger.error("Exception in finding IndicatorData");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController find method");
            return new ResponseDto<IndicatorData>(false);
        }
        if (indicatordata != null) {
            logger.debug("IndicatorData with id: " + indicatordata.getId() + " is found");
            logger.info("Exiting from IndicatorDataController find method");
            return new ResponseDto<IndicatorData>(true, indicatordata);
        }
        logger.debug("IndicatorData is not found");
        logger.info("Exiting from IndicatorDataController find method");
        return new ResponseDto<IndicatorData>(false);
    }
        
    /**
     * Web service is to get {@link IndicatorData} by id
     * @param id
     * @return
     */
    @RequestMapping(value = "/findIndicators/{indicatorID}", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<IndicatorData> findIndicatorData(@PathVariable("indicatorID") Long indicatorID  ) {
        logger.info("Inside IndicatorDataController indicatorID method");
        List<IndicatorData> indicatordata = null;
        try {
            indicatordata = indicatorBusinessDelegate.readIndicatorData(indicatorID);
        } catch (Exception exception) {
            logger.error("Exception in finding IndicatorData in findIndicatorData method in controller");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController findIndicatorData method");
            return new ResponseDto<IndicatorData>(false);
        }
        if (indicatordata != null) {
        //    logger.debug("IndicatorData with id: " + indicatordata.getId() + " is found");
            logger.info("Exiting from IndicatorDataController findIndicatorData method");
            return new ResponseDto<IndicatorData>(true, indicatordata);
        }
        logger.debug("IndicatorData is not found");
        logger.info("Exiting from IndicatorDataController findIndicatorData method");
        return new ResponseDto<IndicatorData>(false);
    }
    
    
    
    /**
     * Web service is to get {@link IndicatorData} by id
     * @param id
     * @return
     */
    @RequestMapping(value = "/findIndicatorsMetaData/{indicatorID}", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<IndicatorMetaData> findIndicatorsMetaData(@PathVariable("indicatorID") String indicatorID  ) {
        logger.info("Inside IndicatorDataController of findIndicatorsMetaData method");
        List<IndicatorMetaData> indicatormetadata = null;
        try {
            indicatormetadata = indicatorBusinessDelegate.readIndicatorMetaData(indicatorID);
        } catch (Exception exception) {
            logger.error("Exception in finding IndicatorData in findIndicatorsMetaData method in controller");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController findIndicatorsMetaData method");
            return new ResponseDto<IndicatorMetaData>(false);
        }
        if (indicatormetadata != null) {
        //    logger.debug("IndicatorData with id: " + indicatordata.getId() + " is found");
            logger.info("Exiting from IndicatorDataController findIndicatorsMetaData method");
            return new ResponseDto<IndicatorMetaData>(true, indicatormetadata);
        }
        logger.debug("IndicatorData is not found");
        logger.info("Exiting from IndicatorDataController findIndicatorsMetaData method");
        return new ResponseDto<IndicatorMetaData>(false);
    }
    
    
    
    
    

    /**
     * Web service is to get {@link HeaderDataBean}
     * @return
     */
    @RequestMapping(value = "/getHeaderData", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<HeaderDataBean> getHeaderData() {
        logger.info("Inside IndicatorDataController add method");
        List<HeaderDataBean> headerDataBeanList = null;
        try {
            headerDataBeanList = indicatorBusinessDelegate.readAllByThresholdGroup();
        } catch (Exception exception) {
            logger.error("Exception in getting get header data IndicatorData");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController getHeaderData method");
            return new ResponseDto<HeaderDataBean>(false);
        }
        if (headerDataBeanList != null) {
            logger.debug("HeaderData is found");
            logger.info("Exiting from IndicatorDataController getHeaderData method");
            return new ResponseDto<HeaderDataBean>(true, headerDataBeanList);
        }
        logger.debug("HeaderData is not found");
        logger.info("Exiting from IndicatorDataController getHeaderData method");
        return new ResponseDto<HeaderDataBean>(false);
    }

    /**
     * Web service is to get {@link TrackPromiseBean}
     * @return
     */
    @RequestMapping(value = "/getPromiseData", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<TrackPromiseBean> getPromiseData() {
        logger.info("Inside IndicatorDataController add method");
        List<TrackPromiseBean> trackPromiseBeanList = null;
        try {
            trackPromiseBeanList = indicatorBusinessDelegate.readAllBySubCategory();
        } catch (Exception exception) {
            logger.error("Exception in getting promise data IndicatorData");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController getPromiseData method");
            return new ResponseDto<TrackPromiseBean>(false);
        }
        if (trackPromiseBeanList != null) {
            logger.debug("TrackPromise is found");
            logger.info("Exiting from IndicatorDataController getPromiseData method");
            return new ResponseDto<TrackPromiseBean>(true, trackPromiseBeanList);
        }
        logger.debug("TrackPromise is not found");
        logger.info("Exiting from IndicatorDataController getPromiseData method");
        return new ResponseDto<TrackPromiseBean>(false);
    }

    /**
     * Web service is to hard delete {@link IndicatorData} by id
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteFromDB", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<IndicatorData> deleteFromDB(Long id) {
        logger.info("Inside IndicatorDataController add method");
        try {
            if (indicatorBusinessDelegate.deleteFromDB(id)) {
                logger.debug("IndicatorData is hard deleted");
                logger.info("Exiting from IndicatorDataController deleteFromDB method");
                return new ResponseDto<IndicatorData>(true);
            }
        } catch (Exception exception) {
            logger.error("Exception in hard deleting IndicatorData");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController deleteFromDB method");
            return new ResponseDto<IndicatorData>(false);
        }

        logger.debug("IndicatorData is not deleted");
        logger.info("Exiting from IndicatorDataController deleteFromDB method");
        return new ResponseDto<IndicatorData>(false);
    }
    
    @RequestMapping(value = "/getIndicatorDataForVerdict", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<ChangeIndicatorDataBean> getIndicatorDataForVerdict() {
    	  logger.info("Inside IndicatorDataController getAll method");
    	  List<ChangeIndicatorDataBean> components = null;
          try {
              components = indicatorBusinessDelegate.findIndicatorDataForVerdict();
          } catch (Exception exception) {
              logger.error("Exception in get all IndicatorData");
              logger.error("Exception: " + exception);
              logger.info("Exiting from IndicatorDataController getAll method");
              return new ResponseDto<ChangeIndicatorDataBean>(false);
          }
          if (components != null) {
              logger.debug("List of IndicatorData is found");
              logger.info("Exiting from IndicatorDataController getAll method");
              return new ResponseDto<ChangeIndicatorDataBean>(true, components);
          }
          logger.debug("List of IndicatorData is not found");
          logger.info("Exiting from IndicatorDataController getAll method");
          return new ResponseDto<ChangeIndicatorDataBean>(false);
    }
    
    
    @RequestMapping(value = "/getIndicatorDataWithBaseLine", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<IndicatorData> getIndicatorDataWithBaseLine(Long categoryId) {
    	logger.info("Inside IndicatorDataController find method");
    	
    	Category category = new Category();
    	category.setId(categoryId);
        IndicatorData indicatordata = null;
        try {
            List<IndicatorThreshold> indicatorThreshHoldList  = threshHoldBusinessDelegate.findByCategory(category);
            List<IndicatorData> indicatorDataList = indicatorBusinessDelegate.findByCategory(category);
           
        } catch (Exception exception) {
            logger.error("Exception in finding IndicatorData");
            logger.error("Exception: " + exception);
            logger.info("Exiting from IndicatorDataController find method");
            return new ResponseDto<IndicatorData>(false);
        }
        if (indicatordata != null) {
            logger.debug("IndicatorData with id: " + indicatordata.getId() + " is found");
            logger.info("Exiting from IndicatorDataController find method");
            return new ResponseDto<IndicatorData>(true, indicatordata);
        }
        logger.debug("IndicatorData is not found");
        logger.info("Exiting from IndicatorDataController find method");
        return new ResponseDto<IndicatorData>(false);
    }


    @RequestMapping(value = "/tracker", method = RequestMethod.GET)
    public @ResponseBody Object tracker(Model model) {

        ResponseDto responseDto=null;
        try {

            indicatorBusinessDelegate.getIndicatorsByThreshold(model);
            responseDto=new ResponseDto(model.asMap().get("indicators"),true);

            return responseDto;
        }catch (Exception e){
            e.printStackTrace();
            responseDto=new ResponseDto(false);
        }


        return responseDto;

    }



    @RequestMapping(value = "/load", method = RequestMethod.GET)
    public @ResponseBody Object load(Model model) {

        ResponseDto responseDto=null;
        try {

           indicatorDataScheduler.indicatorScheduler();
            responseDto=new ResponseDto(true);
            return responseDto;
        }catch (Exception e){
            e.printStackTrace();
            responseDto=new ResponseDto(false);
        }


        return responseDto;

    }
}
