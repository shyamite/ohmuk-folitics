package com.ohmuk.folitics.enums;

public enum GraphSegmentType {
     GA("GA"), GPI("GPI"), FEEL("FEEL");

    private String value;

    private GraphSegmentType(String value) {
        this.setValue(value);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static final GraphSegmentType getGraphSegmentType(String value) {
        if (GA.getValue().equals(value)) {
            return GA;
        }
        if (GPI.getValue().equals(value)) {
            return GPI;
        }
        if (FEEL.getValue().equals(value)) {
            return FEEL;
        }

        return null;
    }
}
