package com.ohmuk.folitics.scheduler;

import com.ohmuk.folitics.businessDelegate.implementations.SearchBusinessDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by WENKY on 7/18/2017.
 */
@Component
public class SearchScheduler {
    @Autowired
    SearchBusinessDelegate searchBusinessDelegate;

    //@Scheduled(cron = "0 0 23 * * * * ")
    @Scheduled(fixedDelay = 24*60*60*1000)
    public void startIndexing(){
        searchBusinessDelegate.init();
    }
}
