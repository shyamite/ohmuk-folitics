package com.ohmuk.folitics.scheduler;

import com.mongodb.util.Hash;
import com.ohmuk.folitics.businessDelegate.implementations.IndicatorDataBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IIndicatorDataBusinessDelegate;
import com.ohmuk.folitics.dto.WorldBankResponseDto;
import com.ohmuk.folitics.dto.worldbank.Data;
import com.ohmuk.folitics.hibernate.entity.IndicatorData;
import com.ohmuk.folitics.hibernate.entity.IndicatorMetaData;
import com.ohmuk.folitics.jpa.repository.module.CategoryRepository;
import com.ohmuk.folitics.jpa.repository.module.IndicatorDataRepository;
import com.ohmuk.folitics.jpa.repository.module.IndicatorMetaDataRepository;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.PreConditions;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by WENKY on 7/9/2017.
 */
@Service
@Transactional
public class IndicatorDataScheduler {

@Autowired
IndicatorMetaDataRepository indicatorMetaDataRepository;
    @Autowired
    IndicatorDataRepository indicatorDataRepository;
    @Autowired
    IIndicatorDataBusinessDelegate indicatorDataBusinessDelegate;
@Autowired
    CategoryRepository categoryRepository;
    public void indicatorScheduler(){
        List<IndicatorMetaData> indicatorMetaDataList=indicatorMetaDataRepository.findAll();
        for(IndicatorMetaData indicatorMetaData:indicatorMetaDataList){
            if("1".equals(indicatorMetaData.getState()))
                continue;
         String src=indicatorMetaData.getIndicatorDetailLink();//"http://api.worldbank.org/countries/in/indicators/NV.AGR.TOTL.KD.ZG?format=json";
            try{
             int pages=1;
                int page=1;
                List<Map> response=new ArrayList<Map>();
                     do{
                         RestTemplate restTemplate=new RestTemplate();
                         String pageuri="&page="+page;
                         ResponseEntity<List> responseEntity=restTemplate.getForEntity(src+pageuri, List.class);

                         response=responseEntity.getBody();

                         if(response.size()>0){
                             if(response.get(0) instanceof HashMap){
                                pages= (int) response.get(0).get("pages");
                                 page=(int)response.get(0).get("page");
                             }
                         }
                         if(response.size()>1){
                             List<HashMap> indicators= (List<HashMap>) response.get(1);
                             for(HashMap m:indicators){
                                 if(PreConditions.isNotEmpty(m.get("value"))){
                                     IndicatorData indicatorData=new IndicatorData();
                                     indicatorData.setCategory(indicatorMetaData.getCategory());
                                     indicatorData.setCategoryID(indicatorMetaData.getCategory().getId());
                                     indicatorData.setCreateTime(DateUtils.getSqlTimeStamp());
                                     indicatorData.setIndicatorvalue(Double.parseDouble((String)m.get("value")));
                                     indicatorData.setEditTime(DateUtils.getTimeStamp(Integer.parseInt((String) m.get("date"))));
                                     indicatorData.setDeleted(1l);
                                     indicatorData.setState("1");


                                     indicatorData.setUpdateddate(DateUtils.getSqlTimeStamp()  );

                                     indicatorData.setEffectfromdate(indicatorData.getEditTime());
                                     indicatorData.setScore(Double.parseDouble((String)m.get("value")));
                                     System.out.println(m.get("date")+"-->"+m.get("value"));
                                     indicatorDataBusinessDelegate.create(indicatorData);

                                 }
                             }
                         }
                        page++;
                     }while(page<=pages);

            indicatorMetaData.setState("1");
         }catch (Exception e){
            e.printStackTrace();
         }
        }

        indicatorDataRepository.flush();
    }
    
}
