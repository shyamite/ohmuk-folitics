package com.ohmuk.folitics.businessDelegate.implementations;

import com.mysql.jdbc.StringUtils;
import com.ohmuk.folitics.businessDelegate.interfaces.IGraphComparisonBusinessDelegate;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.enums.GraphSegmentType;
import com.ohmuk.folitics.hibernate.entity.GraphComparisonFilter;
import com.ohmuk.folitics.jpa.repository.module.GraphComparisonFilterRepository;
import com.ohmuk.folitics.service.IGraphComparisionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@Transactional
public class GraphComparisonBusinessDelegate implements IGraphComparisonBusinessDelegate {

    private static Logger logger = LoggerFactory.getLogger(GraphComparisonBusinessDelegate.class);

    @Autowired
    private volatile IGraphComparisionService graphComparisionService;
    @Autowired
    GraphComparisonFilterRepository graphComparisonFilterRepository;

    @Override
    public GraphComparisonFilter create(GraphComparisonFilter graphComparisonFilter) throws Exception {
        // TODO Auto-generated method stub

        logger.info("Inside create method in GraphComparison Business delegate");
        GraphComparisonFilter graphComparisonData = graphComparisionService.create(graphComparisonFilter);
        logger.info("Exiting create method in GraphComparison Business delegate");
        return graphComparisonData;

    }

    @Override
    public List<GraphComparisonFilter> readGraphFilters(String graphSegment, String[] statusFlags) throws Exception {
        logger.info("Inside readGraphFilters method in business delegate");

        Set<String> setOfStatusFlag = new HashSet<String>();

        if (statusFlags != null) {
            for (String statusFlag : statusFlags) {
                switch (statusFlag.toUpperCase()) {
                case "ACTIVE":
                    setOfStatusFlag.add(ComponentState.ACTIVE.getValue());
                    break;
                case "DELETED":
                    setOfStatusFlag.add(ComponentState.DELETED.getValue());
                    break;
                case "DISABLED":
                    setOfStatusFlag.add(ComponentState.DISABLED.getValue());
                    break;
                default:
                    setOfStatusFlag.add(ComponentState.ACTIVE.getValue());
                    setOfStatusFlag.add(ComponentState.DELETED.getValue());
                    setOfStatusFlag.add(ComponentState.DISABLED.getValue());
                }
            }
            }       
            else{
                setOfStatusFlag.add(ComponentState.ACTIVE.getValue());
                setOfStatusFlag.add(ComponentState.DELETED.getValue());
                setOfStatusFlag.add(ComponentState.DISABLED.getValue());
            }
        

        if (StringUtils.isNullOrEmpty(graphSegment))
            graphSegment = GraphSegmentType.GA.getValue();
        else
            graphSegment = GraphSegmentType.getGraphSegmentType(graphSegment).getValue();
        logger.info("Exiting readGraphFilters method in business delegate");
        return graphComparisionService.readGraphFilters(graphSegment, setOfStatusFlag);
    }

    @Override
    public GraphComparisonFilter update(GraphComparisonFilter graphComparisonFilter) {
        GraphComparisonFilter tmp=graphComparisonFilterRepository.findOne(graphComparisonFilter.getId());

        tmp.setStatusFlag(graphComparisonFilter.getStatusFlag());
        return tmp;
    }

}
