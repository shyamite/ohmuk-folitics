package com.ohmuk.folitics.businessDelegate.implementations;

import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.KeyIndicators;
import com.ohmuk.folitics.jpa.repository.module.CategoryRepository;
import com.ohmuk.folitics.service.KeyIndicatorService;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.PreConditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by WENKY on 7/25/2017.
 */
@Service
@Transactional
public class AdminBusinessDelegate {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    KeyIndicatorService keyIndicatorService;


    public void updateCategory(String indicatorId,Boolean isKeyIndicator){
            Category category=categoryRepository.findOne(Long.parseLong(indicatorId));
            category.setKeyIndicator(isKeyIndicator);
    }

    public KeyIndicators addKeyIndicators(Long indicatorId){
       KeyIndicators keyIndicator= keyIndicatorService.findById(indicatorId);
        if(PreConditions.isEmpty(keyIndicator)){
             keyIndicator=new KeyIndicators();
            keyIndicator.setCreatedDate(DateUtils.getSqlTimeStamp());
            keyIndicator.setUpdatedDate(DateUtils.getSqlTimeStamp());
            keyIndicator.setId(indicatorId);
            keyIndicatorService.save(keyIndicator);
        }
        keyIndicator.setActive(true);
        return keyIndicator;
    }


    public Object removeKeyIndicators(Long indicatorId){
        KeyIndicators keyIndicator= keyIndicatorService.findById(indicatorId);
        if(PreConditions.isEmpty(keyIndicator)){
            keyIndicator=new KeyIndicators();
            keyIndicator.setUpdatedDate(DateUtils.getSqlTimeStamp());
            keyIndicator.setId(indicatorId);
            keyIndicatorService.save(keyIndicator);
        }
        keyIndicator.setActive(false);
        return keyIndicator;
    }

}
