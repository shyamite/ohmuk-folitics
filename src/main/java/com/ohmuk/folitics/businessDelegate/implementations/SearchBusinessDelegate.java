package com.ohmuk.folitics.businessDelegate.implementations;

import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.GovtSchemeData;
import com.ohmuk.folitics.service.CategoryService;
import com.ohmuk.folitics.service.GovtSchemeDataService;
import com.ohmuk.folitics.service.ICategoryService;
import com.ohmuk.folitics.service.IGovtSchemeDataService;
import com.sun.org.apache.bcel.internal.generic.GOTO;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by WENKY on 7/19/2017.
 */
@Service
@Transactional
public class SearchBusinessDelegate {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    ICategoryService categoryService;
    @Autowired
    IGovtSchemeDataService govtSchemeDataService;



    public void init(){
            FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

            try {
                fullTextEntityManager.createIndexer().startAndWait();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

    }

    public Object search(String searchString){
        Map result=new HashMap();
       List<Category> categoryList=null;
        try{
            categoryList = categoryService.search(searchString);
        }catch (Exception e){
            e.printStackTrace();
        }
        List<GovtSchemeData> govtSchemeDataList= govtSchemeDataService.search(searchString);

        result.put("categories",categoryList);
        result.put("govtschemes",govtSchemeDataList);
        return result;
    }

    public Object searchSchemes(String searchString) {
        List<GovtSchemeData> govtSchemeDataList= govtSchemeDataService.search(searchString);

        return govtSchemeDataList;
    }
}
