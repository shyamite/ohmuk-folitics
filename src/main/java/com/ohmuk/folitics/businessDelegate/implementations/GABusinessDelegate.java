package com.ohmuk.folitics.businessDelegate.implementations;

import com.ohmuk.folitics.service.JudgeGovernmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by WENKY on 7/1/2017.
 */
@Service
@Transactional
public class GABusinessDelegate {
    @Autowired
    JudgeGovernmentService judgeGovernmentService;
    private Object keyIndicators;

    public Object getKeyIndicators() {
        return keyIndicators;
    }
}
