package com.ohmuk.folitics.businessDelegate.interfaces;

import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.GraphComparisonFilter;

import java.util.List;

public interface IGraphComparisonBusinessDelegate {

    GraphComparisonFilter create(GraphComparisonFilter graphComparisonFilter) throws MessageException, Exception;

    List<GraphComparisonFilter> readGraphFilters(String graphSegment, String[] statusFlag)
            throws MessageException, Exception;

    GraphComparisonFilter update(GraphComparisonFilter graphComparisonFilter);
}
