package com.ohmuk.folitics.businessDelegate;

import com.ohmuk.folitics.businessDelegate.implementations.IndicatorDataBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IIndicatorDataBusinessDelegate;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.*;
import com.ohmuk.folitics.jpa.repository.module.*;

import com.ohmuk.folitics.service.IIndicatorDataService;
import com.ohmuk.folitics.service.IIndicatorThresholdService;
import com.ohmuk.folitics.service.IIndicatorWeightedDataService;
import com.ohmuk.folitics.service.IndicatorDataService;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.PreConditions;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by WENKY on 7/23/2017.
 */
@Service
@Transactional
public class DataBusinessDalegate {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    IndicatorDataRepository indicatorDataRepository;
    @Autowired
    IndicatorThresholdRepository indicatorThresholdRepository;
    @Autowired
    IndicatorWeightedDataRepository indicatorWeightedDataRepository;
    @Autowired
    IndicatorIdealValueDataRepository indicatorIdealValueDataRepository;
    @Autowired
    IndicatorDataTmpRepository indicatorDataTmpRepository;
    @Value("${excel.loc}")
    String excelPath;
    @Value("${shared.path}")
    String sharedDirectory;
    @Autowired
    IIndicatorDataBusinessDelegate indicatorDataBusinessDelegate;
    @Autowired
    IIndicatorDataService indicatorDataService;
    @Autowired
    IIndicatorWeightedDataService indicatorWeightedDataService;
    @Autowired
    IIndicatorThresholdService indicatorThresholdService;
    private static Logger logger = LoggerFactory.getLogger(DataBusinessDalegate.class);

    enum IndicatorThresholdExcel{
        INDICATOR_ID,
        SUB_CATEGORY_NAME,
        INDICATOR_NAME,
        LINK_OF_DATA,
        FREQUENCY,
        SOURCE,
        FOOLINGUS_MIN,FOOLINGUS_MAX,
        BAD_MIN,BAD_MAX,
        GOOD_MIN,GOOD_MAX,
        EXCELLENT_MIN,EXCELLENT_MAX,
        DIRECTION,
        UNITS;


        int getValue() {
            return this.ordinal();
        }
    }


    enum IndicatorDataExcel{
        YEAR,VALUE;

        int getValue() {
            return this.ordinal();
        }
    }
    public void loadIndicatorData() throws Exception {

        System.out.println("wenkkkkk");
int count=0;
        Set<Long> indicatorIds=new HashSet<Long>();
            List<IndicatorDataTmp> indicatorDataTmpList = indicatorDataTmpRepository.findByStatus("N");

            for (IndicatorDataTmp indicatorDataTmp : indicatorDataTmpList) {

                    IndicatorData indicatorData = new IndicatorData();
                    indicatorData.setCategory(indicatorDataTmp.getCategory());
                    indicatorData.setCategoryID(indicatorDataTmp.getCategory().getId());
                    indicatorData.setScore(indicatorDataTmp.getIndicatorvalue());
                indicatorData.setIndicatorvalue(indicatorDataTmp.getIndicatorvalue());
                    String effectFromDate = indicatorDataTmp.getEffectFromDate();
                    if (effectFromDate.contains("-")) {
                        String year = effectFromDate.split("-")[0];
                        if (NumberUtils.isNumber(year)) {
                            indicatorData.setEffectfromdate(DateUtils.getTimeStamp(Integer.parseInt(year)));
                        } else {
                            SimpleDateFormat sdf = new SimpleDateFormat("MMM-YYYY");
                            Date effecfrom = sdf.parse(effectFromDate);
                            indicatorData.setEffectfromdate(DateUtils.getSqlTimeStamp(effecfrom.getTime()));
                        }
                    }else
                    indicatorData.setEffectfromdate(DateUtils.getTimeStamp(Integer.parseInt(effectFromDate)));



                    indicatorDataBusinessDelegate.create(indicatorData);
                    indicatorDataTmp.setIsInserted("Y");

            }
        System.out.println("count-->"+count);
        System.out.println(indicatorIds);
    }


    public void loadIndicatorDataOld() throws Exception {
        File f = new File(excelPath);
        FileInputStream excelFile = new FileInputStream(new File(excelPath));
        PrintWriter printWriter=new PrintWriter(sharedDirectory+"/indicatorData.sql");
        try {
            Workbook workbook = new XSSFWorkbook(excelFile);

            List<Category> indicators = categoryRepository.findAllIndicators();
            for(int i=0;i<workbook.getNumberOfSheets();i++){
                Sheet sheet=workbook.getSheetAt(i);
                Category category=categoryRepository.findIndicatorByName(sheet.getSheetName());
                if(PreConditions.isEmpty(category)){
                    printWriter.println("--Didn't find the indicator "+sheet.getSheetName());
                    continue;
                }

                Iterator<Row> iterator = sheet.iterator();

                boolean isContinue=false;
                while (iterator.hasNext()) {
                    try {
                        if(!isContinue) {
                            isContinue=true;
                            iterator.next();
                            continue;
                        }
                        Row row = iterator.next();
                        IndicatorData indicatorData=new IndicatorData();

                        indicatorData.setId(category.getId());
                        Object year = getCellValue(row.getCell(IndicatorDataExcel.YEAR.getValue()));
                        String _year=(String) year;
                        if(_year.contains("-"));
                        _year=_year.split("-")[0];
                        Object value=getCellValue(row.getCell(IndicatorDataExcel.VALUE.getValue()));
                        indicatorData.setIndicatorvalue((Double) value);
                        indicatorData.setEffectfromdate(DateUtils.getTimeStamp(Integer.parseInt(_year)));
                        indicatorData.setCreateTime(DateUtils.getSqlTimeStamp());
                        indicatorData.setEditTime(DateUtils.getSqlTimeStamp());
                        indicatorData.setUpdateddate(DateUtils.getSqlTimeStamp());
                        indicatorData.setScore((Double) value);

                        indicatorData = indicatorDataBusinessDelegate.create(indicatorData);

                        StringBuffer insert = new StringBuffer();
                        insert.append("INSERT INTO `folitics`.`indicatordata`(`id`,`indicatorId`,`createTime`,`editTime`,`updateddate`,`effectfromdate`,`deleted`,`delta`,`indicatorvalue`,`score`,`state`,`thresholdcategory`,`weightedIdealValue`,`weightedValue`,`actualValueRange`) VALUES");
                        insert.append(indicatorData);
                        printWriter.println(insert);
                    } catch (Exception e) {
                        printWriter.println("--Exception in reading Sheet " + sheet.getSheetName());
                    }
                }
            }

//            for (Category indicator : indicators) {
//
//                IndicatorData indicatorData = new IndicatorData();
//                indicatorData.setId(indicator.getId());
//                Sheet sheet = workbook.getSheet(indicator.getName());
//                if(PreConditions.isEmpty(sheet)) {
//                    printWriter.println("--Didn't find the sheet "+indicator.getName());
//                    continue;
//                }
//                Iterator<Row> iterator = sheet.iterator();
//
//                while (iterator.hasNext()) {
//                    try {
//                        Row row = iterator.next();
//
//                        Object year = getCellValue(row.getCell(IndicatorDataExcel.YEAR.getValue()));
//                        String _year=(String) year;
//                        if(_year.contains("-"));
//                        _year=_year.split("-")[0];
//                        Object value=getCellValue(row.getCell(IndicatorDataExcel.VALUE.getValue()));
//                        indicatorData.setIndicatorvalue((Double) value);
//                        indicatorData.setEffectfromdate(DateUtils.getTimeStamp(Integer.parseInt(_year)));
//                        indicatorData.setCreateTime(DateUtils.getSqlTimeStamp());
//                        indicatorData.setEditTime(DateUtils.getSqlTimeStamp());
//                        indicatorData.setUpdateddate(DateUtils.getSqlTimeStamp());
//                        indicatorData.setScore((Double) value);
//
//                        indicatorData = indicatorDataBusinessDelegate.create(indicatorData);
//
//                        StringBuffer insert = new StringBuffer();
//                        insert.append("INSERT INTO `folitics`.`indicatordata`(`id`,`indicatorId`,`createTime`,`editTime`,`updateddate`,`effectfromdate`,`deleted`,`delta`,`indicatorvalue`,`score`,`state`,`thresholdcategory`,`weightedIdealValue`,`weightedValue`,`actualValueRange`) VALUES");
//                        insert.append(indicatorData);
//                        printWriter.println(insert);
//                    } catch (Exception e) {
//                        printWriter.println("--Exception in reading Sheet " + indicator.getName());
//                    }
//                }
//
//            }
        }finally{
            excelFile.close();
            printWriter.close();
        }
    }


    public void loadIndicatorIdealValues() {

        List<Category> indicators=categoryRepository.findAllIndicators();
        for(Category indicator:indicators){

            IndicatorIdealValueData indicatorIdealValueData=new IndicatorIdealValueData();
            indicatorIdealValueData.setIndicatorid(indicator.getId());
            indicatorIdealValueData.setEditTime(new Timestamp(System.currentTimeMillis()));
            indicatorIdealValueData.setEffectivefromdate(new Timestamp(System.currentTimeMillis()));
            indicatorIdealValueData.setState("Active");
            indicatorIdealValueData.setUpdateddate(new Timestamp(System.currentTimeMillis()));
            indicatorIdealValueData.setDeleted(0l);
            indicatorIdealValueData.setCreateTime(new Timestamp(System.currentTimeMillis()));

            IndicatorThreshold indicatorThreshold=indicatorThresholdRepository.findIndicatorThresholdId(indicator.getId(),"Best Performance");
            double idealValue=0d;
            int i=0;
            if(PreConditions.isEmpty(indicatorThreshold))
                continue;
            if(PreConditions.isNotEmpty(indicatorThreshold.getThreshold_start())) {
                idealValue += indicatorThreshold.getThreshold_start();
                i++;
            }
            if(PreConditions.isNotEmpty(indicatorThreshold.getThreshold_end())) {
                idealValue += indicatorThreshold.getThreshold_end();
                i++;
            }

            if(i!=0) {
                indicatorIdealValueData.setIndicatoridealvalue((long) idealValue / i*indicatorThreshold.getDirection());
                indicatorIdealValueDataRepository.save(indicatorIdealValueData);
            }
        }
    }


    public void loadIndicatorWeightedData(){
        List<Category> indicators=categoryRepository.findAllIndicators();
        for(Category indicator:indicators){
            IndicatorWeightedData indicatorWeightedData=new IndicatorWeightedData();
            indicatorWeightedData.setCategory(indicator);
            indicatorWeightedData.setEditTime(new Timestamp(System.currentTimeMillis()));
            indicatorWeightedData.setCreateTime(new Timestamp(System.currentTimeMillis()));
            indicatorWeightedData.setDeleted(0l);
            indicatorWeightedData.setWeightage(10l);
            indicatorWeightedData.setUpdateddate(new Timestamp(System.currentTimeMillis()));
            indicatorWeightedData.setState("Active");
            indicatorWeightedData.setImpactOnChart(0);
            indicatorWeightedData.setEffectivefromdate(new Timestamp((System.currentTimeMillis())));
            indicatorWeightedDataRepository.save(indicatorWeightedData);
        }
    }

    public void loadIndicatorThreshold() throws  Exception{
    File f=new File(excelPath);
        FileInputStream excelFile = new FileInputStream(new File(excelPath));
        try{
        Workbook workbook = new XSSFWorkbook(excelFile);

        for(int i=1;i<workbook.getNumberOfSheets();i++){
            Sheet datatypeSheet = workbook.getSheetAt(i);
            Iterator<Row> iterator = datatypeSheet.iterator();

            while(iterator.hasNext()) {
                Row row = iterator.next();
                if (PreConditions.isEmpty(row.getCell(IndicatorThresholdExcel.INDICATOR_ID.getValue())))
                    continue;
                IndicatorThreshold foolingUs = new IndicatorThreshold();
                Category category = categoryRepository.findOne((long) (row.getCell(IndicatorThresholdExcel.INDICATOR_ID.getValue()).getNumericCellValue()));

                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.FOOLINGUS_MIN.getValue()))))
                    foolingUs.setThreshold_start(row.getCell(IndicatorThresholdExcel.FOOLINGUS_MIN.getValue()).getNumericCellValue());
                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.FOOLINGUS_MAX.getValue()))))
                    foolingUs.setThreshold_end(row.getCell(IndicatorThresholdExcel.FOOLINGUS_MAX.getValue()).getNumericCellValue());
                foolingUs.setCategory(category);
                foolingUs.setThreshHoldCategory("Fooling US");
                foolingUs.setState("Active");
                foolingUs.setCreateTime(new Timestamp(System.currentTimeMillis()));
                foolingUs.setEditTime(new Timestamp(System.currentTimeMillis()));
                foolingUs.setDirection((long) row.getCell(IndicatorThresholdExcel.DIRECTION.getValue()).getNumericCellValue());
                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.UNITS.getValue()))))
                    foolingUs.setUnits(row.getCell(IndicatorThresholdExcel.UNITS.getValue()).getStringCellValue());

                indicatorThresholdRepository.save(foolingUs);

                IndicatorThreshold bad = new IndicatorThreshold();
                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.BAD_MIN.getValue()))))
                    bad.setThreshold_start(row.getCell(IndicatorThresholdExcel.BAD_MIN.getValue()).getNumericCellValue());

                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.BAD_MAX.getValue()))))
                    bad.setThreshold_end(row.getCell(IndicatorThresholdExcel.BAD_MAX.getValue()).getNumericCellValue());

                bad.setCategory(category);
                bad.setState("Active");
                bad.setCreateTime(new Timestamp(System.currentTimeMillis()));
                bad.setEditTime(new Timestamp(System.currentTimeMillis()));
                bad.setThreshHoldCategory("UnSatisfactory");
                bad.setDirection((long) row.getCell(IndicatorThresholdExcel.DIRECTION.getValue()).getNumericCellValue());
                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.UNITS.getValue()))))
                    bad.setUnits(row.getCell(IndicatorThresholdExcel.UNITS.getValue()).getStringCellValue());

                indicatorThresholdRepository.save(bad);

                IndicatorThreshold good = new IndicatorThreshold();
                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.GOOD_MIN.getValue()))))
                    good.setThreshold_start(row.getCell(IndicatorThresholdExcel.GOOD_MIN.getValue()).getNumericCellValue());
                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.GOOD_MAX.getValue()))))
                    good.setThreshold_end(row.getCell(IndicatorThresholdExcel.GOOD_MAX.getValue()).getNumericCellValue());
                good.setCategory(category);
                good.setState("Active");
                good.setCreateTime(new Timestamp(System.currentTimeMillis()));
                good.setEditTime(new Timestamp(System.currentTimeMillis()));
                good.setThreshHoldCategory("On Track");
                good.setDirection((long) row.getCell(IndicatorThresholdExcel.DIRECTION.getValue()).getNumericCellValue());
                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.UNITS.getValue()))))
                    good.setUnits(row.getCell(IndicatorThresholdExcel.UNITS.getValue()).getStringCellValue());

                indicatorThresholdRepository.save(good);


                IndicatorThreshold excellent = new IndicatorThreshold();
                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.EXCELLENT_MIN.getValue()))))
                    excellent.setThreshold_start(row.getCell(IndicatorThresholdExcel.EXCELLENT_MIN.getValue()).getNumericCellValue());
                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.EXCELLENT_MAX.getValue()))))
                    excellent.setThreshold_end(row.getCell(IndicatorThresholdExcel.EXCELLENT_MAX.getValue()).getNumericCellValue());
                excellent.setCategory(category);
                excellent.setState("Active");
                excellent.setCreateTime(new Timestamp(System.currentTimeMillis()));
                excellent.setEditTime(new Timestamp(System.currentTimeMillis()));
                excellent.setThreshHoldCategory("Best Performance");
                excellent.setDirection((long) row.getCell(IndicatorThresholdExcel.DIRECTION.getValue()).getNumericCellValue());
                if (PreConditions.isNotEmpty(getCellValue(row.getCell(IndicatorThresholdExcel.UNITS.getValue()))))
                    excellent.setUnits(row.getCell(IndicatorThresholdExcel.UNITS.getValue()).getStringCellValue());

                indicatorThresholdRepository.save(excellent);

            }
            }
        }finally{
            excelFile.close();
        }


    }

    private Object getCellValue(Cell cell){
        if(cell==null)
            return null;
        switch(cell.getCellType()){
            case 0:
                return cell.getNumericCellValue();
            case 3:
            case 1:
                return cell.getStringCellValue();
            default:
                System.err.println(""+cell.getCellType());
                return null;
        }

    }

}
