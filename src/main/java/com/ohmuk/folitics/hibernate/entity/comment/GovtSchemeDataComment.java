package com.ohmuk.folitics.hibernate.entity.comment;

import com.ohmuk.folitics.hibernate.entity.GovtSchemeData;
import com.ohmuk.folitics.util.DateUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Entity for like on entity: {@link GovtSchemeData}
 * 
 * @author Harish
 *
 */

@Entity
@Table(name = "govtschemedatacomment")
public class GovtSchemeDataComment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	@NotNull(message = "error.componentComment.componentId.notNull")
	private Long componentId;

	@Column(nullable = false)
	@NotNull(message = "error.componentComment.userId.notNull")
	private Long userId;

	@Column(nullable = false, length = 128)
	@NotNull(message = "error.componentComment.componentType.notNull")
	private String componentType;

	@Column(nullable = false)
	@NotNull(message = "error.componentComment.createdTime.notNull")
	private Timestamp createdTime;

	@Column(nullable = false)
	@NotNull(message = "error.componentComment.editedTime.notNull")
	private Timestamp editedTime;

	@Column(nullable = false, length = 512)
	@NotNull(message = "error.componentComment.comment.notNull")
	private String comment;

	@Column(nullable = false, length = 512)
	private String subject;

	@Column(nullable = false, length = 25, columnDefinition = "enum('Active','Disabled')")
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public GovtSchemeDataComment() {
		setCreatedTime(DateUtils.getSqlTimeStamp());
		setEditedTime(DateUtils.getSqlTimeStamp());
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the componentId
	 */
	public Long getComponentId() {
		return componentId;
	}

	/**
	 * @param componentId
	 *            the componentId to set
	 */
	public void setComponentId(Long componentId) {
		this.componentId = componentId;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the componentType
	 */
	public String getComponentType() {
		return componentType;
	}

	/**
	 * @param componentType
	 *            the componentType to set
	 */
	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	/**
	 * @return the createdTime
	 */
	public Timestamp getCreatedTime() {
		return createdTime;
	}

	/**
	 * @param createdTime
	 *            the createdTime to set
	 */
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * @return the editedTime
	 */
	public Timestamp getEditedTime() {
		return editedTime;
	}

	/**
	 * @param editedTime
	 *            the editedTime to set
	 */
	public void setEditedTime(Timestamp editedTime) {
		this.editedTime = editedTime;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

}
