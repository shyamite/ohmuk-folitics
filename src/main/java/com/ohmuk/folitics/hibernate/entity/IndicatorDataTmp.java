package com.ohmuk.folitics.hibernate.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "indicatorDataTmp")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
public class IndicatorDataTmp implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "indicatorId", nullable = false)
	private Category category;

	@Column(nullable = false)
	private String effectFromDate;

	@Column(nullable = false)
	private Double indicatorvalue;

	@Column(nullable = true)
	private String isInserted;

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getEffectFromDate() {
		return effectFromDate;
	}

	public void setEffectFromDate(String effectFromDate) {
		this.effectFromDate = effectFromDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getIndicatorvalue() {
		return indicatorvalue;
	}

	public void setIndicatorvalue(Double indicatorvalue) {
		this.indicatorvalue = indicatorvalue;
	}

	public String getIsInserted() {
		return isInserted;
	}

	public void setIsInserted(String isInserted) {
		this.isInserted = isInserted;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
}
