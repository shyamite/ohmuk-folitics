package com.ohmuk.folitics.hibernate.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.util.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import org.apache.lucene.analysis.core.KeywordTokenizerFactory;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.StopFilterFactory;
import org.apache.lucene.analysis.miscellaneous.WordDelimiterFilterFactory;
import org.apache.lucene.analysis.ngram.EdgeNGramFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.pattern.PatternReplaceFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;


/**
 * Entity implementation class for Entity: Category
 * 
 * @author Abhishek Patel
 */
@Entity
@Table(name = "category")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
@Indexed
@AnalyzerDefs({
		@AnalyzerDef(name = "autocompleteEdgeAnalyzer",
				tokenizer = @TokenizerDef(factory = KeywordTokenizerFactory.class),

				filters = {
						@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
								@Parameter(name = "pattern",value = "([^a-zA-Z0-9\\.])"),
								@Parameter(name = "replacement", value = " "),
								@Parameter(name = "replace", value = "all") }),
						@TokenFilterDef(factory = LowerCaseFilterFactory.class),
						@TokenFilterDef(factory = StopFilterFactory.class),
						// Index partial words starting at the front, so we can provide
						// Autocomplete functionality
						@TokenFilterDef(factory = EdgeNGramFilterFactory.class, params = {
								@Parameter(name = "minGramSize", value = "3"),
								@Parameter(name = "maxGramSize", value = "50") }) }),

		@AnalyzerDef(name = "autocompleteNGramAnalyzer",
				tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
				filters = {
						@TokenFilterDef(factory = WordDelimiterFilterFactory.class),
						@TokenFilterDef(factory = LowerCaseFilterFactory.class),
						@TokenFilterDef(factory = NGramFilterFactory.class, params = {
								@Parameter(name = "minGramSize", value = "3"),
								@Parameter(name = "maxGramSize", value = "5") }),
						@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
								@Parameter(name = "pattern",value = "([^a-zA-Z0-9\\.])"),
								@Parameter(name = "replacement", value = " "),
								@Parameter(name = "replace", value = "all") })
				}),

		@AnalyzerDef(name = "standardAnalyzer",
				tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
				filters = {
						@TokenFilterDef(factory = WordDelimiterFilterFactory.class),
						@TokenFilterDef(factory = LowerCaseFilterFactory.class),
						@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
								@Parameter(name = "pattern", value = "([^a-zA-Z0-9\\.])"),
								@Parameter(name = "replacement", value = " "),
								@Parameter(name = "replace", value = "all") })
				})
})
public class Category implements Serializable {

	private static final long serialVersionUID = 1L;



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Fields({
			@Field(name = "name_sort", index = Index.NO, store = Store.NO,
					analyze = Analyze.NO),

			@Field(name = "name", index = Index.YES, store = Store.YES,
					analyze = Analyze.YES, analyzer = @Analyzer(definition = "standardAnalyzer")),
			@Field(name = "edgeNGramName", index = Index.YES, store = Store.NO,
					analyze = Analyze.YES, analyzer = @Analyzer(definition = "autocompleteEdgeAnalyzer")),
			@Field(name = "nGramName", index = Index.YES, store = Store.NO,
					analyze = Analyze.YES, analyzer = @Analyzer(definition = "autocompleteNGramAnalyzer"))
	})
	@Column(nullable = false, length = 255)
	@NotNull(message = "error.category.name.notNull")
	@Size(min = 1, max = 255, message = "error.category.name.size")
	private String name;

	@Column(nullable = false, length = 255)
	@NotNull(message = "error.category.description.notNull")
	@Size(min = 1, max = 255, message = "error.category.description.size")
	private String description;

	@Column(nullable = true, length = 255)
	private String imageURL;

	@Column(nullable = false)
	@NotNull(message = "error.category.editTime.notNull")
	private Timestamp editTime;

	@Column(nullable = true)
	private Long editedBy;

	@Column(nullable = false)
	@NotNull(message = "error.category.createTime.notNull")
	private Timestamp createTime;

	@Column(nullable = false)
	@NotNull(message = "error.category.createdBy.notNull")
	private Long createdBy;

	/**
	 * In database though table name is Category but it comprises
	 * 'Category','SubCategory','Indicator' by setting type field.
	 * 
	 */

	@Column(nullable = false, length = 25, columnDefinition = "enum('Category','SubCategory','Indicator')")
	@NotNull(message = "error.category.type.notNull")
	private String type;

	@Column(nullable = false, length = 25, columnDefinition = "enum('Active','Disabled','Deleted')")
	@NotNull(message = "error.category.status.notNull")
	private String status;

	/**
	 * Krishan.shukla
	 * fetch = FetchType.EAGER has been added because on fethich subcategory alone there was lazy initialization error was coming - Need to confirm on this.
	 */
	@ManyToMany(fetch = FetchType.LAZY)
	@Cascade( value = CascadeType.DETACH)
	@JoinTable(name = "categoryHierarchy", joinColumns = @JoinColumn(name = "parentId", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "childId", referencedColumnName = "id"))
	private List<Category> childs;

	@ManyToMany
	@Cascade(value = CascadeType.DETACH)
	@JoinTable(name = "categoryHierarchy", joinColumns = @JoinColumn(name = "childId", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "parentId", referencedColumnName = "id"))
	private List<Category> parents;

	@Column(name="isKeyIndicator")
	private Boolean keyIndicator;


	@Transient
	private boolean thresholdState;


	@Transient
	private Long subCategoryId;


	@Transient
	private Long categoryId;


	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(Long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public boolean getThresholdState() {
		return thresholdState;
	}

	public void setThresholdState(boolean thresholdState) {
		this.thresholdState = thresholdState;
	}

	/*
	 * @OneToMany(mappedBy="category") private Set<IndicatorData> IndicatorData;
	 */

	public Category() {
		setTimestamp(DateUtils.getSqlTimeStamp());
		setEdited(DateUtils.getSqlTimeStamp());
		setStatus(ComponentState.ACTIVE.getValue());
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}


	public void setName(String category) {
		this.name = category;
	}


	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public Timestamp getEdited() {
		return this.editTime;
	}

	public void setEdited(Timestamp edited) {
		this.editTime = edited;
	}

	public Timestamp getTimestamp() {
		return this.createTime;
	}

	public void setTimestamp(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getEditTime() {
		return editTime;
	}

	public void setEditTime(Timestamp editTime) {
		this.editTime = editTime;
	}

	public Long getEditedBy() {
		return editedBy;
	}

	public void setEditedBy(Long editedBy) {
		this.editedBy = editedBy;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Category> getChilds() {
		return childs;
	}

	public void setChilds(List<Category> childs) {
		this.childs = childs;
	}

	public List<Category> getParents() {
		return parents;
	}

	public void setParents(List<Category> parents) {
		this.parents = parents;
	}

	public void addParent(Category parent) throws Exception {
		try {
			parents.add(parent);
		} catch (NullPointerException e) {
			throw new MessageException("Category object found null while adding it to the parent list in Category");
		} catch (Exception e) {
			throw new Exception("Exception occured while adding parent in parent list of Category");
		}
	}

	public void removeParent(Category parent) {
		if (parent != null) {
			parents.remove(parent);
		}
	}

	public void addChild(Category child) {
		if (child != null) {
			childs.add(child);
		}
	}

	public void removeChild(Category child) {
		if (child != null) {
			childs.remove(child);
		}
	}

	public boolean equals(Object object) {
		if (object == this)
			return true;
		if ((object == null) || !(object instanceof Category))
			return false;

		final Category a = (Category) object;

		if (id != null && a.getId() != null) {
			return id.equals(a.getId());
		}
		return false;
	}

	public Boolean getKeyIndicator() {
		return keyIndicator;
	}

	public void setKeyIndicator(Boolean keyIndicator) {
		this.keyIndicator = keyIndicator;
	}

	/*	*//**
			 * @return the indicatorData
			 */
	/*
	 * public Set<IndicatorData> getIndicatorData() { return IndicatorData; }
	 * 
	 *//**
		 * @param indicatorData
		 *            the indicatorData to set
		 *//*
		 * public void setIndicatorData(Set<IndicatorData> indicatorData) {
		 * IndicatorData = indicatorData; }
		 */
}
