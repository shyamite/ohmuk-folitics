package com.ohmuk.folitics.hibernate.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.ohmuk.folitics.dto.worldbank.Data;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;

/**
 * Created by WENKY on 7/23/2017.
 */
@Entity
@Table(name = "indicatorDataDetails")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
public class IndicatorDataDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "indicatorDataId", nullable = false)
    private IndicatorData indicatorData;

    @Column
    String indicatorName;
    @Column
    Double value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IndicatorData getIndicatorData() {
        return indicatorData;
    }

    public void setIndicatorData(IndicatorData indicatorData) {
        this.indicatorData = indicatorData;
    }

    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
