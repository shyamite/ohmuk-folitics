package com.ohmuk.folitics.hibernate.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.GregorianCalendar;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

/**
 * Entity implementation class for Entity: GraphComparisionFilter
 *
 */
@Entity
@Table(name = "graphComparisionFilter")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")

public class GraphComparisonFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // name , description , enable/disable/deleted flag , startDate , endDate ,
    // createTime, editDate ,

    @Column(nullable = false, length = 25)
    @NotNull(message = "error.graphComparisionFilter.name.notNull")
    @Size(min = 1, max = 255, message = "error.graphComparisionFilter.name.size")
    private String name;

    @Column(nullable = true, length = 255)
    private String description;

    @Column(nullable = false, length = 25, columnDefinition = "enum('Active','Disabled','Deleted')")
    @NotNull(message = "error.graphComparisionFilter.status.notNull")
    private String statusFlag;
    
    
    @Column(nullable = false, length = 25, columnDefinition = "enum('GA','GPI','Feel')")
    private String graphSegment;

    @Column(nullable = true)
   // @NotNull(message = "error.graphComparisionFilter.startDate.notNull")
    private Timestamp startDate;

    @Column(nullable = true)
   // @NotNull(message = "error.graphComparisionFilter.endDate.notNull")
    private Timestamp endDate;

    @Column(nullable = false)
    @CreationTimestamp
    @NotNull(message = "error.graphComparisionFilter.createTime.notNull")
    private Timestamp createTime = new Timestamp(GregorianCalendar.getInstance().getTimeInMillis());;
    
    
    @Column(nullable = false)
    @UpdateTimestamp
    @NotNull(message = "error.graphComparisionFilter.editTime.notNull")
    private Timestamp editTime = new Timestamp(GregorianCalendar.getInstance().getTimeInMillis());

    public GraphComparisonFilter() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }
    
    

    public String getGraphSegment() {
        return graphSegment;
    }

    public void setGraphSegment(String graphSegment) {
        this.graphSegment = graphSegment;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getEditTime() {
        return editTime;
    }

    public void setEditTime(Timestamp editTime) {
        this.editTime = editTime;
    }
    
    

}
