package com.ohmuk.folitics.hibernate.repository.comment;

import com.ohmuk.folitics.hibernate.entity.comment.GovtSchemeDataComment;

/**
 * Interface for entity: {@link GovtSchemeDataComment} repository
 * 
 * @author Harish
 *
 */
public interface IGovtSchemeDataCommentRepository extends
		ICommentHibernateRepository<GovtSchemeDataComment> {

	

}
