/*
-- Query: select * from indicatordata
LIMIT 0, 1000

-- Date: 2016-03-31 16:02

{
	"deleted": 0,
	"delta": -320,
	"effectfromdate": 1495518713,
	"idealValueRange": "4.0-8.0",
	"indicatorvalue": 12.0,
	"score": 6.32,
	"state": "1",
	"weightedIdealValue": 2000,
	"weightedValue": 6320,
	"indicatorId": "1111101"
}
*/
INSERT INTO `indicatordata` (`id`,`createTime`,`deleted`,`delta`,`editTime`,`effectfromdate`,`idealValueRange`,`indicatorvalue`,`score`,`state`,`thresholdcategory`,`updateddate`,`weightedIdealValue`,`weightedValue`,`indicatorId`) VALUES (1,CURRENT_TIMESTAMP(),0,-320,CURRENT_TIMESTAMP(),'1998-05-01 00:00:00','4.0-8.0',6.32,6,1,'On Track',CURRENT_TIMESTAMP(),6000,6320,1111101);
INSERT INTO `indicatordata` (`id`,`createTime`,`deleted`,`delta`,`editTime`,`effectfromdate`,`idealValueRange`,`indicatorvalue`,`score`,`state`,`thresholdcategory`,`updateddate`,`weightedIdealValue`,`weightedValue`,`indicatorId`) VALUES (2,CURRENT_TIMESTAMP(),0,3330,CURRENT_TIMESTAMP(),'1999-05-01 00:00:00','4.0-8.0',2.67,6,1,'Bad',CURRENT_TIMESTAMP(),6000,2670,1111101);
INSERT INTO `indicatordata` (`id`,`createTime`,`deleted`,`delta`,`editTime`,`effectfromdate`,`idealValueRange`,`indicatorvalue`,`score`,`state`,`thresholdcategory`,`updateddate`,`weightedIdealValue`,`weightedValue`,`indicatorId`) VALUES (3,CURRENT_TIMESTAMP(),0,6010,CURRENT_TIMESTAMP(),'2000-05-01 00:00:00','4.0-8.0',-0.01,6,1,'Fooling Us',CURRENT_TIMESTAMP(),6000,-10,1111101);
INSERT INTO `indicatordata` (`id`,`createTime`,`deleted`,`delta`,`editTime`,`effectfromdate`,`idealValueRange`,`indicatorvalue`,`score`,`state`,`thresholdcategory`,`updateddate`,`weightedIdealValue`,`weightedValue`,`indicatorId`) VALUES (4,CURRENT_TIMESTAMP(),0,-10,CURRENT_TIMESTAMP(),'2001-05-01 00:00:00','4.0-8.0',6.01,6,1,'On Track',CURRENT_TIMESTAMP(),6000,6010,1111101);